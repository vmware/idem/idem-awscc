import copy
import json
import os
import re
import time
import uuid
from datetime import datetime
from typing import Any
from typing import Callable
from typing import Dict

import jsonschema
from deepdiff import DeepDiff
from jsonschema._format import draft7_format_checker
from jsonschema.validators import validate


async def get_resource_info(
    hub, ctx, resource_type: str, download_schema_if_unavailable=True
) -> Dict[str, Any]:
    result = dict(
        result=True,
        comment=(),
        can_update=True,
        validation_schema={},
        aws_type_name="",
        read_only_attributes=set(),
        create_only_attributes=set(),
    )

    # TODO: Put schemas in a resources folder so that they can be read
    #  even after this project is turned into a package
    #  use importlib_resources
    filename = os.path.join(os.getcwd(), f"idem_awscc/schemas/{resource_type}.json")

    # Try to download the schema for the given resource type if it does not exist
    # TODO: Be careful with saving any files as the use might not have permissions
    if download_schema_if_unavailable and not os.path.exists(filename):
        all_resource_types_ret = (
            await hub.tool.awscc.cloudcontrol.api.get_all_resource_types_names(ctx)
        )

        if not all_resource_types_ret["result"]:
            result["result"] = False
            result["comment"] += all_resource_types_ret["comment"]
            return result

        aws_resource_type_name = None
        for available_aws_resource_type in all_resource_types_ret[
            "resource_types_list"
        ]:
            idem_resource_name = hub.tool.awscc.cloudcontrol.api.convert_aws_resource_type_name_to_lowercase(
                available_aws_resource_type
            )

            if idem_resource_name == resource_type:
                aws_resource_type_name = available_aws_resource_type
                break

        if not aws_resource_type_name:
            result["result"] = False
            result["comment"] += (
                f"The resource {resource_type} is not an available AWS Cloud Control API type.",
            )
            return result

        generated_schema_ret = await hub.exec.awscc.cloudcontrol.api.generate_json_schema(
            ctx, aws_resource_type_name, idem_resource_name
        )

        if not generated_schema_ret["result"]:
            result["result"] = False
            result["comment"] += generated_schema_ret["comment"]
            return result

        generated_documentation_ret = (
            hub.exec.awscc.cloudcontrol.api.generate_resource_documentation(resource_type)
        )
        if not generated_documentation_ret["result"]:
            result["comment"] += generated_documentation_ret["comment"]

    validation_schema = {}
    try:
        with open(filename) as json_file:
            validation_schema = json.load(json_file)
    except OSError as err:
        result["result"] = False
        result["comment"] += (
            filename,
            err,
        )
        return result

    if validation_schema:
        result["is_resource_mutable"] = validation_schema["is_resource_mutable"]
        result["validation_schema"] = validation_schema
        result["aws_type_name"] = validation_schema["typeName"]
        result["read_only_attributes"] = set(
            map(lambda x: x.split("/")[-1], validation_schema["readOnlyProperties"])
        )
        result["create_only_attributes"] = set(
            map(lambda x: x.split("/")[-1], validation_schema["createOnlyProperties"])
        )
    else:
        result["result"] = False
        result["comment"] += ("The resource json file is empty",)

    return result


async def get_all_resource_types_names(hub, ctx) -> Dict[str, Any]:
    result = dict(result=True, comment=(), resource_types_list=[])
    fully_mutable_resource_types_ret = (
        await hub.exec.boto3.client.cloudformation.list_types(
            ctx,
            Visibility="PUBLIC",
            ProvisioningType="FULLY_MUTABLE",
            Type="RESOURCE",
            Filters={"Category": "AWS_TYPES"},
        )
    )
    immutable_resource_types_ret = (
        await hub.exec.boto3.client.cloudformation.list_types(
            ctx,
            Visibility="PUBLIC",
            ProvisioningType="IMMUTABLE",
            Type="RESOURCE",
            Filters={"Category": "AWS_TYPES"},
        )
    )

    if (
        not fully_mutable_resource_types_ret["result"]
        or not immutable_resource_types_ret["result"]
    ):
        result["result"] = False
        result["comment"] += (
            fully_mutable_resource_types_ret["comment"]
            + immutable_resource_types_ret["comment"]
        )
    else:
        fully_mutable_resource_types_list = list(
            map(
                lambda x: x["TypeName"],
                fully_mutable_resource_types_ret["ret"]["TypeSummaries"],
            )
        )
        immutable_resource_types_list = list(
            map(
                lambda x: x["TypeName"],
                immutable_resource_types_ret["ret"]["TypeSummaries"],
            )
        )
        result["resource_types_list"] = (
            fully_mutable_resource_types_list + immutable_resource_types_list
        )

    return result


async def get_resource(
    hub, ctx, resource_type: str, resource_id: str, read_only_attributes: set
) -> Dict[str, Any]:
    result = dict(comment=(), result=True, state={})

    resource_state_ret = await hub.exec.boto3.client.cloudcontrol.get_resource(
        ctx, TypeName=resource_type, Identifier=resource_id
    )

    if resource_state_ret["result"]:
        resource_description = resource_state_ret["ret"].get("ResourceDescription")
        attributes_json = resource_description.get("Properties")

        result["state"] = hub.tool.awscc.cloudcontrol.api.convert_attributes_to_present(
            attributes_json, read_only_attributes
        )
        result["state"]["resource_id"] = resource_description.get("Identifier")
    else:
        result["result"] = False
        result["comment"] = resource_state_ret["comment"]

    return result


async def list_resources_ids(
    hub, ctx, resource_type: str, attributes: Dict[str, Any] = None
) -> Dict[str, Any]:
    attributes_json = (
        hub.tool.awscc.cloudcontrol.api.convert_attributes_to_json(attributes)
        if attributes
        else None
    )
    resources_list_ret = await hub.exec.boto3.client.cloudcontrol.list_resources(
        ctx, TypeName=resource_type, ResourceModel=attributes_json
    )

    result = dict(result=resources_list_ret["result"], comment=(), resources_ids=[])

    if resources_list_ret["result"]:
        resource_descriptions = resources_list_ret["ret"].get("ResourceDescriptions")
        result["resources_ids"] = [
            resource_description.get("Identifier")
            for resource_description in resource_descriptions
        ]
    else:
        result["comment"] = resources_list_ret["comment"]

    return result


async def replace_resource(
    hub,
    ctx,
    name: str,
    resource_type: str,
    resource_id: str,
    new_attributes: Dict[str, Any],
    read_only_attributes: set,
) -> Dict[str, Any]:
    delete_result = await hub.tool.awscc.cloudcontrol.api.delete_resource(
        ctx, name, resource_type, resource_id, read_only_attributes
    )

    if not delete_result["result"]:
        return delete_result

    create_result = await hub.tool.awscc.cloudcontrol.api.create_resource(
        ctx, name, resource_type, new_attributes, read_only_attributes
    )

    if create_result["result"]:
        create_result["comment"] = hub.tool.awscc.comment_utils.replace_comment(
            resource_type, name
        )

    return create_result


async def delete_resource(
    hub,
    ctx,
    name: str,
    resource_type: str,
    resource_id: str,
    read_only_attributes: set,
) -> Dict[str, Any]:
    result = await hub.tool.awscc.cloudcontrol.api.waiter_loop(
        ctx,
        resource_type,
        hub.exec.boto3.client.cloudcontrol.delete_resource,
        {"Identifier": resource_id},
        read_only_attributes,
        False,
    )

    if result["result"]:
        result["comment"] += hub.tool.awscc.comment_utils.delete_comment(
            resource_type, name
        )

    return result


async def update_resource(
    hub,
    ctx,
    name: str,
    resource_type: str,
    resource_id: str,
    old_attributes: Dict[str, Any],
    new_attributes: Dict[str, Any],
    read_only_attributes: set,
) -> Dict[str, Any]:
    json_patch = hub.tool.awscc.cloudcontrol.api.create_json_patch(
        old_attributes, new_attributes
    )
    result = await hub.tool.awscc.cloudcontrol.api.waiter_loop(
        ctx,
        resource_type,
        hub.exec.boto3.client.cloudcontrol.update_resource,
        {"Identifier": resource_id, "PatchDocument": json_patch},
        read_only_attributes,
    )

    if result["result"]:
        result["comment"] += hub.tool.awscc.comment_utils.update_comment(
            resource_type, name
        )

    return result


async def create_resource(
    hub,
    ctx,
    name: str,
    resource_type: str,
    attributes: Dict[str, Any],
    read_only_attributes: set,
) -> Dict[str, Any]:
    attributes_json = hub.tool.awscc.cloudcontrol.api.convert_attributes_to_json(
        attributes
    )
    result = await hub.tool.awscc.cloudcontrol.api.waiter_loop(
        ctx,
        resource_type,
        hub.exec.boto3.client.cloudcontrol.create_resource,
        {"DesiredState": attributes_json},
        read_only_attributes,
    )

    if result["result"]:
        result["comment"] += hub.tool.awscc.comment_utils.create_comment(
            resource_type, name
        )

    return result


async def waiter_loop(
    hub,
    ctx,
    resource_type: str,
    operation: Callable,
    operation_args: Dict[str, Any],
    read_only_attributes: set,
    get_new_state: bool = True,
    max_retries: int = 15,
    timeout_seconds: int = 1,
) -> Dict:
    result = dict(comment=(), result=False, new_state={})
    client_token = str(uuid.uuid4())
    retries = 0

    operation_request_ret = await operation(
        ctx, TypeName=resource_type, ClientToken=client_token, **operation_args
    )
    if not operation_request_ret["result"]:
        result["comment"] = operation_request_ret["comment"]
        return result

    request_token = operation_request_ret["ret"]["ProgressEvent"].get("RequestToken")

    while retries < max_retries:
        progress_event = operation_request_ret["ret"].get("ProgressEvent")
        progress_status = progress_event.get("OperationStatus")

        if progress_status == "SUCCESS":
            result["result"] = True

            if get_new_state:
                resource_id = progress_event.get("Identifier")
                new_state = progress_event.get("ResourceModel")

                if new_state:
                    result[
                        "new_state"
                    ] = hub.tool.awscc.cloudcontrol.api.convert_attributes_to_present(
                        new_state, read_only_attributes
                    )
                else:
                    new_state_ret = await hub.tool.awscc.cloudcontrol.api.get_resource(
                        ctx, resource_type, resource_id, read_only_attributes
                    )

                    if new_state_ret["result"]:
                        result["new_state"] = new_state_ret["state"]
                    else:
                        result["result"] = False
                        result["comment"] += new_state_ret["comment"]

                result["new_state"]["resource_id"] = resource_id
            break

        if progress_status == "FAILED":
            result["comment"] += (
                progress_event.get("ErrorCode"),
                progress_event.get("StatusMessage"),
            )
            break

        if progress_status == "CANCEL_COMPLETE":
            result["comment"] += ("Operation cancel completed",)
            break

        request_retry_time: datetime = progress_event.get("RetryAfter")
        retry_after_seconds = 0
        if request_retry_time:
            retry_after_seconds = (
                request_retry_time - datetime.now(request_retry_time.tzinfo)
            ).total_seconds()

        if retry_after_seconds > 0:
            time.sleep(retry_after_seconds)
        else:
            time.sleep(timeout_seconds)

        operation_request_ret = (
            await hub.exec.boto3.client.cloudcontrol.get_resource_request_status(
                ctx, RequestToken=request_token
            )
        )

        if not operation_request_ret["result"]:
            result["comment"] = operation_request_ret["comment"]
            return result

        retries += 1

    return result


def validate_attributes(
    hub, attributes, validation_schema: Dict, read_only_attributes: set
) -> Dict[str, Any]:
    result = dict(result=True, comment=())

    # Validate the new attributes names and value types
    try:
        validate(attributes, validation_schema, format_checker=draft7_format_checker)
    except jsonschema.exceptions.ValidationError as err:
        result["result"] = False
        result["comment"] += (
            err.json_path[2:],
            err.message,
        )
    except jsonschema.exceptions.SchemaError as err:
        result["result"] = False
        result["comment"] += (
            err.json_path[2:],
            err.message,
        )

    # Check if the user has modified a read-only attribute
    for read_only_attribute in read_only_attributes:
        if read_only_attribute in attributes.keys():
            result["result"] = False
            result["comment"] += (f"{read_only_attribute} is read-only",)

    return result


def convert_attributes_to_present(
    hub, attributes_json: str, read_only_attributes: set
) -> Dict[str, Any]:
    attributes_camelcase = json.loads(attributes_json)
    attributes: Dict = hub.tool.awscc.cloudcontrol.api.convert_attributes_names(
        attributes_camelcase, False
    )

    if read_only_attributes:
        for read_only_attribute in read_only_attributes:
            attributes.pop(read_only_attribute, None)

    return attributes


def convert_attributes_to_json(hub, attributes: Dict) -> str:
    attributes_copy = copy.deepcopy(attributes)
    camel_case_attributes = hub.tool.awscc.cloudcontrol.api.convert_attributes_names(
        attributes_copy, True
    )
    return json.dumps(camel_case_attributes)


def create_json_patch(hub, old_attributes: Dict, new_attributes: Dict) -> str:
    patch_list = []

    # TODO: convert to camelcase recursively and set the correct path for nested objects
    for attribute_name in new_attributes:
        op = "replace" if attribute_name in old_attributes.keys() else "add"
        new_value = new_attributes[attribute_name]
        if op == "replace":
            old_value = old_attributes[attribute_name]
            old_value_type = type(old_value)
            if isinstance(new_value, str) and (
                old_value_type == Dict or old_value_type == list
            ):
                new_value = json.loads(new_value)

            if not DeepDiff(
                new_value,
                old_value,
                ignore_order=True,
                report_repetition=True,
            ):
                continue
        camel_case_name = hub.tool.awscc.cloudcontrol.api.convert_lowercase_to_camelcase(
            attribute_name
        )
        camel_case_value = hub.tool.awscc.cloudcontrol.api.convert_attributes_names(
            new_value, True
        )

        patch_list.append(
            {"op": op, "path": f"/{camel_case_name}", "value": camel_case_value}
        )

    patch = json.dumps(patch_list)

    return patch


def convert_lowercase_to_camelcase(hub, val: str) -> str:
    return "".join(word.title() for word in val.split("_"))


def convert_camelcase_to_lowercase(hub, val: str) -> str:
    # TODO: Better regex which can match e.g. iooHOHjfhHelloIoTHeeey
    return re.sub(
        r"(((?<=[a-z])[A-Z])[A-Z]|(?<!\A)[A-Z](?=[a-z]))", r"_\1", val
    ).lower()


def convert_aws_resource_type_name_to_lowercase(hub, aws_type_name: str) -> str:
    # The resource type name format is service-provider::service-name::data-type-name
    resource_type_parts = aws_type_name.split("::")
    service_name = resource_type_parts[1].lower()
    data_type_name = hub.tool.awscc.cloudcontrol.api.convert_camelcase_to_lowercase(
        resource_type_parts[2]
    )
    return f"{service_name}_{data_type_name}"


def convert_attributes_names(hub, attributes, camel_case: bool):
    attributes_copy = copy.deepcopy(attributes)
    return hub.tool.awscc.cloudcontrol.api.convert_attributes_names_recursion(
        attributes_copy, camel_case
    )


def convert_attributes_names_recursion(hub, attributes, camel_case: bool):
    if isinstance(attributes, list):
        for i in range(len(attributes)):
            if isinstance(attributes[i], (Dict, list)):
                attributes[
                    i
                ] = hub.tool.awscc.cloudcontrol.api.convert_attributes_names_recursion(
                    attributes[i], camel_case
                )
    elif isinstance(attributes, Dict):
        converted_attributes = {}
        for key, value in attributes.items():
            converted_key_name = ""
            if camel_case:
                converted_key_name = (
                    hub.tool.awscc.cloudcontrol.api.convert_lowercase_to_camelcase(key)
                )
            else:
                converted_key_name = (
                    hub.tool.awscc.cloudcontrol.api.convert_camelcase_to_lowercase(key)
                )
            converted_attributes[
                converted_key_name
            ] = hub.tool.awscc.cloudcontrol.api.convert_attributes_names_recursion(
                value, camel_case
            )
        attributes = converted_attributes

    return attributes


def convert_reference_to_lowercase(hub, reference: str) -> str:
    ref_split = reference.split("/")
    start_index = -1
    if "properties" in ref_split:
        start_index = ref_split.index("properties") + 1
    elif "definitions" in ref_split:
        start_index = ref_split.index("definitions") + 1

    if start_index > -1:
        for i in range(start_index, len(ref_split)):
            ref_split[i] = hub.tool.awscc.cloudcontrol.api.convert_camelcase_to_lowercase(
                ref_split[i]
            )

    return "/".join(ref_split)


def convert_schema_to_lowercase(hub, schema: Dict):
    schema_copy = copy.deepcopy(schema)
    return hub.tool.awscc.cloudcontrol.api.convert_schema_to_lowercase_recursion(
        schema_copy
    )


def convert_schema_to_lowercase_recursion(hub, object_schema: Dict) -> Dict:
    if object_schema.get("$ref"):
        object_schema[
            "$ref"
        ] = hub.tool.awscc.cloudcontrol.api.convert_reference_to_lowercase(
            object_schema["$ref"]
        )

    definitions = copy.deepcopy(object_schema.get("definitions", {}))
    if definitions:
        object_schema["definitions"].clear()
        for prop_name, prop_value in definitions.items():
            prop_name = hub.tool.awscc.cloudcontrol.api.convert_camelcase_to_lowercase(
                prop_name
            )
            if isinstance(prop_value, Dict):
                prop_value = (
                    hub.tool.awscc.cloudcontrol.api.convert_schema_to_lowercase_recursion(
                        prop_value
                    )
                )
            object_schema["definitions"][prop_name] = prop_value

    properties_lists_names = {
        "readOnlyProperties",
        "writeOnlyProperties",
        "createOnlyProperties",
        "deprecatedProperties",
        "primaryIdentifier",
        "additionalIdentifiers",
    }

    properties_schema_keywords = {
        "patternProperties",
        "readOnly",
        "writeOnly",
        "insertionOrder",
        "conditionalCreateOnlyProperties",
        "nonPublicProperties",
        "nonPublicDefinitions",
    }

    combine_schema_keywords = {"oneOf", "anyOf", "allOf"}

    properties = copy.deepcopy(object_schema.get("properties", {}))
    if properties:
        object_schema["properties"].clear()
        for prop_name, prop_value in properties.items():
            if (
                prop_name not in properties_schema_keywords
                and prop_name not in properties_lists_names
                and prop_name not in combine_schema_keywords
            ):
                prop_name = (
                    hub.tool.awscc.cloudcontrol.api.convert_camelcase_to_lowercase(
                        prop_name
                    )
                )
            if isinstance(prop_value, Dict):
                prop_value = (
                    hub.tool.awscc.cloudcontrol.api.convert_schema_to_lowercase_recursion(
                        prop_value
                    )
                )
            object_schema["properties"][prop_name] = prop_value

    pattern_properties = copy.deepcopy(object_schema.get("patternProperties", {}))
    if pattern_properties:
        object_schema["patternProperties"].clear()
        for pattern_property_name, pattern_property_value in pattern_properties.items():
            if isinstance(pattern_property_value, Dict):
                pattern_property_value = (
                    hub.tool.awscc.cloudcontrol.api.convert_schema_to_lowercase_recursion(
                        pattern_property_value
                    )
                )

            object_schema["patternProperties"][
                pattern_property_name
            ] = pattern_property_value

    for combine_keyword in combine_schema_keywords:
        conditions_list = object_schema.get(combine_keyword, [])
        if conditions_list:
            for i in range(len(conditions_list)):
                if isinstance(conditions_list[i], Dict):
                    conditions_list[
                        i
                    ] = hub.tool.awscc.cloudcontrol.api.convert_schema_to_lowercase_recursion(
                        conditions_list[i]
                    )

    if isinstance(object_schema.get("items", False), Dict):
        object_schema[
            "items"
        ] = hub.tool.awscc.cloudcontrol.api.convert_schema_to_lowercase_recursion(
            object_schema["items"]
        )

    if object_schema.get("required"):
        object_schema["required"] = list(
            map(
                lambda x: hub.tool.awscc.cloudcontrol.api.convert_camelcase_to_lowercase(
                    x
                ),
                object_schema["required"],
            )
        )

    for property_list_name in properties_lists_names:
        if isinstance(object_schema.get(property_list_name, False), list):
            if property_list_name == "additionalIdentifiers":
                for j in range(len(object_schema[property_list_name])):
                    for i in range(len(object_schema[property_list_name][j])):
                        object_schema[property_list_name][j][
                            i
                        ] = hub.tool.awscc.cloudcontrol.api.convert_reference_to_lowercase(
                            object_schema[property_list_name][j][i]
                        )
            else:
                for i in range(len(object_schema[property_list_name])):
                    object_schema[property_list_name][
                        i
                    ] = hub.tool.awscc.cloudcontrol.api.convert_reference_to_lowercase(
                        object_schema[property_list_name][i]
                    )

    return object_schema
