# https://pop.readthedocs.io/en/latest/tutorial/quickstart.html#adding-configuration-data

# In this dictionary goes all the immutable values you want to show up under hub.OPT.idem_awscc
CLI_CONFIG = {
    # pop-create options
    "services": {
        "subcommands": ["awscc"],
        "dyne": "pop_create",
    },
}

# The selected subcommand for your cli tool will show up under hub.SUBPARSER
# The value for a subcommand is a dictionary that will be passed as kwargs to argparse.ArgumentParser.add_subparsers
CONFIG = {
    # pop-create options
    "services": {
        "default": [],
        "nargs": "*",
        "help": "The cloud services to target, defaults to all",
        "dyne": "pop_create",
    },
}

# Include keys from the CONFIG dictionary that you want to expose on the cli
# The values for these keys are a dictionaries that will be passed as kwargs to argparse.ArgumentParser.add_option
SUBCOMMANDS = {
    "awscc": {
        "help": "Create idem_aws state modules by parsing boto3",
        "dyne": "pop_create",
    },
}

# These are the namespaces that your project extends
# The hub will extend these keys with the modules listed in the values
DYNE = {
    "acct": ["acct"],
    "exec": ["exec"],
    "pop_create": ["autogen"],
    "states": ["states"],
    "tool": ["tool"],
    "esm": ["esm"]
}
