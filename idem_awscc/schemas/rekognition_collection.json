{
  "typeName": "AWS::Rekognition::Collection",
  "description": "The AWS::Rekognition::Collection type creates an Amazon Rekognition Collection. A collection is a logical grouping of information about detected faces which can later be referenced for searches on the group",
  "sourceUrl": "https://docs.aws.amazon.com/rekognition/latest/dg/collections.html",
  "definitions": {
    "arn": {
      "$comment": "Use the `definitions` block to provide shared resource property schemas",
      "type": "string",
      "maxLength": 2048,
      "format": "(^arn:[a-z\\d-]+:rekognition:[a-z\\d-]+:\\d{12}:collection\\/([a-zA-Z0-9_.\\-]+){1,255})"
    },
    "collection_id": {
      "description": "The name of the collection",
      "type": "string",
      "maxLength": 255,
      "pattern": "\\A[a-zA-Z0-9_\\.\\-]+$"
    },
    "tag": {
      "description": "A key-value pair to associate with a resource.",
      "type": "object",
      "properties": {
        "key": {
          "type": "string",
          "description": "The key name of the tag. You can specify a value that is 1 to 128 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -.",
          "maxLength": 128,
          "pattern": "\\A(?!aws:)[a-zA-Z0-9+\\-=\\._\\:\\/@]+$"
        },
        "value": {
          "type": "string",
          "description": "The value for the tag. You can specify a value that is 0 to 256 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -.",
          "maxLength": 256,
          "pattern": "\\A[a-zA-Z0-9+\\-=\\._\\:\\/@]+$"
        }
      },
      "required": [
        "key",
        "value"
      ],
      "additionalProperties": false
    }
  },
  "properties": {
    "arn": {
      "$ref": "#/definitions/arn"
    },
    "collection_id": {
      "$ref": "#/definitions/collection_id"
    },
    "tags": {
      "description": "An array of key-value pairs to apply to this resource.",
      "type": "array",
      "uniqueItems": true,
      "insertionOrder": false,
      "minItems": 0,
      "maxItems": 200,
      "items": {
        "$ref": "#/definitions/tag"
      }
    }
  },
  "taggable": true,
  "additionalProperties": false,
  "required": [
    "collection_id"
  ],
  "readOnlyProperties": [
    "/properties/arn"
  ],
  "createOnlyProperties": [
    "/properties/collection_id"
  ],
  "primaryIdentifier": [
    "/properties/collection_id"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "rekognition:CreateCollection",
        "rekognition:DescribeCollection",
        "rekognition:ListTagsForResource"
      ]
    },
    "read": {
      "permissions": [
        "rekognition:DescribeCollection",
        "rekognition:ListTagsForResource"
      ]
    },
    "update": {
      "permissions": [
        "rekognition:TagResource",
        "rekognition:UntagResource",
        "rekognition:DescribeCollection",
        "rekognition:ListTagsForResource"
      ]
    },
    "delete": {
      "permissions": [
        "rekognition:DeleteCollection"
      ]
    },
    "list": {
      "permissions": [
        "rekognition:ListCollections"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}