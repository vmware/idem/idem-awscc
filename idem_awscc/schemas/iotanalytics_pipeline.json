{
  "typeName": "AWS::IoTAnalytics::Pipeline",
  "description": "Resource Type definition for AWS::IoTAnalytics::Pipeline",
  "additionalProperties": false,
  "taggable": true,
  "properties": {
    "id": {
      "type": "string"
    },
    "pipeline_name": {
      "type": "string",
      "pattern": "[a-zA-Z0-9_]+",
      "minLength": 1,
      "maxLength": 128
    },
    "tags": {
      "type": "array",
      "uniqueItems": false,
      "insertionOrder": false,
      "minItems": 1,
      "maxItems": 50,
      "items": {
        "$ref": "#/definitions/tag"
      }
    },
    "pipeline_activities": {
      "type": "array",
      "uniqueItems": false,
      "insertionOrder": false,
      "minItems": 1,
      "maxItems": 25,
      "items": {
        "$ref": "#/definitions/activity"
      }
    }
  },
  "definitions": {
    "activity": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "select_attributes": {
          "$ref": "#/definitions/select_attributes"
        },
        "datastore": {
          "$ref": "#/definitions/datastore"
        },
        "filter": {
          "$ref": "#/definitions/filter"
        },
        "add_attributes": {
          "$ref": "#/definitions/add_attributes"
        },
        "channel": {
          "$ref": "#/definitions/channel"
        },
        "device_shadow_enrich": {
          "$ref": "#/definitions/device_shadow_enrich"
        },
        "math": {
          "$ref": "#/definitions/math"
        },
        "lambda": {
          "$ref": "#/definitions/lambda"
        },
        "device_registry_enrich": {
          "$ref": "#/definitions/device_registry_enrich"
        },
        "remove_attributes": {
          "$ref": "#/definitions/remove_attributes"
        }
      }
    },
    "tag": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "key": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "value": {
          "type": "string",
          "minLength": 1,
          "maxLength": 256
        }
      },
      "required": [
        "value",
        "key"
      ]
    },
    "device_shadow_enrich": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "attribute": {
          "type": "string",
          "minLength": 1,
          "maxLength": 256
        },
        "next": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "thing_name": {
          "type": "string",
          "minLength": 1,
          "maxLength": 256
        },
        "role_arn": {
          "type": "string",
          "minLength": 20,
          "maxLength": 2048
        },
        "name": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        }
      },
      "required": [
        "attribute",
        "thing_name",
        "role_arn",
        "name"
      ]
    },
    "filter": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "filter": {
          "type": "string",
          "minLength": 1,
          "maxLength": 256
        },
        "next": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "name": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        }
      },
      "required": [
        "filter",
        "name"
      ]
    },
    "remove_attributes": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "next": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "attributes": {
          "type": "array",
          "uniqueItems": false,
          "insertionOrder": false,
          "minItems": 1,
          "maxItems": 50,
          "items": {
            "type": "string",
            "minLength": 1,
            "maxLength": 256
          }
        },
        "name": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        }
      },
      "required": [
        "attributes",
        "name"
      ]
    },
    "datastore": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "datastore_name": {
          "type": "string",
          "pattern": "[a-zA-Z0-9_]+",
          "minLength": 1,
          "maxLength": 128
        },
        "name": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        }
      },
      "required": [
        "datastore_name",
        "name"
      ]
    },
    "channel": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "channel_name": {
          "type": "string",
          "pattern": "[a-zA-Z0-9_]+",
          "minLength": 1,
          "maxLength": 128
        },
        "next": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "name": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        }
      },
      "required": [
        "channel_name",
        "name"
      ]
    },
    "select_attributes": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "next": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "attributes": {
          "type": "array",
          "uniqueItems": false,
          "insertionOrder": false,
          "minItems": 1,
          "maxItems": 50,
          "items": {
            "type": "string",
            "minLength": 1,
            "maxLength": 256
          }
        },
        "name": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        }
      },
      "required": [
        "name",
        "attributes"
      ]
    },
    "lambda": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "batch_size": {
          "type": "integer",
          "minimum": 1,
          "maximum": 1000
        },
        "next": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "lambda_name": {
          "type": "string",
          "pattern": "[a-zA-Z0-9_-]+",
          "minLength": 1,
          "maxLength": 64
        },
        "name": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        }
      },
      "required": [
        "lambda_name",
        "name",
        "batch_size"
      ]
    },
    "device_registry_enrich": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "attribute": {
          "type": "string",
          "minLength": 1,
          "maxLength": 256
        },
        "next": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "thing_name": {
          "type": "string",
          "minLength": 1,
          "maxLength": 256
        },
        "role_arn": {
          "type": "string",
          "minLength": 20,
          "maxLength": 2048
        },
        "name": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        }
      },
      "required": [
        "attribute",
        "thing_name",
        "role_arn",
        "name"
      ]
    },
    "add_attributes": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "next": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "attributes": {
          "type": "object",
          "minProperties": 1,
          "maxProperties": 50,
          "patternProperties": {
            "^.*$": {
              "type": "string",
              "minLength": 1,
              "maxLength": 256
            }
          },
          "additionalProperties": false
        },
        "name": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        }
      },
      "required": [
        "attributes",
        "name"
      ]
    },
    "math": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "attribute": {
          "type": "string",
          "minLength": 1,
          "maxLength": 256
        },
        "next": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "math": {
          "type": "string",
          "minLength": 1,
          "maxLength": 256
        },
        "name": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        }
      },
      "required": [
        "attribute",
        "math",
        "name"
      ]
    }
  },
  "required": [
    "pipeline_activities"
  ],
  "primaryIdentifier": [
    "/properties/pipeline_name"
  ],
  "createOnlyProperties": [
    "/properties/pipeline_name"
  ],
  "readOnlyProperties": [
    "/properties/id"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "iotanalytics:CreatePipeline"
      ]
    },
    "read": {
      "permissions": [
        "iotanalytics:DescribePipeline",
        "iotanalytics:ListTagsForResource"
      ]
    },
    "update": {
      "permissions": [
        "iotanalytics:UpdatePipeline",
        "iotanalytics:TagResource",
        "iotanalytics:UntagResource"
      ]
    },
    "delete": {
      "permissions": [
        "iotanalytics:DeletePipeline"
      ]
    },
    "list": {
      "permissions": [
        "iotanalytics:ListPipelines"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}