{
  "typeName": "AWS::SageMaker::UserProfile",
  "description": "Resource Type definition for AWS::SageMaker::UserProfile",
  "additionalProperties": false,
  "properties": {
    "user_profile_arn": {
      "type": "string",
      "description": "The user profile Amazon Resource Name (ARN).",
      "maxLength": 256,
      "pattern": "arn:aws[a-z\\-]*:sagemaker:[a-z0-9\\-]*:[0-9]{12}:user-profile/.*"
    },
    "domain_id": {
      "type": "string",
      "description": "The ID of the associated Domain.",
      "minLength": 1,
      "maxLength": 63
    },
    "single_sign_on_user_identifier": {
      "type": "string",
      "description": "A specifier for the type of value specified in SingleSignOnUserValue. Currently, the only supported value is \"UserName\". If the Domain's AuthMode is SSO, this field is required. If the Domain's AuthMode is not SSO, this field cannot be specified.",
      "pattern": "UserName"
    },
    "single_sign_on_user_value": {
      "type": "string",
      "description": "The username of the associated AWS Single Sign-On User for this UserProfile. If the Domain's AuthMode is SSO, this field is required, and must match a valid username of a user in your directory. If the Domain's AuthMode is not SSO, this field cannot be specified.",
      "minLength": 1,
      "maxLength": 256
    },
    "user_profile_name": {
      "type": "string",
      "description": "A name for the UserProfile.",
      "minLength": 1,
      "maxLength": 63
    },
    "user_settings": {
      "$ref": "#/definitions/user_settings",
      "description": "A collection of settings.",
      "uniqueItems": false,
      "minItems": 0,
      "maxItems": 50
    },
    "tags": {
      "type": "array",
      "description": "A list of tags to apply to the user profile.",
      "uniqueItems": false,
      "minItems": 0,
      "maxItems": 50,
      "items": {
        "$ref": "#/definitions/tag"
      }
    }
  },
  "definitions": {
    "user_settings": {
      "type": "object",
      "description": "A collection of settings that apply to users of Amazon SageMaker Studio. These settings are specified when the CreateUserProfile API is called, and as DefaultUserSettings when the CreateDomain API is called.",
      "additionalProperties": false,
      "properties": {
        "execution_role": {
          "type": "string",
          "description": "The user profile Amazon Resource Name (ARN).",
          "minLength": 20,
          "maxLength": 2048,
          "pattern": "^arn:aws[a-z\\-]*:iam::\\d{12}:role/?[a-zA-Z_0-9+=,.@\\-_/]+$"
        },
        "jupyter_server_app_settings": {
          "$ref": "#/definitions/jupyter_server_app_settings",
          "description": "The Jupyter server's app settings."
        },
        "kernel_gateway_app_settings": {
          "$ref": "#/definitions/kernel_gateway_app_settings",
          "description": "The kernel gateway app settings."
        },
        "r_studio_server_pro_app_settings": {
          "$ref": "#/definitions/r_studio_server_pro_app_settings"
        },
        "security_groups": {
          "type": "array",
          "description": "The security groups for the Amazon Virtual Private Cloud (VPC) that Studio uses for communication.",
          "uniqueItems": false,
          "minItems": 0,
          "maxItems": 5,
          "items": {
            "type": "string",
            "maxLength": 32,
            "pattern": "[-0-9a-zA-Z]+"
          }
        },
        "sharing_settings": {
          "$ref": "#/definitions/sharing_settings",
          "description": "The sharing settings."
        }
      }
    },
    "jupyter_server_app_settings": {
      "type": "object",
      "description": "The JupyterServer app settings.",
      "additionalProperties": false,
      "properties": {
        "default_resource_spec": {
          "$ref": "#/definitions/resource_spec"
        }
      }
    },
    "resource_spec": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "instance_type": {
          "type": "string",
          "description": "The instance type that the image version runs on.",
          "enum": [
            "system",
            "ml.t3.micro",
            "ml.t3.small",
            "ml.t3.medium",
            "ml.t3.large",
            "ml.t3.xlarge",
            "ml.t3.2xlarge",
            "ml.m5.large",
            "ml.m5.xlarge",
            "ml.m5.2xlarge",
            "ml.m5.4xlarge",
            "ml.m5.8xlarge",
            "ml.m5.12xlarge",
            "ml.m5.16xlarge",
            "ml.m5.24xlarge",
            "ml.c5.large",
            "ml.c5.xlarge",
            "ml.c5.2xlarge",
            "ml.c5.4xlarge",
            "ml.c5.9xlarge",
            "ml.c5.12xlarge",
            "ml.c5.18xlarge",
            "ml.c5.24xlarge",
            "ml.p3.2xlarge",
            "ml.p3.8xlarge",
            "ml.p3.16xlarge",
            "ml.g4dn.xlarge",
            "ml.g4dn.2xlarge",
            "ml.g4dn.4xlarge",
            "ml.g4dn.8xlarge",
            "ml.g4dn.12xlarge",
            "ml.g4dn.16xlarge"
          ]
        },
        "sage_maker_image_arn": {
          "type": "string",
          "description": "The ARN of the SageMaker image that the image version belongs to.",
          "maxLength": 256,
          "pattern": "^arn:aws(-[\\w]+)*:sagemaker:.+:[0-9]{12}:image/[a-z0-9]([-.]?[a-z0-9])*$"
        },
        "sage_maker_image_version_arn": {
          "type": "string",
          "description": "The ARN of the image version created on the instance.",
          "maxLength": 256,
          "pattern": "^arn:aws(-[\\w]+)*:sagemaker:.+:[0-9]{12}:image-version/[a-z0-9]([-.]?[a-z0-9])*/[0-9]+$"
        }
      }
    },
    "kernel_gateway_app_settings": {
      "type": "object",
      "description": "The kernel gateway app settings.",
      "additionalProperties": false,
      "properties": {
        "custom_images": {
          "type": "array",
          "description": "A list of custom SageMaker images that are configured to run as a KernelGateway app.",
          "uniqueItems": false,
          "minItems": 0,
          "maxItems": 30,
          "items": {
            "$ref": "#/definitions/custom_image"
          }
        },
        "default_resource_spec": {
          "$ref": "#/definitions/resource_spec",
          "description": "The default instance type and the Amazon Resource Name (ARN) of the default SageMaker image used by the KernelGateway app."
        }
      }
    },
    "custom_image": {
      "type": "object",
      "description": "A custom SageMaker image.",
      "additionalProperties": false,
      "properties": {
        "app_image_config_name": {
          "type": "string",
          "description": "The Name of the AppImageConfig.",
          "maxLength": 63,
          "pattern": "^[a-zA-Z0-9](-*[a-zA-Z0-9]){0,62}"
        },
        "image_name": {
          "type": "string",
          "description": "The name of the CustomImage. Must be unique to your account.",
          "maxLength": 63,
          "pattern": "^[a-zA-Z0-9]([-.]?[a-zA-Z0-9]){0,62}$"
        },
        "image_version_number": {
          "type": "integer",
          "description": "The version number of the CustomImage.",
          "minimum": 0
        }
      },
      "required": [
        "app_image_config_name",
        "image_name"
      ]
    },
    "sharing_settings": {
      "type": "object",
      "description": "Specifies options when sharing an Amazon SageMaker Studio notebook. These settings are specified as part of DefaultUserSettings when the CreateDomain API is called, and as part of UserSettings when the CreateUserProfile API is called.",
      "additionalProperties": false,
      "properties": {
        "notebook_output_option": {
          "type": "string",
          "description": "Whether to include the notebook cell output when sharing the notebook. The default is Disabled.",
          "enum": [
            "Allowed",
            "Disabled"
          ]
        },
        "s3_kms_key_id": {
          "type": "string",
          "description": "When NotebookOutputOption is Allowed, the AWS Key Management Service (KMS) encryption key ID used to encrypt the notebook cell output in the Amazon S3 bucket.",
          "maxLength": 2048,
          "pattern": ".*"
        },
        "s3_output_path": {
          "type": "string",
          "description": "When NotebookOutputOption is Allowed, the Amazon S3 bucket used to store the shared notebook snapshots.",
          "maxLength": 1024,
          "pattern": "^(https|s3)://([^/]+)/?(.*)$"
        }
      }
    },
    "tag": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "value": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "key": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        }
      },
      "required": [
        "key",
        "value"
      ]
    },
    "r_studio_server_pro_app_settings": {
      "type": "object",
      "description": "A collection of settings that configure user interaction with the RStudioServerPro app.",
      "additionalProperties": false,
      "properties": {
        "access_status": {
          "type": "string",
          "description": "Indicates whether the current user has access to the RStudioServerPro app.",
          "enum": [
            "ENABLED",
            "DISABLED"
          ]
        },
        "user_group": {
          "type": "string",
          "description": "The level of permissions that the user has within the RStudioServerPro app. This value defaults to User. The Admin value allows the user access to the RStudio Administrative Dashboard.",
          "enum": [
            "R_STUDIO_ADMIN",
            "R_STUDIO_USER"
          ]
        }
      }
    }
  },
  "required": [
    "domain_id",
    "user_profile_name"
  ],
  "createOnlyProperties": [
    "/properties/domain_id",
    "/properties/user_profile_name",
    "/properties/single_sign_on_user_identifier",
    "/properties/single_sign_on_user_value",
    "/properties/user_settings/r_studio_server_pro_app_settings/access_status",
    "/properties/user_settings/r_studio_server_pro_app_settings/user_group",
    "/properties/tags"
  ],
  "writeOnlyProperties": [
    "/properties/tags"
  ],
  "primaryIdentifier": [
    "/properties/user_profile_name",
    "/properties/domain_id"
  ],
  "readOnlyProperties": [
    "/properties/user_profile_arn"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "sagemaker:CreateUserProfile",
        "sagemaker:DescribeUserProfile",
        "sagemaker:DescribeImage",
        "sagemaker:DescribeImageVersion",
        "iam:PassRole"
      ]
    },
    "read": {
      "permissions": [
        "sagemaker:DescribeUserProfile"
      ]
    },
    "update": {
      "permissions": [
        "sagemaker:UpdateUserProfile",
        "sagemaker:DescribeUserProfile",
        "sagemaker:DescribeImage",
        "sagemaker:DescribeImageVersion",
        "iam:PassRole"
      ]
    },
    "delete": {
      "permissions": [
        "sagemaker:DeleteUserProfile",
        "sagemaker:DescribeUserProfile"
      ]
    },
    "list": {
      "permissions": [
        "sagemaker:ListUserProfiles"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}