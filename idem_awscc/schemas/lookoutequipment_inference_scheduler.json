{
  "typeName": "AWS::LookoutEquipment::InferenceScheduler",
  "description": "Resource schema for LookoutEquipment InferenceScheduler.",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-rpdk.git",
  "definitions": {
    "bucket": {
      "type": "string",
      "pattern": "^[a-z0-9][\\.\\-a-z0-9]{1,61}[a-z0-9]$",
      "minLength": 3,
      "maxLength": 63
    },
    "prefix": {
      "type": "string",
      "minLength": 0,
      "maxLength": 1024
    },
    "s3_input_configuration": {
      "description": "Specifies configuration information for the input data for the inference, including input data S3 location.",
      "type": "object",
      "properties": {
        "bucket": {
          "$ref": "#/definitions/bucket"
        },
        "prefix": {
          "$ref": "#/definitions/prefix"
        }
      },
      "required": [
        "bucket"
      ],
      "additionalProperties": false
    },
    "s3_output_configuration": {
      "description": "Specifies configuration information for the output results from the inference, including output S3 location.",
      "type": "object",
      "properties": {
        "bucket": {
          "$ref": "#/definitions/bucket"
        },
        "prefix": {
          "$ref": "#/definitions/prefix"
        }
      },
      "required": [
        "bucket"
      ],
      "additionalProperties": false
    },
    "input_name_configuration": {
      "description": "Specifies configuration information for the input data for the inference, including timestamp format and delimiter.",
      "type": "object",
      "properties": {
        "component_timestamp_delimiter": {
          "description": "Indicates the delimiter character used between items in the data.",
          "type": "string",
          "pattern": "^(\\-|\\_|\\s)?$",
          "minLength": 0,
          "maxLength": 1
        },
        "timestamp_format": {
          "description": "The format of the timestamp, whether Epoch time, or standard, with or without hyphens (-).",
          "type": "string",
          "pattern": "^EPOCH|yyyy-MM-dd-HH-mm-ss|yyyyMMddHHmmss$"
        }
      },
      "additionalProperties": false
    },
    "tag": {
      "description": "A tag is a key-value pair that can be added to a resource as metadata.",
      "type": "object",
      "properties": {
        "key": {
          "description": "The key for the specified tag.",
          "type": "string",
          "pattern": "^(?!aws:)[a-zA-Z+-=._:/]+$",
          "minLength": 1,
          "maxLength": 128
        },
        "value": {
          "description": "The value for the specified tag.",
          "type": "string",
          "pattern": "[\\s\\w+-=\\.:/@]*",
          "minLength": 0,
          "maxLength": 256
        }
      },
      "required": [
        "key",
        "value"
      ],
      "additionalProperties": false
    }
  },
  "properties": {
    "data_delay_offset_in_minutes": {
      "description": "A period of time (in minutes) by which inference on the data is delayed after the data starts.",
      "type": "integer",
      "minimum": 0,
      "maximum": 60
    },
    "data_input_configuration": {
      "description": "Specifies configuration information for the input data for the inference scheduler, including delimiter, format, and dataset location.",
      "type": "object",
      "properties": {
        "input_time_zone_offset": {
          "description": "Indicates the difference between your time zone and Greenwich Mean Time (GMT).",
          "type": "string",
          "pattern": "^(\\+|\\-)[0-9]{2}\\:[0-9]{2}$"
        },
        "inference_input_name_configuration": {
          "$ref": "#/definitions/input_name_configuration"
        },
        "s3_input_configuration": {
          "$ref": "#/definitions/s3_input_configuration"
        }
      },
      "required": [
        "s3_input_configuration"
      ],
      "additionalProperties": false
    },
    "data_output_configuration": {
      "description": "Specifies configuration information for the output results for the inference scheduler, including the S3 location for the output.",
      "type": "object",
      "properties": {
        "kms_key_id": {
          "description": "The ID number for the AWS KMS key used to encrypt the inference output.",
          "type": "string",
          "pattern": "^[A-Za-z0-9][A-Za-z0-9:_/+=,@.-]{0,2048}$",
          "minLength": 1,
          "maxLength": 2048
        },
        "s3_output_configuration": {
          "$ref": "#/definitions/s3_output_configuration"
        }
      },
      "required": [
        "s3_output_configuration"
      ],
      "additionalProperties": false
    },
    "data_upload_frequency": {
      "description": "How often data is uploaded to the source S3 bucket for the input data.",
      "type": "string",
      "enum": [
        "PT5M",
        "PT10M",
        "PT15M",
        "PT30M",
        "PT1H"
      ]
    },
    "inference_scheduler_name": {
      "description": "The name of the inference scheduler being created.",
      "type": "string",
      "pattern": "^[0-9a-zA-Z_-]{1,200}$",
      "minLength": 1,
      "maxLength": 200
    },
    "model_name": {
      "description": "The name of the previously trained ML model being used to create the inference scheduler.",
      "type": "string",
      "pattern": "^[0-9a-zA-Z_-]{1,200}$",
      "minLength": 1,
      "maxLength": 200
    },
    "role_arn": {
      "description": "The Amazon Resource Name (ARN) of a role with permission to access the data source being used for the inference.",
      "type": "string",
      "pattern": "arn:aws(-[^:]+)?:iam::[0-9]{12}:role/.+",
      "minLength": 20,
      "maxLength": 2048
    },
    "server_side_kms_key_id": {
      "description": "Provides the identifier of the AWS KMS customer master key (CMK) used to encrypt inference scheduler data by Amazon Lookout for Equipment.",
      "type": "string",
      "pattern": "^[A-Za-z0-9][A-Za-z0-9:_/+=,@.-]{0,2048}$",
      "minLength": 1,
      "maxLength": 2048
    },
    "tags": {
      "description": "Any tags associated with the inference scheduler.",
      "type": "array",
      "uniqueItems": true,
      "insertionOrder": false,
      "maxItems": 200,
      "items": {
        "$ref": "#/definitions/tag"
      }
    },
    "inference_scheduler_arn": {
      "description": "The Amazon Resource Name (ARN) of the inference scheduler being created.",
      "type": "string",
      "pattern": "arn:aws(-[^:]+)?:lookoutequipment:[a-zA-Z0-9\\-]*:[0-9]{12}:inference-scheduler\\/.+",
      "minLength": 1,
      "maxLength": 200
    }
  },
  "additionalProperties": false,
  "required": [
    "data_input_configuration",
    "data_output_configuration",
    "data_upload_frequency",
    "model_name",
    "role_arn"
  ],
  "readOnlyProperties": [
    "/properties/inference_scheduler_arn"
  ],
  "createOnlyProperties": [
    "/properties/inference_scheduler_name",
    "/properties/model_name",
    "/properties/server_side_kms_key_id"
  ],
  "primaryIdentifier": [
    "/properties/inference_scheduler_name"
  ],
  "taggable": true,
  "handlers": {
    "create": {
      "permissions": [
        "iam:PassRole",
        "lookoutequipment:CreateInferenceScheduler",
        "lookoutequipment:DescribeInferenceScheduler"
      ]
    },
    "read": {
      "permissions": [
        "lookoutequipment:DescribeInferenceScheduler"
      ]
    },
    "delete": {
      "permissions": [
        "lookoutequipment:DeleteInferenceScheduler",
        "lookoutequipment:StopInferenceScheduler",
        "lookoutequipment:DescribeInferenceScheduler"
      ]
    },
    "update": {
      "permissions": [
        "lookoutequipment:UpdateInferenceScheduler",
        "lookoutequipment:DescribeInferenceScheduler",
        "lookoutequipment:StopInferenceScheduler",
        "lookoutequipment:StartInferenceScheduler"
      ]
    },
    "list": {
      "permissions": [
        "lookoutequipment:ListInferenceSchedulers"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}