{
  "typeName": "AWS::Events::Connection",
  "description": "Resource Type definition for AWS::Events::Connection.",
  "definitions": {
    "auth_parameters": {
      "type": "object",
      "minProperties": 1,
      "maxProperties": 2,
      "properties": {
        "api_key_auth_parameters": {
          "$ref": "#/definitions/api_key_auth_parameters"
        },
        "basic_auth_parameters": {
          "$ref": "#/definitions/basic_auth_parameters"
        },
        "o_auth_parameters": {
          "$ref": "#/definitions/o_auth_parameters"
        },
        "invocation_http_parameters": {
          "$ref": "#/definitions/connection_http_parameters"
        }
      },
      "oneOf": [
        {
          "required": [
            "basic_auth_parameters"
          ]
        },
        {
          "required": [
            "o_auth_parameters"
          ]
        },
        {
          "required": [
            "api_key_auth_parameters"
          ]
        }
      ],
      "additionalProperties": false
    },
    "basic_auth_parameters": {
      "type": "object",
      "properties": {
        "username": {
          "type": "string"
        },
        "password": {
          "type": "string"
        }
      },
      "required": [
        "username",
        "password"
      ],
      "additionalProperties": false
    },
    "o_auth_parameters": {
      "type": "object",
      "properties": {
        "client_parameters": {
          "$ref": "#/definitions/client_parameters"
        },
        "authorization_endpoint": {
          "type": "string",
          "minLength": 1,
          "maxLength": 2048
        },
        "http_method": {
          "type": "string",
          "enum": [
            "GET",
            "POST",
            "PUT"
          ]
        },
        "o_auth_http_parameters": {
          "$ref": "#/definitions/connection_http_parameters"
        }
      },
      "required": [
        "client_parameters",
        "authorization_endpoint",
        "http_method"
      ],
      "additionalProperties": false
    },
    "api_key_auth_parameters": {
      "type": "object",
      "properties": {
        "api_key_name": {
          "type": "string"
        },
        "api_key_value": {
          "type": "string"
        }
      },
      "required": [
        "api_key_name",
        "api_key_value"
      ],
      "additionalProperties": false
    },
    "client_parameters": {
      "type": "object",
      "properties": {
        "client_id": {
          "type": "string"
        },
        "client_secret": {
          "type": "string"
        }
      },
      "required": [
        "client_id",
        "client_secret"
      ],
      "additionalProperties": false
    },
    "connection_http_parameters": {
      "type": "object",
      "properties": {
        "header_parameters": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/parameter"
          }
        },
        "query_string_parameters": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/parameter"
          }
        },
        "body_parameters": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/parameter"
          }
        }
      },
      "additionalProperties": false
    },
    "parameter": {
      "type": "object",
      "properties": {
        "key": {
          "type": "string"
        },
        "value": {
          "type": "string"
        },
        "is_value_secret": {
          "type": "boolean",
          "default": true
        }
      },
      "required": [
        "key",
        "value"
      ],
      "additionalProperties": false
    }
  },
  "properties": {
    "name": {
      "description": "Name of the connection.",
      "type": "string",
      "minLength": 1,
      "maxLength": 64
    },
    "arn": {
      "description": "The arn of the connection resource.",
      "type": "string"
    },
    "secret_arn": {
      "description": "The arn of the secrets manager secret created in the customer account.",
      "type": "string"
    },
    "description": {
      "description": "Description of the connection.",
      "type": "string",
      "maxLength": 512
    },
    "authorization_type": {
      "type": "string",
      "enum": [
        "API_KEY",
        "BASIC",
        "OAUTH_CLIENT_CREDENTIALS"
      ]
    },
    "auth_parameters": {
      "$ref": "#/definitions/auth_parameters"
    }
  },
  "additionalProperties": false,
  "required": [
    "authorization_type",
    "auth_parameters"
  ],
  "createOnlyProperties": [
    "/properties/name"
  ],
  "readOnlyProperties": [
    "/properties/arn",
    "/properties/secret_arn"
  ],
  "writeOnlyProperties": [
    "/properties/auth_parameters"
  ],
  "primaryIdentifier": [
    "/properties/name"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "events:CreateConnection",
        "secretsmanager:CreateSecret",
        "secretsmanager:GetSecretValue",
        "secretsmanager:PutSecretValue",
        "iam:CreateServiceLinkedRole"
      ]
    },
    "read": {
      "permissions": [
        "events:DescribeConnection"
      ]
    },
    "update": {
      "permissions": [
        "events:UpdateConnection",
        "events:DescribeConnection",
        "secretsmanager:CreateSecret",
        "secretsmanager:UpdateSecret",
        "secretsmanager:GetSecretValue",
        "secretsmanager:PutSecretValue"
      ]
    },
    "delete": {
      "permissions": [
        "events:DeleteConnection"
      ]
    },
    "list": {
      "permissions": [
        "events:ListConnections"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}