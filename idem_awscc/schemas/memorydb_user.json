{
  "typeName": "AWS::MemoryDB::User",
  "description": "Resource Type definition for AWS::MemoryDB::User",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-memorydb",
  "taggable": true,
  "definitions": {
    "tag": {
      "description": "A key-value pair to associate with a resource.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "key": {
          "description": "The key name of the tag. You can specify a value that is 1 to 128 Unicode characters in length and cannot be prefixed with aws: or memorydb:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -.",
          "type": "string",
          "pattern": "^(?!aws:)(?!memorydb:)[a-zA-Z0-9 _\\.\\/=+:\\-@]{1,128}$",
          "minLength": 1,
          "maxLength": 128
        },
        "value": {
          "description": "The value for the tag. You can specify a value that is 1 to 256 Unicode characters in length and cannot be prefixed with aws: or memorydb:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -.",
          "type": "string",
          "pattern": "^(?!aws:)(?!memorydb:)[a-zA-Z0-9 _\\.\\/=+:\\-@]{1,256}$",
          "minLength": 1,
          "maxLength": 256
        }
      },
      "required": [
        "value",
        "key"
      ]
    }
  },
  "properties": {
    "status": {
      "description": "Indicates the user status. Can be \"active\", \"modifying\" or \"deleting\".",
      "type": "string"
    },
    "user_name": {
      "description": "The name of the user.",
      "pattern": "[a-z][a-z0-9\\\\-]*",
      "type": "string"
    },
    "access_string": {
      "description": "Access permissions string used for this user account.",
      "type": "string"
    },
    "authentication_mode": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "description": "Type of authentication strategy for this user.",
          "enum": [
            "password"
          ]
        },
        "passwords": {
          "type": "array",
          "$comment": "List of passwords.",
          "uniqueItems": true,
          "insertionOrder": true,
          "maxItems": 2,
          "minItems": 1,
          "items": {
            "type": "string"
          },
          "description": "Passwords used for this user account. You can create up to two passwords for each user."
        }
      },
      "additionalProperties": false
    },
    "arn": {
      "description": "The Amazon Resource Name (ARN) of the user account.",
      "type": "string"
    },
    "tags": {
      "description": "An array of key-value pairs to apply to this user.",
      "type": "array",
      "maxItems": 50,
      "uniqueItems": true,
      "insertionOrder": false,
      "items": {
        "$ref": "#/definitions/tag"
      }
    }
  },
  "additionalProperties": false,
  "required": [
    "user_name",
    "access_string",
    "authentication_mode"
  ],
  "readOnlyProperties": [
    "/properties/status",
    "/properties/arn"
  ],
  "createOnlyProperties": [
    "/properties/user_name"
  ],
  "writeOnlyProperties": [
    "/properties/authentication_mode",
    "/properties/access_string"
  ],
  "primaryIdentifier": [
    "/properties/user_name"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "memorydb:CreateUser",
        "memorydb:DescribeUsers",
        "memorydb:ListTags"
      ]
    },
    "read": {
      "permissions": [
        "memorydb:DescribeUsers",
        "memorydb:ListTags"
      ]
    },
    "update": {
      "permissions": [
        "memorydb:UpdateUser",
        "memorydb:DescribeUsers",
        "memorydb:ListTags",
        "memorydb:TagResource",
        "memorydb:UntagResource"
      ]
    },
    "delete": {
      "permissions": [
        "memorydb:DeleteUser",
        "memorydb:DescribeUsers"
      ]
    },
    "list": {
      "permissions": [
        "memorydb:DescribeUsers",
        "memorydb:ListTags"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}