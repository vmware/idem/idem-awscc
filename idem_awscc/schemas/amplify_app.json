{
  "typeName": "AWS::Amplify::App",
  "description": "The AWS::Amplify::App resource creates Apps in the Amplify Console. An App is a collection of branches.",
  "additionalProperties": false,
  "properties": {
    "access_token": {
      "type": "string",
      "minLength": 1,
      "maxLength": 255
    },
    "app_id": {
      "type": "string",
      "minLength": 1,
      "maxLength": 20,
      "pattern": "d[a-z0-9]+"
    },
    "app_name": {
      "type": "string",
      "minLength": 1,
      "maxLength": 255,
      "pattern": "(?s).+"
    },
    "arn": {
      "type": "string",
      "maxLength": 1000,
      "pattern": "(?s).*"
    },
    "auto_branch_creation_config": {
      "$ref": "#/definitions/auto_branch_creation_config"
    },
    "basic_auth_config": {
      "$ref": "#/definitions/basic_auth_config"
    },
    "build_spec": {
      "type": "string",
      "minLength": 1,
      "maxLength": 25000,
      "pattern": "(?s).+"
    },
    "custom_headers": {
      "type": "string",
      "minLength": 0,
      "maxLength": 25000,
      "pattern": "(?s).*"
    },
    "custom_rules": {
      "type": "array",
      "uniqueItems": false,
      "items": {
        "$ref": "#/definitions/custom_rule"
      }
    },
    "default_domain": {
      "type": "string",
      "minLength": 0,
      "maxLength": 1000
    },
    "description": {
      "type": "string",
      "maxLength": 1000,
      "pattern": "(?s).*"
    },
    "enable_branch_auto_deletion": {
      "type": "boolean"
    },
    "environment_variables": {
      "type": "array",
      "uniqueItems": false,
      "items": {
        "$ref": "#/definitions/environment_variable"
      }
    },
    "iam_service_role": {
      "type": "string",
      "minLength": 1,
      "maxLength": 1000,
      "pattern": "(?s).*"
    },
    "name": {
      "type": "string",
      "minLength": 1,
      "maxLength": 255,
      "pattern": "(?s).+"
    },
    "oauth_token": {
      "type": "string",
      "maxLength": 1000,
      "pattern": "(?s).*"
    },
    "repository": {
      "type": "string",
      "pattern": "(?s).*"
    },
    "tags": {
      "type": "array",
      "uniqueItems": false,
      "items": {
        "$ref": "#/definitions/tag"
      }
    }
  },
  "definitions": {
    "auto_branch_creation_config": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "auto_branch_creation_patterns": {
          "type": "array",
          "uniqueItems": false,
          "items": {
            "type": "string",
            "minLength": 1,
            "maxLength": 2048
          }
        },
        "basic_auth_config": {
          "$ref": "#/definitions/basic_auth_config"
        },
        "build_spec": {
          "type": "string",
          "minLength": 1,
          "maxLength": 25000
        },
        "enable_auto_branch_creation": {
          "type": "boolean"
        },
        "enable_auto_build": {
          "type": "boolean"
        },
        "enable_performance_mode": {
          "type": "boolean"
        },
        "enable_pull_request_preview": {
          "type": "boolean"
        },
        "environment_variables": {
          "type": "array",
          "uniqueItems": false,
          "items": {
            "$ref": "#/definitions/environment_variable"
          }
        },
        "pull_request_environment_name": {
          "type": "string",
          "maxLength": 20,
          "pattern": "(?s).*"
        },
        "stage": {
          "type": "string",
          "enum": [
            "EXPERIMENTAL",
            "BETA",
            "PULL_REQUEST",
            "PRODUCTION",
            "DEVELOPMENT"
          ]
        }
      }
    },
    "basic_auth_config": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "enable_basic_auth": {
          "type": "boolean"
        },
        "username": {
          "type": "string",
          "minLength": 1,
          "maxLength": 255
        },
        "password": {
          "type": "string",
          "minLength": 1,
          "maxLength": 255
        }
      }
    },
    "custom_rule": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "condition": {
          "type": "string",
          "minLength": 0,
          "maxLength": 2048,
          "pattern": "(?s).*"
        },
        "status": {
          "type": "string",
          "minLength": 3,
          "maxLength": 7,
          "pattern": ".{3,7}"
        },
        "target": {
          "type": "string",
          "minLength": 1,
          "maxLength": 2048,
          "pattern": "(?s).+"
        },
        "source": {
          "type": "string",
          "minLength": 1,
          "maxLength": 2048,
          "pattern": "(?s).+"
        }
      },
      "required": [
        "target",
        "source"
      ]
    },
    "environment_variable": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "name": {
          "type": "string",
          "maxLength": 255,
          "pattern": "(?s).*"
        },
        "value": {
          "type": "string",
          "maxLength": 5500,
          "pattern": "(?s).*"
        }
      },
      "required": [
        "name",
        "value"
      ]
    },
    "tag": {
      "type": "object",
      "additionalProperties": false,
      "insertionOrder": false,
      "properties": {
        "key": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128,
          "pattern": "^(?!aws:)[a-zA-Z+-=._:/]+$"
        },
        "value": {
          "type": "string",
          "minLength": 0,
          "maxLength": 256
        }
      },
      "required": [
        "key",
        "value"
      ]
    }
  },
  "required": [
    "name"
  ],
  "primaryIdentifier": [
    "/properties/arn"
  ],
  "readOnlyProperties": [
    "/properties/app_id",
    "/properties/app_name",
    "/properties/arn",
    "/properties/default_domain"
  ],
  "writeOnlyProperties": [
    "/properties/access_token",
    "/properties/basic_auth_config",
    "/properties/oauth_token",
    "/properties/auto_branch_creation_config"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "amplify:CreateApp",
        "amplify:TagResource",
        "codecommit:GetRepository",
        "codecommit:PutRepositoryTriggers",
        "codecommit:GetRepositoryTriggers",
        "sns:CreateTopic",
        "sns:Subscribe",
        "iam:PassRole"
      ]
    },
    "delete": {
      "permissions": [
        "amplify:DeleteApp",
        "codecommit:GetRepository",
        "codecommit:GetRepositoryTriggers",
        "sns:Unsubscribe",
        "iam:PassRole"
      ]
    },
    "list": {
      "permissions": [
        "amplify:ListApps",
        "amplify:ListTagsForResource",
        "iam:PassRole"
      ]
    },
    "read": {
      "permissions": [
        "amplify:GetApp",
        "amplify:ListTagsForResource",
        "codecommit:GetRepository",
        "codecommit:GetRepositoryTriggers",
        "iam:PassRole"
      ]
    },
    "update": {
      "permissions": [
        "amplify:UpdateApp",
        "amplify:ListTagsForResource",
        "amplify:TagResource",
        "amplify:UntagResource",
        "codecommit:GetRepository",
        "codecommit:PutRepositoryTriggers",
        "codecommit:GetRepositoryTriggers",
        "sns:CreateTopic",
        "sns:Subscribe",
        "sns:Unsubscribe",
        "iam:PassRole"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}