{
  "typeName": "AWS::DynamoDB::Table",
  "description": "Version: None. Resource Type definition for AWS::DynamoDB::Table",
  "additionalProperties": false,
  "properties": {
    "arn": {
      "type": "string"
    },
    "stream_arn": {
      "type": "string"
    },
    "attribute_definitions": {
      "type": "array",
      "uniqueItems": true,
      "items": {
        "$ref": "#/definitions/attribute_definition"
      }
    },
    "billing_mode": {
      "type": "string"
    },
    "global_secondary_indexes": {
      "type": "array",
      "uniqueItems": false,
      "items": {
        "$ref": "#/definitions/global_secondary_index"
      }
    },
    "key_schema": {
      "oneOf": [
        {
          "type": "array",
          "uniqueItems": true,
          "items": {
            "$ref": "#/definitions/key_schema"
          }
        },
        {
          "type": "object"
        }
      ]
    },
    "local_secondary_indexes": {
      "type": "array",
      "uniqueItems": false,
      "items": {
        "$ref": "#/definitions/local_secondary_index"
      }
    },
    "point_in_time_recovery_specification": {
      "$ref": "#/definitions/point_in_time_recovery_specification"
    },
    "table_class": {
      "type": "string"
    },
    "provisioned_throughput": {
      "$ref": "#/definitions/provisioned_throughput"
    },
    "sse_specification": {
      "$ref": "#/definitions/sse_specification"
    },
    "stream_specification": {
      "$ref": "#/definitions/stream_specification"
    },
    "table_name": {
      "type": "string"
    },
    "tags": {
      "type": "array",
      "uniqueItems": false,
      "items": {
        "$ref": "#/definitions/tag"
      }
    },
    "time_to_live_specification": {
      "$ref": "#/definitions/time_to_live_specification"
    },
    "contributor_insights_specification": {
      "$ref": "#/definitions/contributor_insights_specification"
    },
    "kinesis_stream_specification": {
      "$ref": "#/definitions/kinesis_stream_specification"
    },
    "import_source_specification": {
      "$ref": "#/definitions/import_source_specification"
    }
  },
  "propertyTransform": {
    "/properties/SSESpecification/KMSMasterKeyId": "$join([\"arn:(aws)[-]{0,1}[a-z]{0,2}[-]{0,1}[a-z]{0,3}:kms:[a-z]{2}[-]{1}[a-z]{3,10}[-]{0,1}[a-z]{0,4}[-]{1}[1-4]{1}:[0-9]{12}[:]{1}key\\/\", SSESpecification.KMSMasterKeyId])"
  },
  "definitions": {
    "stream_specification": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "stream_view_type": {
          "type": "string"
        }
      },
      "required": [
        "stream_view_type"
      ]
    },
    "deprecated_key_schema": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "hash_key_element": {
          "$ref": "#/definitions/deprecated_hash_key_element"
        }
      },
      "required": [
        "hash_key_element"
      ]
    },
    "deprecated_hash_key_element": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "attribute_type": {
          "type": "string"
        },
        "attribute_name": {
          "type": "string"
        }
      },
      "required": [
        "attribute_type",
        "attribute_name"
      ]
    },
    "key_schema": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "attribute_name": {
          "type": "string"
        },
        "key_type": {
          "type": "string"
        }
      },
      "required": [
        "key_type",
        "attribute_name"
      ]
    },
    "point_in_time_recovery_specification": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "point_in_time_recovery_enabled": {
          "type": "boolean"
        }
      }
    },
    "kinesis_stream_specification": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "stream_arn": {
          "type": "string"
        }
      },
      "required": [
        "stream_arn"
      ]
    },
    "time_to_live_specification": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "attribute_name": {
          "type": "string"
        },
        "enabled": {
          "type": "boolean"
        }
      },
      "required": [
        "enabled",
        "attribute_name"
      ]
    },
    "local_secondary_index": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "index_name": {
          "type": "string"
        },
        "key_schema": {
          "type": "array",
          "uniqueItems": true,
          "items": {
            "$ref": "#/definitions/key_schema"
          }
        },
        "projection": {
          "$ref": "#/definitions/projection"
        }
      },
      "required": [
        "index_name",
        "projection",
        "key_schema"
      ]
    },
    "global_secondary_index": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "index_name": {
          "type": "string"
        },
        "key_schema": {
          "type": "array",
          "uniqueItems": true,
          "items": {
            "$ref": "#/definitions/key_schema"
          }
        },
        "projection": {
          "$ref": "#/definitions/projection"
        },
        "provisioned_throughput": {
          "$ref": "#/definitions/provisioned_throughput"
        },
        "contributor_insights_specification": {
          "$ref": "#/definitions/contributor_insights_specification"
        }
      },
      "required": [
        "index_name",
        "projection",
        "key_schema"
      ]
    },
    "sse_specification": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "kms_master_key_id": {
          "type": "string"
        },
        "sse_enabled": {
          "type": "boolean"
        },
        "sse_type": {
          "type": "string"
        }
      },
      "required": [
        "sse_enabled"
      ]
    },
    "attribute_definition": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "attribute_name": {
          "type": "string"
        },
        "attribute_type": {
          "type": "string"
        }
      },
      "required": [
        "attribute_name",
        "attribute_type"
      ]
    },
    "provisioned_throughput": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "read_capacity_units": {
          "type": "integer"
        },
        "write_capacity_units": {
          "type": "integer"
        }
      },
      "required": [
        "write_capacity_units",
        "read_capacity_units"
      ]
    },
    "tag": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "key": {
          "type": "string"
        },
        "value": {
          "type": "string"
        }
      },
      "required": [
        "value",
        "key"
      ]
    },
    "projection": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "non_key_attributes": {
          "type": "array",
          "uniqueItems": false,
          "items": {
            "type": "string"
          }
        },
        "projection_type": {
          "type": "string"
        }
      }
    },
    "contributor_insights_specification": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "enabled": {
          "type": "boolean"
        }
      },
      "required": [
        "enabled"
      ]
    },
    "import_source_specification": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "s3_bucket_source": {
          "$ref": "#/definitions/s3_bucket_source"
        },
        "input_format": {
          "type": "string"
        },
        "input_format_options": {
          "$ref": "#/definitions/input_format_options"
        },
        "input_compression_type": {
          "type": "string"
        }
      },
      "required": [
        "s3_bucket_source",
        "input_format"
      ]
    },
    "s3_bucket_source": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "s3_bucket_owner": {
          "type": "string"
        },
        "s3_bucket": {
          "type": "string"
        },
        "s3_key_prefix": {
          "type": "string"
        }
      },
      "required": [
        "s3_bucket"
      ]
    },
    "input_format_options": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "csv": {
          "$ref": "#/definitions/csv"
        }
      }
    },
    "csv": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "header_list": {
          "type": "array",
          "uniqueItems": true,
          "items": {
            "type": "string"
          }
        },
        "delimiter": {
          "type": "string"
        }
      }
    }
  },
  "tagging": {
    "taggable": true,
    "tagOnCreate": true,
    "tagUpdatable": true,
    "cloudFormationSystemTags": false,
    "tagProperty": "/properties/Tags"
  },
  "required": [
    "key_schema"
  ],
  "readOnlyProperties": [
    "/properties/arn",
    "/properties/stream_arn"
  ],
  "createOnlyProperties": [
    "/properties/table_name",
    "/properties/import_source_specification"
  ],
  "primaryIdentifier": [
    "/properties/table_name"
  ],
  "writeOnlyProperties": [
    "/properties/import_source_specification"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "dynamodb:CreateTable",
        "dynamodb:DescribeImport",
        "dynamodb:DescribeTable",
        "dynamodb:DescribeTimeToLive",
        "dynamodb:UpdateTimeToLive",
        "dynamodb:UpdateContributorInsights",
        "dynamodb:UpdateContinuousBackups",
        "dynamodb:DescribeContinuousBackups",
        "dynamodb:DescribeContributorInsights",
        "dynamodb:EnableKinesisStreamingDestination",
        "dynamodb:DisableKinesisStreamingDestination",
        "dynamodb:DescribeKinesisStreamingDestination",
        "dynamodb:ImportTable",
        "dynamodb:ListTagsOfResource",
        "dynamodb:TagResource",
        "dynamodb:UpdateTable",
        "kinesis:DescribeStream",
        "kinesis:PutRecords",
        "iam:CreateServiceLinkedRole",
        "kms:CreateGrant",
        "kms:Decrypt",
        "kms:Describe*",
        "kms:Encrypt",
        "kms:Get*",
        "kms:List*",
        "kms:RevokeGrant",
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams",
        "logs:PutLogEvents",
        "logs:PutRetentionPolicy",
        "s3:GetObject",
        "s3:GetObjectMetadata",
        "s3:ListBucket"
      ],
      "timeoutInMinutes": 720
    },
    "read": {
      "permissions": [
        "dynamodb:DescribeTable",
        "dynamodb:DescribeContinuousBackups",
        "dynamodb:DescribeContributorInsights"
      ]
    },
    "update": {
      "permissions": [
        "dynamodb:UpdateTable",
        "dynamodb:DescribeTable",
        "dynamodb:DescribeTimeToLive",
        "dynamodb:UpdateTimeToLive",
        "dynamodb:UpdateContinuousBackups",
        "dynamodb:UpdateContributorInsights",
        "dynamodb:DescribeContinuousBackups",
        "dynamodb:DescribeKinesisStreamingDestination",
        "dynamodb:ListTagsOfResource",
        "dynamodb:TagResource",
        "dynamodb:UntagResource",
        "dynamodb:DescribeContributorInsights",
        "dynamodb:EnableKinesisStreamingDestination",
        "dynamodb:DisableKinesisStreamingDestination",
        "kinesis:DescribeStream",
        "kinesis:PutRecords",
        "iam:CreateServiceLinkedRole",
        "kms:CreateGrant",
        "kms:Describe*",
        "kms:Get*",
        "kms:List*",
        "kms:RevokeGrant"
      ],
      "timeoutInMinutes": 720
    },
    "delete": {
      "permissions": [
        "dynamodb:DeleteTable",
        "dynamodb:DescribeTable"
      ],
      "timeoutInMinutes": 720
    },
    "list": {
      "permissions": [
        "dynamodb:ListTables"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}