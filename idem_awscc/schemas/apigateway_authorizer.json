{
  "typeName": "AWS::ApiGateway::Authorizer",
  "description": "Represents an authorization layer for methods. If enabled on a method, API Gateway will activate the authorizer when a client calls the method.",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-apigateway.git",
  "properties": {
    "rest_api_id": {
      "description": "The identifier of the API.",
      "type": "string"
    },
    "authorizer_id": {
      "type": "string"
    },
    "auth_type": {
      "description": "Optional customer-defined field, used in OpenAPI imports and exports without functional impact.",
      "type": "string"
    },
    "authorizer_credentials": {
      "description": "Specifies the required credentials as an IAM role for API Gateway to invoke the authorizer.",
      "type": "string"
    },
    "authorizer_result_ttl_in_seconds": {
      "description": "The TTL in seconds of cached authorizer results.",
      "type": "integer"
    },
    "authorizer_uri": {
      "description": "Specifies the authorizer's Uniform Resource Identifier (URI).",
      "type": "string"
    },
    "identity_source": {
      "description": "The identity source for which authorization is requested.",
      "type": "string"
    },
    "identity_validation_expression": {
      "description": "A validation expression for the incoming identity token.",
      "type": "string"
    },
    "name": {
      "description": "The name of the authorizer.",
      "type": "string"
    },
    "provider_ar_ns": {
      "description": "A list of the Amazon Cognito user pool ARNs for the COGNITO_USER_POOLS authorizer.",
      "type": "array",
      "uniqueItems": true,
      "items": {
        "type": "string"
      },
      "insertionOrder": false
    },
    "type": {
      "description": "The authorizer type.",
      "type": "string"
    }
  },
  "taggable": false,
  "additionalProperties": false,
  "required": [
    "rest_api_id",
    "type",
    "name"
  ],
  "createOnlyProperties": [
    "/properties/rest_api_id"
  ],
  "primaryIdentifier": [
    "/properties/rest_api_id",
    "/properties/authorizer_id"
  ],
  "readOnlyProperties": [
    "/properties/authorizer_id"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "apigateway:POST"
      ]
    },
    "read": {
      "permissions": [
        "apigateway:GET"
      ]
    },
    "update": {
      "permissions": [
        "apigateway:GET",
        "apigateway:PATCH"
      ]
    },
    "delete": {
      "permissions": [
        "apigateway:DELETE"
      ]
    },
    "list": {
      "permissions": [
        "apigateway:GET"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}