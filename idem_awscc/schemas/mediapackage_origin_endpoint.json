{
  "typeName": "AWS::MediaPackage::OriginEndpoint",
  "description": "Resource schema for AWS::MediaPackage::OriginEndpoint",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-rpdk.git",
  "properties": {
    "arn": {
      "description": "The Amazon Resource Name (ARN) assigned to the OriginEndpoint.",
      "type": "string"
    },
    "url": {
      "description": "The URL of the packaged OriginEndpoint for consumption.",
      "type": "string"
    },
    "id": {
      "description": "The ID of the OriginEndpoint.",
      "type": "string",
      "pattern": "\\A[0-9a-zA-Z-_]+\\Z",
      "minLength": 1,
      "maxLength": 256
    },
    "channel_id": {
      "description": "The ID of the Channel the OriginEndpoint is associated with.",
      "type": "string"
    },
    "description": {
      "description": "A short text description of the OriginEndpoint.",
      "type": "string"
    },
    "whitelist": {
      "description": "A list of source IP CIDR blocks that will be allowed to access the OriginEndpoint.",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "startover_window_seconds": {
      "description": "Maximum duration (seconds) of content to retain for startover playback. If not specified, startover playback will be disabled for the OriginEndpoint.",
      "type": "integer"
    },
    "time_delay_seconds": {
      "description": "Amount of delay (seconds) to enforce on the playback of live content. If not specified, there will be no time delay in effect for the OriginEndpoint.",
      "type": "integer"
    },
    "manifest_name": {
      "description": "A short string appended to the end of the OriginEndpoint URL.",
      "type": "string"
    },
    "origination": {
      "description": "Control whether origination of video is allowed for this OriginEndpoint. If set to ALLOW, the OriginEndpoint may by requested, pursuant to any other form of access control. If set to DENY, the OriginEndpoint may not be requested. This can be helpful for Live to VOD harvesting, or for temporarily disabling origination",
      "type": "string",
      "enum": [
        "ALLOW",
        "DENY"
      ]
    },
    "authorization": {
      "$ref": "#/definitions/authorization"
    },
    "hls_package": {
      "$ref": "#/definitions/hls_package"
    },
    "dash_package": {
      "$ref": "#/definitions/dash_package"
    },
    "mss_package": {
      "$ref": "#/definitions/mss_package"
    },
    "cmaf_package": {
      "$ref": "#/definitions/cmaf_package"
    },
    "tags": {
      "description": "A collection of tags associated with a resource",
      "type": "array",
      "uniqueItems": true,
      "items": {
        "$ref": "#/definitions/tag"
      }
    }
  },
  "definitions": {
    "mss_package": {
      "description": "A Microsoft Smooth Streaming (MSS) packaging configuration.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "manifest_window_seconds": {
          "description": "The time window (in seconds) contained in each manifest.",
          "type": "integer"
        },
        "segment_duration_seconds": {
          "description": "The duration (in seconds) of each segment.",
          "type": "integer"
        },
        "encryption": {
          "$ref": "#/definitions/mss_encryption"
        },
        "stream_selection": {
          "$ref": "#/definitions/stream_selection"
        }
      }
    },
    "mss_encryption": {
      "description": "A Microsoft Smooth Streaming (MSS) encryption configuration.",
      "type": "object",
      "additionalProperties": false,
      "required": [
        "speke_key_provider"
      ],
      "properties": {
        "speke_key_provider": {
          "$ref": "#/definitions/speke_key_provider"
        }
      }
    },
    "dash_package": {
      "description": "A Dynamic Adaptive Streaming over HTTP (DASH) packaging configuration.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "segment_duration_seconds": {
          "description": "Duration (in seconds) of each segment. Actual segments will be rounded to the nearest multiple of the source segment duration.",
          "type": "integer"
        },
        "manifest_window_seconds": {
          "description": "Time window (in seconds) contained in each manifest.",
          "type": "integer"
        },
        "profile": {
          "description": "The Dynamic Adaptive Streaming over HTTP (DASH) profile type.  When set to \"HBBTV_1_5\", HbbTV 1.5 compliant output is enabled.",
          "type": "string",
          "enum": [
            "NONE",
            "HBBTV_1_5",
            "HYBRIDCAST",
            "DVB_DASH_2014"
          ]
        },
        "min_update_period_seconds": {
          "description": "Minimum duration (in seconds) between potential changes to the Dynamic Adaptive Streaming over HTTP (DASH) Media Presentation Description (MPD).",
          "type": "integer"
        },
        "min_buffer_time_seconds": {
          "description": "Minimum duration (in seconds) that a player will buffer media before starting the presentation.",
          "type": "integer"
        },
        "suggested_presentation_delay_seconds": {
          "description": "Duration (in seconds) to delay live content before presentation.",
          "type": "integer"
        },
        "period_triggers": {
          "description": "A list of triggers that controls when the outgoing Dynamic Adaptive Streaming over HTTP (DASH) Media Presentation Description (MPD) will be partitioned into multiple periods. If empty, the content will not be partitioned into more than one period. If the list contains \"ADS\", new periods will be created where the Channel source contains SCTE-35 ad markers.",
          "type": "array",
          "items": {
            "type": "string",
            "enum": [
              "ADS"
            ]
          }
        },
        "include_iframe_only_stream": {
          "description": "When enabled, an I-Frame only stream will be included in the output.",
          "type": "boolean"
        },
        "manifest_layout": {
          "description": "Determines the position of some tags in the Media Presentation Description (MPD).  When set to FULL, elements like SegmentTemplate and ContentProtection are included in each Representation.  When set to COMPACT, duplicate elements are combined and presented at the AdaptationSet level.",
          "type": "string",
          "enum": [
            "FULL",
            "COMPACT"
          ]
        },
        "segment_template_format": {
          "description": "Determines the type of SegmentTemplate included in the Media Presentation Description (MPD).  When set to NUMBER_WITH_TIMELINE, a full timeline is presented in each SegmentTemplate, with $Number$ media URLs.  When set to TIME_WITH_TIMELINE, a full timeline is presented in each SegmentTemplate, with $Time$ media URLs. When set to NUMBER_WITH_DURATION, only a duration is included in each SegmentTemplate, with $Number$ media URLs.",
          "type": "string",
          "enum": [
            "NUMBER_WITH_TIMELINE",
            "TIME_WITH_TIMELINE",
            "NUMBER_WITH_DURATION"
          ]
        },
        "ad_triggers": {
          "description": "A list of SCTE-35 message types that are treated as ad markers in the output.  If empty, no ad markers are output.  Specify multiple items to create ad markers for all of the included message types.",
          "type": "array",
          "items": {
            "type": "string",
            "enum": [
              "SPLICE_INSERT",
              "BREAK",
              "PROVIDER_ADVERTISEMENT",
              "DISTRIBUTOR_ADVERTISEMENT",
              "PROVIDER_PLACEMENT_OPPORTUNITY",
              "DISTRIBUTOR_PLACEMENT_OPPORTUNITY",
              "PROVIDER_OVERLAY_PLACEMENT_OPPORTUNITY",
              "DISTRIBUTOR_OVERLAY_PLACEMENT_OPPORTUNITY"
            ]
          }
        },
        "ads_on_delivery_restrictions": {
          "$ref": "#/definitions/ads_on_delivery_restrictions"
        },
        "encryption": {
          "$ref": "#/definitions/dash_encryption"
        },
        "stream_selection": {
          "$ref": "#/definitions/stream_selection"
        },
        "utc_timing": {
          "description": "Determines the type of UTCTiming included in the Media Presentation Description (MPD)",
          "type": "string",
          "enum": [
            "HTTP-XSDATE",
            "HTTP-ISO",
            "HTTP-HEAD",
            "NONE"
          ]
        },
        "utc_timing_uri": {
          "description": "Specifies the value attribute of the UTCTiming field when utcTiming is set to HTTP-ISO, HTTP-HEAD or HTTP-XSDATE",
          "type": "string"
        }
      }
    },
    "dash_encryption": {
      "description": "A Dynamic Adaptive Streaming over HTTP (DASH) encryption configuration.",
      "type": "object",
      "additionalProperties": false,
      "required": [
        "speke_key_provider"
      ],
      "properties": {
        "key_rotation_interval_seconds": {
          "description": "Time (in seconds) between each encryption key rotation.",
          "type": "integer"
        },
        "speke_key_provider": {
          "$ref": "#/definitions/speke_key_provider"
        }
      }
    },
    "authorization": {
      "description": "CDN Authorization credentials",
      "type": "object",
      "additionalProperties": false,
      "required": [
        "secrets_role_arn",
        "cdn_identifier_secret"
      ],
      "properties": {
        "secrets_role_arn": {
          "description": "The Amazon Resource Name (ARN) for the IAM role that allows MediaPackage to communicate with AWS Secrets Manager.",
          "type": "string"
        },
        "cdn_identifier_secret": {
          "description": "The Amazon Resource Name (ARN) for the secret in Secrets Manager that your Content Distribution Network (CDN) uses for authorization to access your endpoint.",
          "type": "string"
        }
      }
    },
    "hls_package": {
      "description": "An HTTP Live Streaming (HLS) packaging configuration.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "segment_duration_seconds": {
          "description": "Duration (in seconds) of each fragment. Actual fragments will be rounded to the nearest multiple of the source fragment duration.",
          "type": "integer"
        },
        "playlist_window_seconds": {
          "description": "Time window (in seconds) contained in each parent manifest.",
          "type": "integer"
        },
        "playlist_type": {
          "description": "The HTTP Live Streaming (HLS) playlist type. When either \"EVENT\" or \"VOD\" is specified, a corresponding EXT-X-PLAYLIST-TYPE entry will be included in the media playlist.",
          "type": "string",
          "enum": [
            "NONE",
            "EVENT",
            "VOD"
          ]
        },
        "ad_markers": {
          "description": "This setting controls how ad markers are included in the packaged OriginEndpoint. \"NONE\" will omit all SCTE-35 ad markers from the output. \"PASSTHROUGH\" causes the manifest to contain a copy of the SCTE-35 ad markers (comments) taken directly from the input HTTP Live Streaming (HLS) manifest. \"SCTE35_ENHANCED\" generates ad markers and blackout tags based on SCTE-35 messages in the input source. \"DATERANGE\" inserts EXT-X-DATERANGE tags to signal ad and program transition events in HLS and CMAF manifests. For this option, you must set a programDateTimeIntervalSeconds value that is greater than 0.",
          "type": "string",
          "enum": [
            "NONE",
            "SCTE35_ENHANCED",
            "PASSTHROUGH",
            "DATERANGE"
          ]
        },
        "ad_triggers": {
          "description": "A list of SCTE-35 message types that are treated as ad markers in the output.  If empty, no ad markers are output.  Specify multiple items to create ad markers for all of the included message types.",
          "type": "array",
          "items": {
            "type": "string",
            "enum": [
              "SPLICE_INSERT",
              "BREAK",
              "PROVIDER_ADVERTISEMENT",
              "DISTRIBUTOR_ADVERTISEMENT",
              "PROVIDER_PLACEMENT_OPPORTUNITY",
              "DISTRIBUTOR_PLACEMENT_OPPORTUNITY",
              "PROVIDER_OVERLAY_PLACEMENT_OPPORTUNITY",
              "DISTRIBUTOR_OVERLAY_PLACEMENT_OPPORTUNITY"
            ]
          }
        },
        "ads_on_delivery_restrictions": {
          "$ref": "#/definitions/ads_on_delivery_restrictions"
        },
        "program_date_time_interval_seconds": {
          "description": "The interval (in seconds) between each EXT-X-PROGRAM-DATE-TIME tag inserted into manifests. Additionally, when an interval is specified ID3Timed Metadata messages will be generated every 5 seconds using the ingest time of the content. If the interval is not specified, or set to 0, then no EXT-X-PROGRAM-DATE-TIME tags will be inserted into manifests and no ID3Timed Metadata messages will be generated. Note that irrespective of this parameter, if any ID3 Timed Metadata is found in HTTP Live Streaming (HLS) input, it will be passed through to HLS output.",
          "type": "integer"
        },
        "include_iframe_only_stream": {
          "description": "When enabled, an I-Frame only stream will be included in the output.",
          "type": "boolean"
        },
        "use_audio_rendition_group": {
          "description": "When enabled, audio streams will be placed in rendition groups in the output.",
          "type": "boolean"
        },
        "encryption": {
          "$ref": "#/definitions/hls_encryption"
        },
        "stream_selection": {
          "$ref": "#/definitions/stream_selection"
        }
      }
    },
    "hls_encryption": {
      "description": "An HTTP Live Streaming (HLS) encryption configuration.",
      "type": "object",
      "additionalProperties": false,
      "required": [
        "speke_key_provider"
      ],
      "properties": {
        "encryption_method": {
          "description": "The encryption method to use.",
          "type": "string",
          "enum": [
            "AES_128",
            "SAMPLE_AES"
          ]
        },
        "constant_initialization_vector": {
          "description": "A constant initialization vector for encryption (optional). When not specified the initialization vector will be periodically rotated.",
          "type": "string"
        },
        "key_rotation_interval_seconds": {
          "description": "Interval (in seconds) between each encryption key rotation.",
          "type": "integer"
        },
        "repeat_ext_xkey": {
          "description": "When enabled, the EXT-X-KEY tag will be repeated in output manifests.",
          "type": "boolean"
        },
        "speke_key_provider": {
          "$ref": "#/definitions/speke_key_provider"
        }
      }
    },
    "cmaf_package": {
      "description": "A Common Media Application Format (CMAF) packaging configuration.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "segment_duration_seconds": {
          "description": "Duration (in seconds) of each segment. Actual segments will be rounded to the nearest multiple of the source segment duration.",
          "type": "integer"
        },
        "segment_prefix": {
          "description": "An optional custom string that is prepended to the name of each segment. If not specified, it defaults to the ChannelId.",
          "type": "string"
        },
        "encryption": {
          "$ref": "#/definitions/cmaf_encryption"
        },
        "stream_selection": {
          "$ref": "#/definitions/stream_selection"
        },
        "hls_manifests": {
          "description": "A list of HLS manifest configurations",
          "type": "array",
          "items": {
            "$ref": "#/definitions/hls_manifest"
          }
        }
      }
    },
    "cmaf_encryption": {
      "description": "A Common Media Application Format (CMAF) encryption configuration.",
      "type": "object",
      "additionalProperties": false,
      "required": [
        "speke_key_provider"
      ],
      "properties": {
        "key_rotation_interval_seconds": {
          "description": "Time (in seconds) between each encryption key rotation.",
          "type": "integer"
        },
        "speke_key_provider": {
          "$ref": "#/definitions/speke_key_provider"
        },
        "constant_initialization_vector": {
          "description": "An optional 128-bit, 16-byte hex value represented by a 32-character string, used in conjunction with the key for encrypting blocks. If you don't specify a value, then MediaPackage creates the constant initialization vector (IV).",
          "type": "string",
          "pattern": "\\A[0-9a-fA-F]+\\Z",
          "minLength": 32,
          "maxLength": 32
        },
        "encryption_method": {
          "description": "The encryption method used",
          "type": "string",
          "enum": [
            "SAMPLE_AES",
            "AES_CTR"
          ]
        }
      }
    },
    "hls_manifest": {
      "description": "A HTTP Live Streaming (HLS) manifest configuration.",
      "type": "object",
      "additionalProperties": false,
      "required": [
        "id"
      ],
      "properties": {
        "id": {
          "description": "The ID of the manifest. The ID must be unique within the OriginEndpoint and it cannot be changed after it is created.",
          "type": "string"
        },
        "manifest_name": {
          "description": "An optional short string appended to the end of the OriginEndpoint URL. If not specified, defaults to the manifestName for the OriginEndpoint.",
          "type": "string"
        },
        "url": {
          "description": "The URL of the packaged OriginEndpoint for consumption.",
          "type": "string"
        },
        "playlist_window_seconds": {
          "description": "Time window (in seconds) contained in each parent manifest.",
          "type": "integer"
        },
        "playlist_type": {
          "description": "The HTTP Live Streaming (HLS) playlist type. When either \"EVENT\" or \"VOD\" is specified, a corresponding EXT-X-PLAYLIST-TYPE entry will be included in the media playlist.",
          "type": "string",
          "enum": [
            "NONE",
            "EVENT",
            "VOD"
          ]
        },
        "ad_markers": {
          "description": "This setting controls how ad markers are included in the packaged OriginEndpoint. \"NONE\" will omit all SCTE-35 ad markers from the output. \"PASSTHROUGH\" causes the manifest to contain a copy of the SCTE-35 ad markers (comments) taken directly from the input HTTP Live Streaming (HLS) manifest. \"SCTE35_ENHANCED\" generates ad markers and blackout tags based on SCTE-35 messages in the input source. \"DATERANGE\" inserts EXT-X-DATERANGE tags to signal ad and program transition events in HLS and CMAF manifests. For this option, you must set a programDateTimeIntervalSeconds value that is greater than 0.",
          "type": "string",
          "enum": [
            "NONE",
            "SCTE35_ENHANCED",
            "PASSTHROUGH",
            "DATERANGE"
          ]
        },
        "program_date_time_interval_seconds": {
          "description": "The interval (in seconds) between each EXT-X-PROGRAM-DATE-TIME tag inserted into manifests. Additionally, when an interval is specified ID3Timed Metadata messages will be generated every 5 seconds using the ingest time of the content. If the interval is not specified, or set to 0, then no EXT-X-PROGRAM-DATE-TIME tags will be inserted into manifests and no ID3Timed Metadata messages will be generated. Note that irrespective of this parameter, if any ID3 Timed Metadata is found in HTTP Live Streaming (HLS) input, it will be passed through to HLS output.",
          "type": "integer"
        },
        "include_iframe_only_stream": {
          "description": "When enabled, an I-Frame only stream will be included in the output.",
          "type": "boolean"
        },
        "ad_triggers": {
          "description": "A list of SCTE-35 message types that are treated as ad markers in the output.  If empty, no ad markers are output.  Specify multiple items to create ad markers for all of the included message types.",
          "type": "array",
          "items": {
            "type": "string",
            "enum": [
              "SPLICE_INSERT",
              "BREAK",
              "PROVIDER_ADVERTISEMENT",
              "DISTRIBUTOR_ADVERTISEMENT",
              "PROVIDER_PLACEMENT_OPPORTUNITY",
              "DISTRIBUTOR_PLACEMENT_OPPORTUNITY",
              "PROVIDER_OVERLAY_PLACEMENT_OPPORTUNITY",
              "DISTRIBUTOR_OVERLAY_PLACEMENT_OPPORTUNITY"
            ]
          }
        },
        "ads_on_delivery_restrictions": {
          "$ref": "#/definitions/ads_on_delivery_restrictions"
        }
      }
    },
    "stream_selection": {
      "description": "A StreamSelection configuration.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "min_video_bits_per_second": {
          "description": "The minimum video bitrate (bps) to include in output.",
          "type": "integer"
        },
        "max_video_bits_per_second": {
          "description": "The maximum video bitrate (bps) to include in output.",
          "type": "integer"
        },
        "stream_order": {
          "description": "A directive that determines the order of streams in the output.",
          "type": "string",
          "enum": [
            "ORIGINAL",
            "VIDEO_BITRATE_ASCENDING",
            "VIDEO_BITRATE_DESCENDING"
          ]
        }
      }
    },
    "speke_key_provider": {
      "description": "A configuration for accessing an external Secure Packager and Encoder Key Exchange (SPEKE) service that will provide encryption keys.",
      "type": "object",
      "additionalProperties": false,
      "required": [
        "resource_id",
        "system_ids",
        "url",
        "role_arn"
      ],
      "properties": {
        "resource_id": {
          "description": "The resource ID to include in key requests.",
          "type": "string"
        },
        "system_ids": {
          "description": "The system IDs to include in key requests.",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "url": {
          "description": "The URL of the external key provider service.",
          "type": "string"
        },
        "role_arn": {
          "description": "An Amazon Resource Name (ARN) of an IAM role that AWS Elemental MediaPackage will assume when accessing the key provider service.",
          "type": "string"
        },
        "certificate_arn": {
          "description": "An Amazon Resource Name (ARN) of a Certificate Manager certificate that MediaPackage will use for enforcing secure end-to-end data transfer with the key provider service.",
          "type": "string"
        },
        "encryption_contract_configuration": {
          "$ref": "#/definitions/encryption_contract_configuration"
        }
      }
    },
    "encryption_contract_configuration": {
      "description": "The configuration to use for encrypting one or more content tracks separately for endpoints that use SPEKE 2.0.",
      "type": "object",
      "additionalProperties": false,
      "required": [
        "preset_speke20_audio",
        "preset_speke20_video"
      ],
      "properties": {
        "preset_speke20_audio": {
          "description": "A collection of audio encryption presets.",
          "type": "string",
          "enum": [
            "PRESET-AUDIO-1",
            "PRESET-AUDIO-2",
            "PRESET-AUDIO-3",
            "SHARED",
            "UNENCRYPTED"
          ]
        },
        "preset_speke20_video": {
          "description": "A collection of video encryption presets.",
          "type": "string",
          "enum": [
            "PRESET-VIDEO-1",
            "PRESET-VIDEO-2",
            "PRESET-VIDEO-3",
            "PRESET-VIDEO-4",
            "PRESET-VIDEO-5",
            "PRESET-VIDEO-6",
            "PRESET-VIDEO-7",
            "PRESET-VIDEO-8",
            "SHARED",
            "UNENCRYPTED"
          ]
        }
      }
    },
    "ads_on_delivery_restrictions": {
      "description": "This setting allows the delivery restriction flags on SCTE-35 segmentation descriptors to determine whether a message signals an ad.  Choosing \"NONE\" means no SCTE-35 messages become ads.  Choosing \"RESTRICTED\" means SCTE-35 messages of the types specified in AdTriggers that contain delivery restrictions will be treated as ads.  Choosing \"UNRESTRICTED\" means SCTE-35 messages of the types specified in AdTriggers that do not contain delivery restrictions will be treated as ads.  Choosing \"BOTH\" means all SCTE-35 messages of the types specified in AdTriggers will be treated as ads.  Note that Splice Insert messages do not have these flags and are always treated as ads if specified in AdTriggers.",
      "type": "string",
      "enum": [
        "NONE",
        "RESTRICTED",
        "UNRESTRICTED",
        "BOTH"
      ]
    },
    "tag": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "key": {
          "type": "string"
        },
        "value": {
          "type": "string"
        }
      },
      "required": [
        "value",
        "key"
      ]
    }
  },
  "additionalProperties": false,
  "required": [
    "id",
    "channel_id"
  ],
  "readOnlyProperties": [
    "/properties/arn",
    "/properties/url"
  ],
  "primaryIdentifier": [
    "/properties/id"
  ],
  "createOnlyProperties": [
    "/properties/id"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "mediapackage:CreateOriginEndpoint",
        "mediapackage:DescribeChannel",
        "mediapackage:TagResource",
        "iam:PassRole"
      ]
    },
    "read": {
      "permissions": [
        "mediapackage:DescribeOriginEndpoint"
      ]
    },
    "update": {
      "permissions": [
        "mediapackage:UpdateOriginEndpoint",
        "iam:PassRole"
      ]
    },
    "delete": {
      "permissions": [
        "mediapackage:DeleteOriginEndpoint"
      ]
    },
    "list": {
      "permissions": [
        "mediapackage:ListOriginEndpoints"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}