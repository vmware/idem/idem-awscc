{
  "typeName": "AWS::GlobalAccelerator::Accelerator",
  "description": "Resource Type definition for AWS::GlobalAccelerator::Accelerator",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-globalaccelerator",
  "definitions": {
    "tag": {
      "description": "Tag is a key-value pair associated with accelerator.",
      "type": "object",
      "properties": {
        "key": {
          "description": "Key of the tag. Value can be 1 to 127 characters.",
          "type": "string",
          "minLength": 1,
          "maxLength": 127
        },
        "value": {
          "description": "Value for the tag. Value can be 1 to 255 characters.",
          "type": "string",
          "minLength": 1,
          "maxLength": 255
        }
      },
      "required": [
        "value",
        "key"
      ],
      "additionalProperties": false
    },
    "ip_address": {
      "pattern": "^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$",
      "description": "An IPV4 address",
      "type": "string"
    }
  },
  "properties": {
    "name": {
      "description": "Name of accelerator.",
      "type": "string",
      "pattern": "^[a-zA-Z0-9_-]{0,64}$",
      "minLength": 1,
      "maxLength": 64
    },
    "ip_address_type": {
      "description": "IP Address type.",
      "type": "string",
      "default": "IPV4",
      "enum": [
        "IPV4",
        "DUAL_STACK"
      ]
    },
    "ip_addresses": {
      "description": "The IP addresses from BYOIP Prefix pool.",
      "default": null,
      "type": "array",
      "items": {
        "$ref": "#/definitions/ip_address"
      }
    },
    "enabled": {
      "description": "Indicates whether an accelerator is enabled. The value is true or false.",
      "default": true,
      "type": "boolean"
    },
    "dns_name": {
      "description": "The Domain Name System (DNS) name that Global Accelerator creates that points to your accelerator's static IPv4 addresses.",
      "type": "string"
    },
    "ipv4_addresses": {
      "description": "The IPv4 addresses assigned to the accelerator.",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "ipv6_addresses": {
      "description": "The IPv6 addresses assigned if the accelerator is dualstack",
      "default": null,
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "dual_stack_dns_name": {
      "description": "The Domain Name System (DNS) name that Global Accelerator creates that points to your accelerator's static IPv4 and IPv6 addresses.",
      "type": "string"
    },
    "accelerator_arn": {
      "description": "The Amazon Resource Name (ARN) of the accelerator.",
      "type": "string"
    },
    "tags": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/tag"
      }
    }
  },
  "required": [
    "name"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "globalaccelerator:CreateAccelerator",
        "globalaccelerator:DescribeAccelerator",
        "globalaccelerator:TagResource"
      ]
    },
    "read": {
      "permissions": [
        "globalaccelerator:DescribeAccelerator"
      ]
    },
    "update": {
      "permissions": [
        "globalaccelerator:UpdateAccelerator",
        "globalaccelerator:TagResource",
        "globalaccelerator:UntagResource"
      ]
    },
    "delete": {
      "permissions": [
        "globalaccelerator:UpdateAccelerator",
        "globalaccelerator:DeleteAccelerator"
      ]
    },
    "list": {
      "permissions": [
        "globalaccelerator:ListAccelerators"
      ]
    }
  },
  "readOnlyProperties": [
    "/properties/accelerator_arn",
    "/properties/dns_name",
    "/properties/ipv4_addresses",
    "/properties/ipv6_addresses",
    "/properties/dual_stack_dns_name"
  ],
  "primaryIdentifier": [
    "/properties/accelerator_arn"
  ],
  "additionalProperties": false,
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}