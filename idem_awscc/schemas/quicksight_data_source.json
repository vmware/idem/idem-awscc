{
  "typeName": "AWS::QuickSight::DataSource",
  "description": "Definition of the AWS::QuickSight::DataSource Resource Type.",
  "definitions": {
    "amazon_elasticsearch_parameters": {
      "type": "object",
      "description": "<p>Amazon Elasticsearch Service parameters.</p>",
      "properties": {
        "domain": {
          "type": "string",
          "maxLength": 64,
          "minLength": 1,
          "description": "<p>The Amazon Elasticsearch Service domain.</p>"
        }
      },
      "required": [
        "domain"
      ]
    },
    "amazon_open_search_parameters": {
      "type": "object",
      "description": "<p>Amazon OpenSearch Service parameters.</p>",
      "properties": {
        "domain": {
          "type": "string",
          "maxLength": 64,
          "minLength": 1,
          "description": "<p>The Amazon OpenSearch Service domain.</p>"
        }
      },
      "required": [
        "domain"
      ]
    },
    "athena_parameters": {
      "type": "object",
      "description": "<p>Amazon Athena parameters.</p>",
      "properties": {
        "work_group": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1,
          "description": "<p>The workgroup that Amazon Athena uses.</p>"
        }
      }
    },
    "aurora_parameters": {
      "type": "object",
      "description": "<p>Amazon Aurora parameters.</p>",
      "properties": {
        "port": {
          "type": "number",
          "maximum": 65535,
          "minimum": 1,
          "description": "<p>Port.</p>"
        },
        "database": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1,
          "description": "<p>Database.</p>"
        },
        "host": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1,
          "description": "<p>Host.</p>"
        }
      },
      "required": [
        "database",
        "host",
        "port"
      ]
    },
    "aurora_postgre_sql_parameters": {
      "type": "object",
      "description": "<p>Amazon Aurora with PostgreSQL compatibility parameters.</p>",
      "properties": {
        "port": {
          "type": "number",
          "maximum": 65535,
          "minimum": 1,
          "description": "<p>Port.</p>"
        },
        "database": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1,
          "description": "<p>Database.</p>"
        },
        "host": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1,
          "description": "<p>Host.</p>"
        }
      },
      "required": [
        "database",
        "host",
        "port"
      ]
    },
    "aws_iot_analytics_parameters": {
      "type": "object",
      "description": "<p>AWS IoT Analytics parameters.</p>",
      "properties": {
        "data_set_name": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1,
          "description": "<p>Dataset name.</p>"
        }
      },
      "required": [
        "data_set_name"
      ]
    },
    "credential_pair": {
      "type": "object",
      "description": "<p>The combination of user name and password that are used as credentials.</p>",
      "properties": {
        "alternate_data_source_parameters": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/data_source_parameters"
          },
          "maxItems": 50,
          "minItems": 1,
          "description": "<p>A set of alternate data source parameters that you want to share for these\n            credentials. The credentials are applied in tandem with the data source parameters when\n            you copy a data source by using a create or update request. The API operation compares\n            the <code>DataSourceParameters</code> structure that's in the request with the\n            structures in the <code>AlternateDataSourceParameters</code> allow list. If the\n            structures are an exact match, the request is allowed to use the new data source with\n            the existing credentials. If the <code>AlternateDataSourceParameters</code> list is\n            null, the <code>DataSourceParameters</code> originally used with these\n                <code>Credentials</code> is automatically allowed.</p>"
        },
        "username": {
          "type": "string",
          "maxLength": 64,
          "minLength": 1,
          "description": "<p>User name.</p>"
        },
        "password": {
          "type": "string",
          "maxLength": 1024,
          "minLength": 1,
          "description": "<p>Password.</p>"
        }
      },
      "required": [
        "password",
        "username"
      ]
    },
    "data_source_credentials": {
      "type": "object",
      "description": "<p>Data source credentials. This is a variant type structure. For this structure to be\n            valid, only one of the attributes can be non-null.</p>",
      "properties": {
        "copy_source_arn": {
          "type": "string",
          "pattern": "^arn:[-a-z0-9]*:quicksight:[-a-z0-9]*:[0-9]{12}:datasource/.+",
          "description": "<p>The Amazon Resource Name (ARN) of a data source that has the credential pair that you\n            want to use. When <code>CopySourceArn</code> is not null, the credential pair from the\n            data source in the ARN is used as the credentials for the\n            <code>DataSourceCredentials</code> structure.</p>"
        },
        "credential_pair": {
          "$ref": "#/definitions/credential_pair"
        }
      }
    },
    "data_source_error_info": {
      "type": "object",
      "description": "<p>Error information for the data source creation or update.</p>",
      "properties": {
        "type": {
          "$ref": "#/definitions/data_source_error_info_type"
        },
        "message": {
          "type": "string",
          "description": "<p>Error message.</p>"
        }
      }
    },
    "data_source_error_info_type": {
      "type": "string",
      "enum": [
        "ACCESS_DENIED",
        "COPY_SOURCE_NOT_FOUND",
        "TIMEOUT",
        "ENGINE_VERSION_NOT_SUPPORTED",
        "UNKNOWN_HOST",
        "GENERIC_SQL_FAILURE",
        "CONFLICT",
        "UNKNOWN"
      ]
    },
    "data_source_parameters": {
      "type": "object",
      "description": "<p>The parameters that Amazon QuickSight uses to connect to your underlying data source.\n            This is a variant type structure. For this structure to be valid, only one of the\n            attributes can be non-null.</p>",
      "properties": {
        "aurora_postgre_sql_parameters": {
          "$ref": "#/definitions/aurora_postgre_sql_parameters"
        },
        "teradata_parameters": {
          "$ref": "#/definitions/teradata_parameters"
        },
        "rds_parameters": {
          "$ref": "#/definitions/rds_parameters"
        },
        "athena_parameters": {
          "$ref": "#/definitions/athena_parameters"
        },
        "spark_parameters": {
          "$ref": "#/definitions/spark_parameters"
        },
        "maria_db_parameters": {
          "$ref": "#/definitions/maria_db_parameters"
        },
        "oracle_parameters": {
          "$ref": "#/definitions/oracle_parameters"
        },
        "presto_parameters": {
          "$ref": "#/definitions/presto_parameters"
        },
        "redshift_parameters": {
          "$ref": "#/definitions/redshift_parameters"
        },
        "my_sql_parameters": {
          "$ref": "#/definitions/my_sql_parameters"
        },
        "sql_server_parameters": {
          "$ref": "#/definitions/sql_server_parameters"
        },
        "snowflake_parameters": {
          "$ref": "#/definitions/snowflake_parameters"
        },
        "amazon_elasticsearch_parameters": {
          "$ref": "#/definitions/amazon_elasticsearch_parameters"
        },
        "amazon_open_search_parameters": {
          "$ref": "#/definitions/amazon_open_search_parameters"
        },
        "postgre_sql_parameters": {
          "$ref": "#/definitions/postgre_sql_parameters"
        },
        "aurora_parameters": {
          "$ref": "#/definitions/aurora_parameters"
        },
        "s3_parameters": {
          "$ref": "#/definitions/s3_parameters"
        }
      }
    },
    "data_source_type": {
      "type": "string",
      "enum": [
        "ADOBE_ANALYTICS",
        "AMAZON_ELASTICSEARCH",
        "AMAZON_OPENSEARCH",
        "ATHENA",
        "AURORA",
        "AURORA_POSTGRESQL",
        "AWS_IOT_ANALYTICS",
        "GITHUB",
        "JIRA",
        "MARIADB",
        "MYSQL",
        "ORACLE",
        "POSTGRESQL",
        "PRESTO",
        "REDSHIFT",
        "S3",
        "SALESFORCE",
        "SERVICENOW",
        "SNOWFLAKE",
        "SPARK",
        "SQLSERVER",
        "TERADATA",
        "TWITTER",
        "TIMESTREAM"
      ]
    },
    "manifest_file_location": {
      "type": "object",
      "description": "<p>Amazon S3 manifest file location.</p>",
      "properties": {
        "bucket": {
          "type": "string",
          "maxLength": 1024,
          "minLength": 1,
          "description": "<p>Amazon S3 bucket.</p>"
        },
        "key": {
          "type": "string",
          "maxLength": 1024,
          "minLength": 1,
          "description": "<p>Amazon S3 key that identifies an object.</p>"
        }
      },
      "required": [
        "bucket",
        "key"
      ]
    },
    "maria_db_parameters": {
      "type": "object",
      "description": "<p>MariaDB parameters.</p>",
      "properties": {
        "port": {
          "type": "number",
          "maximum": 65535,
          "minimum": 1,
          "description": "<p>Port.</p>"
        },
        "database": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1,
          "description": "<p>Database.</p>"
        },
        "host": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1,
          "description": "<p>Host.</p>"
        }
      },
      "required": [
        "database",
        "host",
        "port"
      ]
    },
    "my_sql_parameters": {
      "type": "object",
      "description": "<p>MySQL parameters.</p>",
      "properties": {
        "port": {
          "type": "number",
          "maximum": 65535,
          "minimum": 1,
          "description": "<p>Port.</p>"
        },
        "database": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1,
          "description": "<p>Database.</p>"
        },
        "host": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1,
          "description": "<p>Host.</p>"
        }
      },
      "required": [
        "database",
        "host",
        "port"
      ]
    },
    "oracle_parameters": {
      "type": "object",
      "properties": {
        "port": {
          "type": "number",
          "maximum": 65535,
          "minimum": 1
        },
        "database": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1
        },
        "host": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1
        }
      },
      "required": [
        "database",
        "host",
        "port"
      ]
    },
    "postgre_sql_parameters": {
      "type": "object",
      "description": "<p>PostgreSQL parameters.</p>",
      "properties": {
        "port": {
          "type": "number",
          "maximum": 65535,
          "minimum": 1,
          "description": "<p>Port.</p>"
        },
        "database": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1,
          "description": "<p>Database.</p>"
        },
        "host": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1,
          "description": "<p>Host.</p>"
        }
      },
      "required": [
        "database",
        "host",
        "port"
      ]
    },
    "presto_parameters": {
      "type": "object",
      "description": "<p>Presto parameters.</p>",
      "properties": {
        "port": {
          "type": "number",
          "maximum": 65535,
          "minimum": 1,
          "description": "<p>Port.</p>"
        },
        "host": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1,
          "description": "<p>Host.</p>"
        },
        "catalog": {
          "type": "string",
          "maxLength": 128,
          "minLength": 0,
          "description": "<p>Catalog.</p>"
        }
      },
      "required": [
        "catalog",
        "host",
        "port"
      ]
    },
    "rds_parameters": {
      "type": "object",
      "description": "<p>Amazon RDS parameters.</p>",
      "properties": {
        "instance_id": {
          "type": "string",
          "maxLength": 64,
          "minLength": 1,
          "description": "<p>Instance ID.</p>"
        },
        "database": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1,
          "description": "<p>Database.</p>"
        }
      },
      "required": [
        "database",
        "instance_id"
      ]
    },
    "redshift_parameters": {
      "type": "object",
      "description": "<p>Amazon Redshift parameters. The <code>ClusterId</code> field can be blank if\n            <code>Host</code> and <code>Port</code> are both set. The <code>Host</code> and\n            <code>Port</code> fields can be blank if the <code>ClusterId</code> field is set.</p>",
      "properties": {
        "cluster_id": {
          "type": "string",
          "maxLength": 64,
          "minLength": 1,
          "description": "<p>Cluster ID. This field can be blank if the <code>Host</code> and <code>Port</code> are\n            provided.</p>"
        },
        "port": {
          "type": "number",
          "maximum": 65535,
          "minimum": 0,
          "description": "<p>Port. This field can be blank if the <code>ClusterId</code> is provided.</p>"
        },
        "database": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1,
          "description": "<p>Database.</p>"
        },
        "host": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1,
          "description": "<p>Host. This field can be blank if <code>ClusterId</code> is provided.</p>"
        }
      },
      "required": [
        "database"
      ]
    },
    "resource_permission": {
      "type": "object",
      "description": "<p>Permission for the resource.</p>",
      "properties": {
        "actions": {
          "type": "array",
          "items": {
            "type": "string"
          },
          "maxItems": 16,
          "minItems": 1,
          "description": "<p>The IAM action to grant or revoke permissions on.</p>"
        },
        "principal": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1,
          "description": "<p>The Amazon Resource Name (ARN) of the principal. This can be one of the\n            following:</p>\n        <ul>\n            <li>\n                <p>The ARN of an Amazon QuickSight user or group associated with a data source or dataset. (This is common.)</p>\n            </li>\n            <li>\n                <p>The ARN of an Amazon QuickSight user, group, or namespace associated with an analysis, dashboard, template, or theme. (This is common.)</p>\n            </li>\n            <li>\n                <p>The ARN of an AWS account root: This is an IAM ARN rather than a QuickSight\n                    ARN. Use this option only to share resources (templates) across AWS accounts.\n                    (This is less common.) </p>\n            </li>\n         </ul>"
        }
      },
      "required": [
        "actions",
        "principal"
      ]
    },
    "resource_status": {
      "type": "string",
      "enum": [
        "CREATION_IN_PROGRESS",
        "CREATION_SUCCESSFUL",
        "CREATION_FAILED",
        "UPDATE_IN_PROGRESS",
        "UPDATE_SUCCESSFUL",
        "UPDATE_FAILED",
        "DELETED"
      ]
    },
    "s3_parameters": {
      "type": "object",
      "description": "<p>S3 parameters.</p>",
      "properties": {
        "manifest_file_location": {
          "$ref": "#/definitions/manifest_file_location"
        }
      },
      "required": [
        "manifest_file_location"
      ]
    },
    "snowflake_parameters": {
      "type": "object",
      "description": "<p>Snowflake parameters.</p>",
      "properties": {
        "warehouse": {
          "type": "string",
          "maxLength": 128,
          "minLength": 0,
          "description": "<p>Warehouse.</p>"
        },
        "database": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1,
          "description": "<p>Database.</p>"
        },
        "host": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1,
          "description": "<p>Host.</p>"
        }
      },
      "required": [
        "database",
        "host",
        "warehouse"
      ]
    },
    "spark_parameters": {
      "type": "object",
      "description": "<p>Spark parameters.</p>",
      "properties": {
        "port": {
          "type": "number",
          "maximum": 65535,
          "minimum": 1,
          "description": "<p>Port.</p>"
        },
        "host": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1,
          "description": "<p>Host.</p>"
        }
      },
      "required": [
        "host",
        "port"
      ]
    },
    "sql_server_parameters": {
      "type": "object",
      "description": "<p>SQL Server parameters.</p>",
      "properties": {
        "port": {
          "type": "number",
          "maximum": 65535,
          "minimum": 1,
          "description": "<p>Port.</p>"
        },
        "database": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1,
          "description": "<p>Database.</p>"
        },
        "host": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1,
          "description": "<p>Host.</p>"
        }
      },
      "required": [
        "database",
        "host",
        "port"
      ]
    },
    "ssl_properties": {
      "type": "object",
      "description": "<p>Secure Socket Layer (SSL) properties that apply when QuickSight connects to your\n            underlying data source.</p>",
      "properties": {
        "disable_ssl": {
          "type": "boolean",
          "description": "<p>A Boolean option to control whether SSL should be disabled.</p>"
        }
      }
    },
    "tag": {
      "type": "object",
      "description": "<p>The key or keys of the key-value pairs for the resource tag or tags assigned to the\n            resource.</p>",
      "properties": {
        "value": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1,
          "description": "<p>Tag value.</p>"
        },
        "key": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1,
          "description": "<p>Tag key.</p>"
        }
      },
      "required": [
        "key",
        "value"
      ]
    },
    "teradata_parameters": {
      "type": "object",
      "description": "<p>Teradata parameters.</p>",
      "properties": {
        "port": {
          "type": "number",
          "maximum": 65535,
          "minimum": 1,
          "description": "<p>Port.</p>"
        },
        "database": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1,
          "description": "<p>Database.</p>"
        },
        "host": {
          "type": "string",
          "maxLength": 256,
          "minLength": 1,
          "description": "<p>Host.</p>"
        }
      },
      "required": [
        "database",
        "host",
        "port"
      ]
    },
    "vpc_connection_properties": {
      "type": "object",
      "description": "<p>VPC connection properties.</p>",
      "properties": {
        "vpc_connection_arn": {
          "type": "string",
          "description": "<p>The Amazon Resource Name (ARN) for the VPC connection.</p>"
        }
      },
      "required": [
        "vpc_connection_arn"
      ]
    }
  },
  "properties": {
    "alternate_data_source_parameters": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/data_source_parameters"
      },
      "maxItems": 50,
      "minItems": 1,
      "description": "<p>A set of alternate data source parameters that you want to share for the credentials\n            stored with this data source. The credentials are applied in tandem with the data source\n            parameters when you copy a data source by using a create or update request. The API\n            operation compares the <code>DataSourceParameters</code> structure that's in the request\n            with the structures in the <code>AlternateDataSourceParameters</code> allow list. If the\n            structures are an exact match, the request is allowed to use the credentials from this\n            existing data source. If the <code>AlternateDataSourceParameters</code> list is null,\n            the <code>Credentials</code> originally used with this <code>DataSourceParameters</code>\n            are automatically allowed.</p>"
    },
    "arn": {
      "type": "string",
      "description": "<p>The Amazon Resource Name (ARN) of the data source.</p>"
    },
    "aws_account_id": {
      "type": "string",
      "maxLength": 12,
      "minLength": 12,
      "pattern": "^[0-9]{12}$"
    },
    "created_time": {
      "type": "string",
      "description": "<p>The time that this data source was created.</p>",
      "format": "date-time"
    },
    "credentials": {
      "$ref": "#/definitions/data_source_credentials"
    },
    "data_source_id": {
      "type": "string"
    },
    "data_source_parameters": {
      "$ref": "#/definitions/data_source_parameters"
    },
    "error_info": {
      "$ref": "#/definitions/data_source_error_info"
    },
    "last_updated_time": {
      "type": "string",
      "description": "<p>The last time that this data source was updated.</p>",
      "format": "date-time"
    },
    "name": {
      "type": "string",
      "maxLength": 128,
      "minLength": 1,
      "description": "<p>A display name for the data source.</p>"
    },
    "permissions": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/resource_permission"
      },
      "maxItems": 64,
      "minItems": 1,
      "description": "<p>A list of resource permissions on the data source.</p>"
    },
    "ssl_properties": {
      "$ref": "#/definitions/ssl_properties"
    },
    "status": {
      "$ref": "#/definitions/resource_status"
    },
    "tags": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/tag"
      },
      "maxItems": 200,
      "minItems": 1,
      "description": "<p>Contains a map of the key-value pairs for the resource tag or tags assigned to the data source.</p>"
    },
    "type": {
      "$ref": "#/definitions/data_source_type"
    },
    "vpc_connection_properties": {
      "$ref": "#/definitions/vpc_connection_properties"
    }
  },
  "readOnlyProperties": [
    "/properties/arn",
    "/properties/created_time",
    "/properties/last_updated_time",
    "/properties/status"
  ],
  "writeOnlyProperties": [
    "/properties/credentials"
  ],
  "createOnlyProperties": [
    "/properties/aws_account_id",
    "/properties/data_source_id",
    "/properties/type"
  ],
  "primaryIdentifier": [
    "/properties/aws_account_id",
    "/properties/data_source_id"
  ],
  "additionalProperties": false,
  "handlers": {
    "create": {
      "permissions": [
        "quicksight:CreateDataSource",
        "quicksight:DescribeDataSource",
        "quicksight:DescribeDataSourcePermissions",
        "quicksight:TagResource",
        "quicksight:ListTagsForResource"
      ]
    },
    "read": {
      "permissions": [
        "quicksight:DescribeDataSource",
        "quicksight:DescribeDataSourcePermissions",
        "quicksight:ListTagsForResource"
      ]
    },
    "update": {
      "permissions": [
        "quicksight:DescribeDataSource",
        "quicksight:DescribeDataSourcePermissions",
        "quicksight:UpdateDataSource",
        "quicksight:UpdateDataSourcePermissions",
        "quicksight:TagResource",
        "quicksight:UntagResource",
        "quicksight:ListTagsForResource"
      ]
    },
    "delete": {
      "permissions": [
        "quicksight:DescribeDataSource",
        "quicksight:DescribeDataSourcePermissions",
        "quicksight:DeleteDataSource",
        "quicksight:ListTagsForResource"
      ]
    },
    "list": {
      "permissions": [
        "quicksight:DescribeDataSource",
        "quicksight:ListDataSources"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}