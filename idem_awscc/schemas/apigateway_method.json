{
  "typeName": "AWS::ApiGateway::Method",
  "description": "Resource Type definition for AWS::ApiGateway::Method",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-apigateway.git",
  "definitions": {
    "integration": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "cache_key_parameters": {
          "description": "A list of request parameters whose values API Gateway caches.",
          "type": "array",
          "uniqueItems": true,
          "items": {
            "type": "string"
          }
        },
        "cache_namespace": {
          "description": "An API-specific tag group of related cached parameters.",
          "type": "string"
        },
        "connection_id": {
          "description": "The ID of the VpcLink used for the integration when connectionType=VPC_LINK, otherwise undefined.",
          "type": "string"
        },
        "connection_type": {
          "description": "The type of the network connection to the integration endpoint.",
          "type": "string",
          "enum": [
            "INTERNET",
            "VPC_LINK"
          ]
        },
        "content_handling": {
          "description": "Specifies how to handle request payload content type conversions.",
          "type": "string",
          "enum": [
            "CONVERT_TO_BINARY",
            "CONVERT_TO_TEXT"
          ]
        },
        "credentials": {
          "description": "The credentials that are required for the integration.",
          "type": "string"
        },
        "integration_http_method": {
          "description": "The integration's HTTP method type.",
          "type": "string"
        },
        "integration_responses": {
          "description": "The response that API Gateway provides after a method's backend completes processing a request.",
          "type": "array",
          "uniqueItems": true,
          "items": {
            "$ref": "#/definitions/integration_response"
          }
        },
        "passthrough_behavior": {
          "description": "Indicates when API Gateway passes requests to the targeted backend.",
          "type": "string",
          "enum": [
            "WHEN_NO_MATCH",
            "WHEN_NO_TEMPLATES",
            "NEVER"
          ]
        },
        "request_parameters": {
          "description": "The request parameters that API Gateway sends with the backend request.",
          "type": "object",
          "additionalProperties": false,
          "patternProperties": {
            "[a-zA-Z0-9]+": {
              "type": "string"
            }
          }
        },
        "request_templates": {
          "description": "A map of Apache Velocity templates that are applied on the request payload.",
          "type": "object",
          "additionalProperties": false,
          "patternProperties": {
            "[a-zA-Z0-9]+": {
              "type": "string"
            }
          }
        },
        "timeout_in_millis": {
          "description": "Custom timeout between 50 and 29,000 milliseconds.",
          "type": "integer",
          "minimum": 50,
          "maximum": 29000
        },
        "type": {
          "description": "The type of backend that your method is running.",
          "type": "string",
          "enum": [
            "AWS",
            "AWS_PROXY",
            "HTTP",
            "HTTP_PROXY",
            "MOCK"
          ]
        },
        "uri": {
          "description": "The Uniform Resource Identifier (URI) for the integration.",
          "type": "string"
        }
      },
      "required": [
        "type"
      ]
    },
    "method_response": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "response_models": {
          "description": "The resources used for the response's content type. Specify response models as key-value pairs (string-to-string maps), with a content type as the key and a Model resource name as the value.",
          "type": "object",
          "additionalProperties": false,
          "patternProperties": {
            "[a-zA-Z0-9]+": {
              "type": "string"
            }
          }
        },
        "response_parameters": {
          "description": "Response parameters that API Gateway sends to the client that called a method. Specify response parameters as key-value pairs (string-to-Boolean maps), with a destination as the key and a Boolean as the value.",
          "type": "object",
          "additionalProperties": false,
          "patternProperties": {
            "[a-zA-Z0-9]+": {
              "type": "boolean"
            }
          }
        },
        "status_code": {
          "description": "The method response's status code, which you map to an IntegrationResponse.",
          "type": "string"
        }
      },
      "required": [
        "status_code"
      ]
    },
    "integration_response": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "content_handling": {
          "description": "Specifies how to handle request payload content type conversions.",
          "type": "string",
          "enum": [
            "CONVERT_TO_BINARY",
            "CONVERT_TO_TEXT"
          ]
        },
        "response_parameters": {
          "description": "The response parameters from the backend response that API Gateway sends to the method response.",
          "type": "object",
          "additionalProperties": false,
          "patternProperties": {
            "[a-zA-Z0-9]+": {
              "type": "string"
            }
          }
        },
        "response_templates": {
          "description": "The templates that are used to transform the integration response body. Specify templates as key-value pairs (string-to-string mappings), with a content type as the key and a template as the value.",
          "type": "object",
          "additionalProperties": false,
          "patternProperties": {
            "[a-zA-Z0-9]+": {
              "type": "string"
            }
          }
        },
        "selection_pattern": {
          "description": "A regular expression that specifies which error strings or status codes from the backend map to the integration response.",
          "type": "string"
        },
        "status_code": {
          "description": "The status code that API Gateway uses to map the integration response to a MethodResponse status code.",
          "type": "string"
        }
      },
      "required": [
        "status_code"
      ]
    }
  },
  "properties": {
    "api_key_required": {
      "description": "Indicates whether the method requires clients to submit a valid API key.",
      "type": "boolean"
    },
    "authorization_scopes": {
      "description": "A list of authorization scopes configured on the method.",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "authorization_type": {
      "description": "The method's authorization type.",
      "type": "string",
      "enum": [
        "NONE",
        "AWS_IAM",
        "CUSTOM",
        "COGNITO_USER_POOLS"
      ]
    },
    "authorizer_id": {
      "description": "The identifier of the authorizer to use on this method.",
      "type": "string"
    },
    "http_method": {
      "description": "The backend system that the method calls when it receives a request.",
      "type": "string"
    },
    "integration": {
      "description": "The backend system that the method calls when it receives a request.",
      "$ref": "#/definitions/integration"
    },
    "method_responses": {
      "description": "The responses that can be sent to the client who calls the method.",
      "type": "array",
      "uniqueItems": true,
      "items": {
        "$ref": "#/definitions/method_response"
      }
    },
    "operation_name": {
      "description": "A friendly operation name for the method.",
      "type": "string"
    },
    "request_models": {
      "description": "The resources that are used for the request's content type. Specify request models as key-value pairs (string-to-string mapping), with a content type as the key and a Model resource name as the value.",
      "type": "object",
      "additionalProperties": false,
      "patternProperties": {
        "[a-zA-Z0-9]+": {
          "type": "string"
        }
      }
    },
    "request_parameters": {
      "description": "The request parameters that API Gateway accepts. Specify request parameters as key-value pairs (string-to-Boolean mapping), with a source as the key and a Boolean as the value.",
      "type": "object",
      "additionalProperties": false,
      "patternProperties": {
        "[a-zA-Z0-9]+": {
          "type": "boolean"
        }
      }
    },
    "request_validator_id": {
      "description": "The ID of the associated request validator.",
      "type": "string"
    },
    "resource_id": {
      "description": "The ID of an API Gateway resource.",
      "type": "string"
    },
    "rest_api_id": {
      "description": "The ID of the RestApi resource in which API Gateway creates the method.",
      "type": "string"
    }
  },
  "additionalProperties": false,
  "required": [
    "rest_api_id",
    "resource_id",
    "http_method"
  ],
  "primaryIdentifier": [
    "/properties/rest_api_id",
    "/properties/resource_id",
    "/properties/http_method"
  ],
  "createOnlyProperties": [
    "/properties/rest_api_id",
    "/properties/resource_id",
    "/properties/http_method"
  ],
  "replacementStrategy": "delete_then_create",
  "taggable": false,
  "handlers": {
    "create": {
      "permissions": [
        "apigateway:PUT",
        "apigateway:GET"
      ]
    },
    "read": {
      "permissions": [
        "apigateway:GET"
      ]
    },
    "update": {
      "permissions": [
        "apigateway:GET",
        "apigateway:DELETE",
        "apigateway:PUT"
      ]
    },
    "delete": {
      "permissions": [
        "apigateway:DELETE"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}