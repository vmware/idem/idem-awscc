{
  "typeName": "AWS::Synthetics::Canary",
  "description": "Resource Type definition for AWS::Synthetics::Canary",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-synthetics",
  "properties": {
    "name": {
      "description": "Name of the canary.",
      "type": "string",
      "pattern": "^[0-9a-z_\\-]{1,21}$"
    },
    "id": {
      "description": "Id of the canary",
      "type": "string"
    },
    "state": {
      "description": "State of the canary",
      "type": "string"
    },
    "code": {
      "description": "Provide the canary script source",
      "$ref": "#/definitions/code"
    },
    "artifacts3_location": {
      "description": "Provide the s3 bucket output location for test results",
      "type": "string",
      "pattern": "^(s3|S3)://"
    },
    "artifact_config": {
      "description": "Provide artifact configuration",
      "$ref": "#/definitions/artifact_config"
    },
    "schedule": {
      "description": "Frequency to run your canaries",
      "$ref": "#/definitions/schedule"
    },
    "execution_role_arn": {
      "description": "Lambda Execution role used to run your canaries",
      "type": "string"
    },
    "runtime_version": {
      "description": "Runtime version of Synthetics Library",
      "type": "string"
    },
    "success_retention_period": {
      "description": "Retention period of successful canary runs represented in number of days",
      "type": "integer"
    },
    "failure_retention_period": {
      "description": "Retention period of failed canary runs represented in number of days",
      "type": "integer"
    },
    "tags": {
      "type": "array",
      "uniqueItems": false,
      "items": {
        "$ref": "#/definitions/tag"
      }
    },
    "vpc_config": {
      "description": "Provide VPC Configuration if enabled.",
      "$ref": "#/definitions/vpc_config"
    },
    "run_config": {
      "description": "Provide canary run configuration",
      "$ref": "#/definitions/run_config"
    },
    "start_canary_after_creation": {
      "description": "Runs canary if set to True. Default is False",
      "type": "boolean"
    },
    "visual_reference": {
      "description": "Visual reference configuration for visual testing",
      "$ref": "#/definitions/visual_reference"
    },
    "delete_lambda_resources_on_canary_deletion": {
      "description": "Deletes associated lambda resources created by Synthetics if set to True. Default is False",
      "type": "boolean"
    }
  },
  "definitions": {
    "schedule": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "expression": {
          "type": "string"
        },
        "duration_in_seconds": {
          "type": "string"
        }
      },
      "required": [
        "expression"
      ]
    },
    "code": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "s3_bucket": {
          "type": "string"
        },
        "s3_key": {
          "type": "string"
        },
        "s3_object_version": {
          "type": "string"
        },
        "script": {
          "type": "string"
        },
        "handler": {
          "type": "string"
        }
      },
      "required": [
        "handler"
      ],
      "oneOf": [
        {
          "required": [
            "s3_bucket",
            "s3_key"
          ]
        },
        {
          "required": [
            "script"
          ]
        }
      ]
    },
    "tag": {
      "description": "A key-value pair to associate with a resource.",
      "additionalProperties": false,
      "type": "object",
      "properties": {
        "key": {
          "type": "string",
          "description": "The key name of the tag. You can specify a value that is 1 to 127 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -. ",
          "minLength": 1,
          "maxLength": 128
        },
        "value": {
          "type": "string",
          "description": "The value for the tag. You can specify a value that is 1 to 255 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -. ",
          "minLength": 0,
          "maxLength": 256
        }
      },
      "required": [
        "value",
        "key"
      ]
    },
    "vpc_config": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "vpc_id": {
          "type": "string"
        },
        "subnet_ids": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "security_group_ids": {
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      },
      "required": [
        "subnet_ids",
        "security_group_ids"
      ]
    },
    "run_config": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "timeout_in_seconds": {
          "description": "Provide maximum canary timeout per run in seconds",
          "type": "integer"
        },
        "memory_in_mb": {
          "description": "Provide maximum memory available for canary in MB",
          "type": "integer"
        },
        "active_tracing": {
          "description": "Enable active tracing if set to true",
          "type": "boolean"
        },
        "environment_variables": {
          "type": "object",
          "additionalProperties": false,
          "description": "Environment variable key-value pairs.",
          "patternProperties": {
            "[a-zA-Z][a-zA-Z0-9_]+": {
              "type": "string"
            }
          }
        }
      }
    },
    "visual_reference": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "base_canary_run_id": {
          "type": "string",
          "description": "Canary run id to be used as base reference for visual testing"
        },
        "base_screenshots": {
          "type": "array",
          "description": "List of screenshots used as base reference for visual testing",
          "items": {
            "$ref": "#/definitions/base_screenshot"
          }
        }
      },
      "required": [
        "base_canary_run_id"
      ]
    },
    "base_screenshot": {
      "type": "object",
      "properties": {
        "screenshot_name": {
          "type": "string",
          "description": "Name of the screenshot to be used as base reference for visual testing"
        },
        "ignore_coordinates": {
          "type": "array",
          "description": "List of coordinates of rectangles to be ignored during visual testing",
          "items": {
            "type": "string",
            "description": "Coordinates of a rectangle to be ignored during visual testing"
          }
        }
      },
      "required": [
        "screenshot_name"
      ]
    },
    "artifact_config": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "s3_encryption": {
          "$ref": "#/definitions/s3_encryption",
          "description": "Encryption configuration for uploading artifacts to S3"
        }
      }
    },
    "s3_encryption": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "encryption_mode": {
          "type": "string",
          "description": "Encryption mode for encrypting artifacts when uploading to S3. Valid values: SSE_S3 and SSE_KMS."
        },
        "kms_key_arn": {
          "type": "string",
          "description": "KMS key Arn for encrypting artifacts when uploading to S3. You must specify KMS key Arn for SSE_KMS encryption mode only."
        }
      }
    }
  },
  "required": [
    "name",
    "code",
    "artifacts3_location",
    "execution_role_arn",
    "schedule",
    "runtime_version",
    "start_canary_after_creation"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "synthetics:CreateCanary",
        "synthetics:StartCanary",
        "s3:CreateBucket",
        "s3:GetObject",
        "s3:GetObjectVersion",
        "s3:PutBucketEncryption",
        "s3:PutEncryptionConfiguration",
        "lambda:CreateFunction",
        "lambda:AddPermission",
        "lambda:PublishVersion",
        "lambda:UpdateFunctionConfiguration",
        "lambda:GetFunctionConfiguration",
        "lambda:GetLayerVersionByArn",
        "lambda:GetLayerVersion",
        "lambda:PublishLayerVersion",
        "ec2:DescribeVpcs",
        "ec2:DescribeSubnets",
        "ec2:DescribeSecurityGroups",
        "iam:PassRole"
      ]
    },
    "update": {
      "permissions": [
        "synthetics:UpdateCanary",
        "synthetics:StartCanary",
        "synthetics:StopCanary",
        "synthetics:TagResource",
        "synthetics:UntagResource"
      ]
    },
    "read": {
      "permissions": [
        "synthetics:GetCanary",
        "synthetics:DescribeCanaries",
        "synthetics:ListTagsForResource",
        "iam:ListRoles",
        "s3:ListAllMyBuckets",
        "s3:GetBucketLocation"
      ]
    },
    "delete": {
      "permissions": [
        "synthetics:DeleteCanary",
        "synthetics:GetCanary"
      ]
    },
    "list": {
      "permissions": [
        "synthetics:DescribeCanaries"
      ]
    }
  },
  "additionalProperties": false,
  "createOnlyProperties": [
    "/properties/name"
  ],
  "primaryIdentifier": [
    "/properties/name"
  ],
  "readOnlyProperties": [
    "/properties/id",
    "/properties/state",
    "/properties/code/source_location_arn"
  ],
  "writeOnlyProperties": [
    "/properties/code/s3_bucket",
    "/properties/code/s3_key",
    "/properties/code/s3_object_version",
    "/properties/code/script"
  ],
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}