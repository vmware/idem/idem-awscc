{
  "typeName": "AWS::ResilienceHub::App",
  "description": "Resource Type Definition for AWS::ResilienceHub::App.",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-resiliencehub",
  "definitions": {
    "tag_value": {
      "type": "string",
      "maxLength": 256
    },
    "tag_map": {
      "type": "object",
      "patternProperties": {
        ".{1,128}": {
          "$ref": "#/definitions/tag_value"
        }
      },
      "additionalProperties": false
    },
    "physical_resource_id": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "aws_account_id": {
          "type": "string",
          "pattern": "^[0-9]{12}$"
        },
        "aws_region": {
          "type": "string",
          "pattern": "^[a-z]{2}-((iso[a-z]{0,1}-)|(gov-)){0,1}[a-z]+-[0-9]$"
        },
        "identifier": {
          "type": "string",
          "minLength": 1,
          "maxLength": 255
        },
        "type": {
          "type": "string",
          "pattern": "Arn|Native"
        }
      },
      "required": [
        "identifier",
        "type"
      ]
    },
    "resource_mapping": {
      "description": "Resource mapping is used to map logical resources from template to physical resource",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "logical_stack_name": {
          "type": "string"
        },
        "mapping_type": {
          "type": "string",
          "pattern": "CfnStack|Resource|Terraform"
        },
        "resource_name": {
          "type": "string",
          "pattern": "^[A-Za-z0-9][A-Za-z0-9_\\-]{1,59}$"
        },
        "terraform_source_name": {
          "type": "string"
        },
        "physical_resource_id": {
          "$ref": "#/definitions/physical_resource_id"
        }
      },
      "required": [
        "mapping_type",
        "physical_resource_id"
      ]
    }
  },
  "properties": {
    "name": {
      "description": "Name of the app.",
      "type": "string",
      "pattern": "^[A-Za-z0-9][A-Za-z0-9_\\-]{1,59}$"
    },
    "description": {
      "description": "App description.",
      "type": "string",
      "minLength": 0,
      "maxLength": 500
    },
    "app_arn": {
      "type": "string",
      "description": "Amazon Resource Name (ARN) of the App.",
      "pattern": "^arn:(aws|aws-cn|aws-iso|aws-iso-[a-z]{1}|aws-us-gov):[A-Za-z0-9][A-Za-z0-9_/.-]{0,62}:([a-z]{2}-((iso[a-z]{0,1}-)|(gov-)){0,1}[a-z]+-[0-9]):[0-9]{12}:[A-Za-z0-9][A-Za-z0-9:_/+=,@.-]{0,1023}$"
    },
    "resiliency_policy_arn": {
      "type": "string",
      "description": "Amazon Resource Name (ARN) of the Resiliency Policy.",
      "pattern": "^arn:(aws|aws-cn|aws-iso|aws-iso-[a-z]{1}|aws-us-gov):[A-Za-z0-9][A-Za-z0-9_/.-]{0,62}:([a-z]{2}-((iso[a-z]{0,1}-)|(gov-)){0,1}[a-z]+-[0-9]):[0-9]{12}:[A-Za-z0-9][A-Za-z0-9:_/+=,@.-]{0,1023}$"
    },
    "tags": {
      "$ref": "#/definitions/tag_map"
    },
    "app_template_body": {
      "description": "A string containing full ResilienceHub app template body.",
      "type": "string",
      "minLength": 0,
      "maxLength": 5000,
      "pattern": "^[\\w\\s:,-\\.'{}\\[\\]:\"]+$"
    },
    "resource_mappings": {
      "description": "An array of ResourceMapping objects.",
      "type": "array",
      "uniqueItems": false,
      "insertionOrder": false,
      "items": {
        "$ref": "#/definitions/resource_mapping"
      }
    },
    "app_assessment_schedule": {
      "description": "Assessment execution schedule.",
      "type": "string",
      "enum": [
        "Disabled",
        "Daily"
      ]
    }
  },
  "additionalProperties": false,
  "required": [
    "name",
    "app_template_body",
    "resource_mappings"
  ],
  "createOnlyProperties": [
    "/properties/name"
  ],
  "readOnlyProperties": [
    "/properties/app_arn"
  ],
  "primaryIdentifier": [
    "/properties/app_arn"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "cloudformation:DescribeStacks",
        "cloudformation:ListStackResources",
        "s3:GetBucketLocation",
        "s3:GetObject",
        "resiliencehub:CreateApp",
        "resiliencehub:TagResource",
        "resiliencehub:PutDraftAppVersionTemplate",
        "resiliencehub:AddDraftAppVersionResourceMappings",
        "resiliencehub:PublishAppVersion"
      ]
    },
    "read": {
      "permissions": [
        "resiliencehub:DescribeApp",
        "resiliencehub:DescribeAppVersionTemplate",
        "resiliencehub:ListAppVersionResourceMappings",
        "resiliencehub:ListTagsForResource"
      ]
    },
    "update": {
      "permissions": [
        "cloudformation:DescribeStacks",
        "cloudformation:ListStackResources",
        "s3:GetBucketLocation",
        "s3:GetObject",
        "resiliencehub:UpdateApp",
        "resiliencehub:PutDraftAppVersionTemplate",
        "resiliencehub:AddDraftAppVersionResourceMappings",
        "resiliencehub:RemoveDraftAppVersionResourceMappings",
        "resiliencehub:PublishAppVersion",
        "resiliencehub:TagResource",
        "resiliencehub:UntagResource",
        "resiliencehub:ListTagsForResource"
      ]
    },
    "delete": {
      "permissions": [
        "resiliencehub:DeleteApp",
        "resiliencehub:UntagResource",
        "resiliencehub:ListApps"
      ]
    },
    "list": {
      "permissions": [
        "resiliencehub:ListApps"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}