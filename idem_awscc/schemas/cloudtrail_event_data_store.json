{
  "typeName": "AWS::CloudTrail::EventDataStore",
  "description": "A storage lake of event data against which you can run complex SQL-based queries. An event data store can include events that you have logged on your account from the last 90 to 2555 days (about three months to up to seven years).",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-cloudtrail.git",
  "definitions": {
    "advanced_field_selector": {
      "description": "A single selector statement in an advanced event selector.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "field": {
          "description": "A field in an event record on which to filter events to be logged. Supported fields include readOnly, eventCategory, eventSource (for management events), eventName, resources.type, and resources.ARN.",
          "type": "string",
          "pattern": "([\\w|\\d|\\.|_]+)",
          "minLength": 1,
          "maxLength": 1000
        },
        "equals": {
          "description": "An operator that includes events that match the exact value of the event record field specified as the value of Field. This is the only valid operator that you can use with the readOnly, eventCategory, and resources.type fields.",
          "type": "array",
          "uniqueItems": true,
          "insertionOrder": false,
          "minItems": 1,
          "items": {
            "type": "string",
            "pattern": "(.+)",
            "minLength": 1,
            "maxLength": 2048
          }
        },
        "starts_with": {
          "description": "An operator that includes events that match the first few characters of the event record field specified as the value of Field.",
          "type": "array",
          "uniqueItems": true,
          "insertionOrder": false,
          "minItems": 1,
          "items": {
            "type": "string",
            "pattern": "(.+)",
            "minLength": 1,
            "maxLength": 2048
          }
        },
        "ends_with": {
          "description": "An operator that includes events that match the last few characters of the event record field specified as the value of Field.",
          "type": "array",
          "uniqueItems": true,
          "insertionOrder": false,
          "minItems": 1,
          "items": {
            "type": "string",
            "pattern": "(.+)",
            "minLength": 1,
            "maxLength": 2048
          }
        },
        "not_equals": {
          "description": "An operator that excludes events that match the exact value of the event record field specified as the value of Field.",
          "type": "array",
          "uniqueItems": true,
          "insertionOrder": false,
          "minItems": 1,
          "items": {
            "type": "string",
            "pattern": "(.+)",
            "minLength": 1,
            "maxLength": 2048
          }
        },
        "not_starts_with": {
          "description": "An operator that excludes events that match the first few characters of the event record field specified as the value of Field.",
          "type": "array",
          "uniqueItems": true,
          "insertionOrder": false,
          "minItems": 1,
          "items": {
            "type": "string",
            "pattern": "(.+)",
            "minLength": 1,
            "maxLength": 2048
          }
        },
        "not_ends_with": {
          "description": "An operator that excludes events that match the last few characters of the event record field specified as the value of Field.",
          "type": "array",
          "uniqueItems": true,
          "insertionOrder": false,
          "minItems": 1,
          "items": {
            "type": "string",
            "pattern": "(.+)",
            "minLength": 1,
            "maxLength": 2048
          }
        }
      },
      "required": [
        "field"
      ]
    },
    "advanced_event_selector": {
      "description": "Advanced event selectors let you create fine-grained selectors for the following AWS CloudTrail event record ?elds. They help you control costs by logging only those events that are important to you.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "name": {
          "description": "An optional, descriptive name for an advanced event selector, such as \"Log data events for only two S3 buckets\".",
          "type": "string",
          "minLength": 1,
          "maxLength": 1000
        },
        "field_selectors": {
          "description": "Contains all selector statements in an advanced event selector.",
          "type": "array",
          "uniqueItems": true,
          "insertionOrder": false,
          "minItems": 1,
          "items": {
            "$ref": "#/definitions/advanced_field_selector"
          }
        }
      },
      "required": [
        "field_selectors"
      ]
    },
    "tag": {
      "description": "An arbitrary set of tags (key-value pairs) for this event data store.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "key": {
          "description": "The key name of the tag. You can specify a value that is 1 to 127 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -.",
          "type": "string"
        },
        "value": {
          "description": "The value for the tag. You can specify a value that is 1 to 255 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -.",
          "type": "string"
        }
      },
      "required": [
        "value",
        "key"
      ]
    },
    "timestamp": {
      "type": "string"
    }
  },
  "properties": {
    "advanced_event_selectors": {
      "description": "The advanced event selectors that were used to select events for the data store.",
      "type": "array",
      "items": {
        "$ref": "#/definitions/advanced_event_selector"
      },
      "uniqueItems": true,
      "insertionOrder": false
    },
    "created_timestamp": {
      "description": "The timestamp of the event data store's creation.",
      "$ref": "#/definitions/timestamp"
    },
    "event_data_store_arn": {
      "description": "The ARN of the event data store.",
      "type": "string"
    },
    "multi_region_enabled": {
      "description": "Indicates whether the event data store includes events from all regions, or only from the region in which it was created.",
      "type": "boolean"
    },
    "name": {
      "description": "The name of the event data store.",
      "type": "string"
    },
    "organization_enabled": {
      "description": "Indicates that an event data store is collecting logged events for an organization.",
      "type": "boolean"
    },
    "retention_period": {
      "description": "The retention period, in days.",
      "type": "integer"
    },
    "status": {
      "description": "The status of an event data store. Values are ENABLED and PENDING_DELETION.",
      "type": "string"
    },
    "termination_protection_enabled": {
      "description": "Indicates whether the event data store is protected from termination.",
      "type": "boolean"
    },
    "updated_timestamp": {
      "description": "The timestamp showing when an event data store was updated, if applicable. UpdatedTimestamp is always either the same or newer than the time shown in CreatedTimestamp.",
      "$ref": "#/definitions/timestamp"
    },
    "tags": {
      "type": "array",
      "uniqueItems": false,
      "insertionOrder": false,
      "items": {
        "$ref": "#/definitions/tag"
      }
    }
  },
  "required": [],
  "readOnlyProperties": [
    "/properties/event_data_store_arn",
    "/properties/created_timestamp",
    "/properties/updated_timestamp",
    "/properties/status"
  ],
  "primaryIdentifier": [
    "/properties/event_data_store_arn"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "CloudTrail:CreateEventDataStore",
        "CloudTrail:AddTags",
        "iam:PassRole",
        "iam:GetRole",
        "iam:CreateServiceLinkedRole",
        "organizations:DescribeOrganization",
        "organizations:ListAWSServiceAccessForOrganization"
      ]
    },
    "read": {
      "permissions": [
        "CloudTrail:GetEventDataStore",
        "CloudTrail:ListEventDataStores",
        "CloudTrail:ListTags"
      ]
    },
    "update": {
      "permissions": [
        "CloudTrail:UpdateEventDataStore",
        "CloudTrail:RestoreEventDataStore",
        "CloudTrail:AddTags",
        "CloudTrail:RemoveTags",
        "iam:PassRole",
        "iam:GetRole",
        "iam:CreateServiceLinkedRole",
        "organizations:DescribeOrganization",
        "organizations:ListAWSServiceAccessForOrganization"
      ]
    },
    "delete": {
      "permissions": [
        "CloudTrail:DeleteEventDataStore"
      ]
    },
    "list": {
      "permissions": [
        "CloudTrail:ListEventDataStores",
        "CloudTrail:GetEventDataStore",
        "CloudTrail:ListTags"
      ]
    }
  },
  "additionalProperties": false,
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}