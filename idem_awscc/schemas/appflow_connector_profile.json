{
  "typeName": "AWS::AppFlow::ConnectorProfile",
  "description": "Resource Type definition for AWS::AppFlow::ConnectorProfile",
  "additionalProperties": false,
  "properties": {
    "connector_profile_arn": {
      "description": "Unique identifier for connector profile resources",
      "type": "string",
      "pattern": "arn:aws:appflow:.*:[0-9]+:.*",
      "maxLength": 512
    },
    "connector_label": {
      "description": "The label of the connector. The label is unique for each ConnectorRegistration in your AWS account. Only needed if calling for CUSTOMCONNECTOR connector type/.",
      "type": "string",
      "pattern": "[\\w!@#.-]+",
      "maxLength": 256
    },
    "connector_profile_name": {
      "description": "The maximum number of items to retrieve in a single batch.",
      "type": "string",
      "pattern": "[\\w/!@#+=.-]+",
      "maxLength": 256
    },
    "kms_arn": {
      "description": "The ARN of the AWS Key Management Service (AWS KMS) key that's used to encrypt your function's environment variables. If it's not provided, AWS Lambda uses a default service key.",
      "type": "string",
      "pattern": "arn:aws:kms:.*:[0-9]+:.*",
      "maxLength": 2048,
      "minLength": 20
    },
    "connector_type": {
      "description": "List of Saas providers that need connector profile to be created",
      "$ref": "#/definitions/connector_type"
    },
    "connection_mode": {
      "description": "Mode in which data transfer should be enabled. Private connection mode is currently enabled for Salesforce, Snowflake, Trendmicro and Singular",
      "type": "string",
      "enum": [
        "Public",
        "Private"
      ]
    },
    "connector_profile_config": {
      "description": "Connector specific configurations needed to create connector profile",
      "$ref": "#/definitions/connector_profile_config"
    },
    "credentials_arn": {
      "description": "A unique Arn for Connector-Profile resource",
      "type": "string",
      "pattern": "arn:aws:.*:.*:[0-9]+:.*",
      "maxLength": 512
    }
  },
  "definitions": {
    "connector_type": {
      "type": "string",
      "enum": [
        "Salesforce",
        "Singular",
        "Slack",
        "Redshift",
        "Marketo",
        "Googleanalytics",
        "Zendesk",
        "Servicenow",
        "SAPOData",
        "Datadog",
        "Trendmicro",
        "Snowflake",
        "Dynatrace",
        "Infornexus",
        "Amplitude",
        "Veeva",
        "CustomConnector"
      ]
    },
    "connector_profile_config": {
      "description": "Connector specific configurations needed to create connector profile",
      "type": "object",
      "required": [
        "connector_profile_credentials"
      ],
      "properties": {
        "connector_profile_properties": {
          "$ref": "#/definitions/connector_profile_properties"
        },
        "connector_profile_credentials": {
          "$ref": "#/definitions/connector_profile_credentials"
        }
      }
    },
    "connector_profile_properties": {
      "description": "Connector specific properties needed to create connector profile - currently not needed for Amplitude, Trendmicro, Googleanalytics and Singular",
      "type": "object",
      "properties": {
        "datadog": {
          "$ref": "#/definitions/datadog_connector_profile_properties"
        },
        "dynatrace": {
          "$ref": "#/definitions/dynatrace_connector_profile_properties"
        },
        "infor_nexus": {
          "$ref": "#/definitions/infor_nexus_connector_profile_properties"
        },
        "marketo": {
          "$ref": "#/definitions/marketo_connector_profile_properties"
        },
        "redshift": {
          "$ref": "#/definitions/redshift_connector_profile_properties"
        },
        "sapo_data": {
          "$ref": "#/definitions/sapo_data_connector_profile_properties"
        },
        "salesforce": {
          "$ref": "#/definitions/salesforce_connector_profile_properties"
        },
        "service_now": {
          "$ref": "#/definitions/service_now_connector_profile_properties"
        },
        "slack": {
          "$ref": "#/definitions/slack_connector_profile_properties"
        },
        "snowflake": {
          "$ref": "#/definitions/snowflake_connector_profile_properties"
        },
        "veeva": {
          "$ref": "#/definitions/veeva_connector_profile_properties"
        },
        "zendesk": {
          "$ref": "#/definitions/zendesk_connector_profile_properties"
        },
        "custom_connector": {
          "$ref": "#/definitions/custom_connector_profile_properties"
        }
      }
    },
    "connector_profile_credentials": {
      "description": "Connector specific configuration needed to create connector profile based on Authentication mechanism",
      "type": "object",
      "properties": {
        "amplitude": {
          "$ref": "#/definitions/amplitude_connector_profile_credentials"
        },
        "datadog": {
          "$ref": "#/definitions/datadog_connector_profile_credentials"
        },
        "dynatrace": {
          "$ref": "#/definitions/dynatrace_connector_profile_credentials"
        },
        "google_analytics": {
          "$ref": "#/definitions/google_analytics_connector_profile_credentials"
        },
        "infor_nexus": {
          "$ref": "#/definitions/infor_nexus_connector_profile_credentials"
        },
        "marketo": {
          "$ref": "#/definitions/marketo_connector_profile_credentials"
        },
        "redshift": {
          "$ref": "#/definitions/redshift_connector_profile_credentials"
        },
        "sapo_data": {
          "$ref": "#/definitions/sapo_data_connector_profile_credentials"
        },
        "salesforce": {
          "$ref": "#/definitions/salesforce_connector_profile_credentials"
        },
        "service_now": {
          "$ref": "#/definitions/service_now_connector_profile_credentials"
        },
        "singular": {
          "$ref": "#/definitions/singular_connector_profile_credentials"
        },
        "slack": {
          "$ref": "#/definitions/slack_connector_profile_credentials"
        },
        "snowflake": {
          "$ref": "#/definitions/snowflake_connector_profile_credentials"
        },
        "trendmicro": {
          "$ref": "#/definitions/trendmicro_connector_profile_credentials"
        },
        "veeva": {
          "$ref": "#/definitions/veeva_connector_profile_credentials"
        },
        "zendesk": {
          "$ref": "#/definitions/zendesk_connector_profile_credentials"
        },
        "custom_connector": {
          "$ref": "#/definitions/custom_connector_profile_credentials"
        }
      }
    },
    "amplitude_connector_profile_credentials": {
      "type": "object",
      "required": [
        "api_key",
        "secret_key"
      ],
      "properties": {
        "api_key": {
          "description": "A unique alphanumeric identi?er used to authenticate a user, developer, or calling program to your API.",
          "$ref": "#/definitions/api_key"
        },
        "secret_key": {
          "$ref": "#/definitions/secret_key"
        }
      }
    },
    "datadog_connector_profile_credentials": {
      "type": "object",
      "required": [
        "api_key",
        "application_key"
      ],
      "properties": {
        "api_key": {
          "description": "A unique alphanumeric identi?er used to authenticate a user, developer, or calling program to your API.",
          "$ref": "#/definitions/api_key"
        },
        "application_key": {
          "description": "Application keys, in conjunction with your API key, give you full access to Datadog?s programmatic API. Application keys are associated with the user account that created them. The application key is used to log all requests made to the API.",
          "$ref": "#/definitions/application_key"
        }
      }
    },
    "datadog_connector_profile_properties": {
      "type": "object",
      "required": [
        "instance_url"
      ],
      "properties": {
        "instance_url": {
          "description": "The location of the Datadog resource",
          "$ref": "#/definitions/instance_url"
        }
      }
    },
    "dynatrace_connector_profile_credentials": {
      "type": "object",
      "required": [
        "api_token"
      ],
      "properties": {
        "api_token": {
          "description": "The API tokens used by Dynatrace API to authenticate various API calls.",
          "$ref": "#/definitions/api_token"
        }
      }
    },
    "dynatrace_connector_profile_properties": {
      "type": "object",
      "required": [
        "instance_url"
      ],
      "properties": {
        "instance_url": {
          "description": "The location of the Dynatrace resource",
          "$ref": "#/definitions/instance_url"
        }
      }
    },
    "google_analytics_connector_profile_credentials": {
      "type": "object",
      "required": [
        "client_id",
        "client_secret"
      ],
      "properties": {
        "client_id": {
          "description": "The identi?er for the desired client.",
          "$ref": "#/definitions/client_id"
        },
        "client_secret": {
          "description": "The client secret used by the oauth client to authenticate to the authorization server.",
          "$ref": "#/definitions/client_secret"
        },
        "access_token": {
          "description": "The credentials used to access protected resources.",
          "$ref": "#/definitions/access_token"
        },
        "refresh_token": {
          "description": "The credentials used to acquire new access tokens.",
          "$ref": "#/definitions/refresh_token"
        },
        "connector_oauth_request": {
          "description": "The oauth needed to request security tokens from the connector endpoint.",
          "$ref": "#/definitions/connector_oauth_request"
        }
      }
    },
    "infor_nexus_connector_profile_credentials": {
      "type": "object",
      "required": [
        "access_key_id",
        "user_id",
        "secret_access_key",
        "datakey"
      ],
      "properties": {
        "access_key_id": {
          "description": "The Access Key portion of the credentials.",
          "$ref": "#/definitions/access_key_id"
        },
        "user_id": {
          "description": "The identi?er for the user.",
          "$ref": "#/definitions/username"
        },
        "secret_access_key": {
          "description": "The secret key used to sign requests.",
          "$ref": "#/definitions/key"
        },
        "datakey": {
          "description": "The encryption keys used to encrypt data.",
          "$ref": "#/definitions/key"
        }
      }
    },
    "infor_nexus_connector_profile_properties": {
      "type": "object",
      "required": [
        "instance_url"
      ],
      "properties": {
        "instance_url": {
          "description": "The location of the InforNexus resource",
          "$ref": "#/definitions/instance_url"
        }
      }
    },
    "marketo_connector_profile_credentials": {
      "type": "object",
      "required": [
        "client_id",
        "client_secret"
      ],
      "properties": {
        "client_id": {
          "description": "The identi?er for the desired client.",
          "$ref": "#/definitions/client_id"
        },
        "client_secret": {
          "description": "The client secret used by the oauth client to authenticate to the authorization server.",
          "$ref": "#/definitions/client_secret"
        },
        "access_token": {
          "description": "The credentials used to access protected resources.",
          "$ref": "#/definitions/access_token"
        },
        "connector_oauth_request": {
          "description": "The oauth needed to request security tokens from the connector endpoint.",
          "$ref": "#/definitions/connector_oauth_request"
        }
      }
    },
    "marketo_connector_profile_properties": {
      "type": "object",
      "required": [
        "instance_url"
      ],
      "properties": {
        "instance_url": {
          "description": "The location of the Marketo resource",
          "$ref": "#/definitions/instance_url"
        }
      }
    },
    "redshift_connector_profile_credentials": {
      "type": "object",
      "required": [
        "username",
        "password"
      ],
      "properties": {
        "username": {
          "description": "The name of the user.",
          "$ref": "#/definitions/username"
        },
        "password": {
          "description": "The password that corresponds to the username.",
          "$ref": "#/definitions/password"
        }
      }
    },
    "redshift_connector_profile_properties": {
      "type": "object",
      "required": [
        "database_url",
        "bucket_name",
        "role_arn"
      ],
      "properties": {
        "database_url": {
          "description": "The JDBC URL of the Amazon Redshift cluster.",
          "$ref": "#/definitions/database_url"
        },
        "bucket_name": {
          "description": "The name of the Amazon S3 bucket associated with Redshift.",
          "$ref": "#/definitions/bucket_name"
        },
        "bucket_prefix": {
          "description": "The object key for the destination bucket in which Amazon AppFlow will place the ?les.",
          "$ref": "#/definitions/bucket_prefix"
        },
        "role_arn": {
          "description": "The Amazon Resource Name (ARN) of the IAM role.",
          "$ref": "#/definitions/role_arn"
        }
      }
    },
    "sapo_data_connector_profile_credentials": {
      "type": "object",
      "properties": {
        "basic_auth_credentials": {
          "$ref": "#/definitions/basic_auth_credentials"
        },
        "o_auth_credentials": {
          "type": "object",
          "properties": {
            "access_token": {
              "$ref": "#/definitions/access_token"
            },
            "refresh_token": {
              "$ref": "#/definitions/refresh_token"
            },
            "connector_oauth_request": {
              "$ref": "#/definitions/connector_oauth_request"
            },
            "client_id": {
              "$ref": "#/definitions/client_id"
            },
            "client_secret": {
              "$ref": "#/definitions/client_secret"
            }
          }
        }
      }
    },
    "sapo_data_connector_profile_properties": {
      "type": "object",
      "properties": {
        "application_host_url": {
          "$ref": "#/definitions/application_host_url"
        },
        "application_service_path": {
          "$ref": "#/definitions/application_service_path"
        },
        "port_number": {
          "$ref": "#/definitions/port_number"
        },
        "client_number": {
          "$ref": "#/definitions/client_number"
        },
        "logon_language": {
          "$ref": "#/definitions/logon_language"
        },
        "private_link_service_name": {
          "$ref": "#/definitions/private_link_service_name"
        },
        "o_auth_properties": {
          "$ref": "#/definitions/o_auth_properties"
        }
      }
    },
    "salesforce_connector_profile_credentials": {
      "type": "object",
      "properties": {
        "access_token": {
          "description": "The credentials used to access protected resources.",
          "$ref": "#/definitions/access_token"
        },
        "refresh_token": {
          "description": "The credentials used to acquire new access tokens.",
          "$ref": "#/definitions/refresh_token"
        },
        "connector_oauth_request": {
          "description": "The oauth needed to request security tokens from the connector endpoint.",
          "$ref": "#/definitions/connector_oauth_request"
        },
        "client_credentials_arn": {
          "description": "The client credentials to fetch access token and refresh token.",
          "$ref": "#/definitions/client_credentials_arn"
        }
      }
    },
    "salesforce_connector_profile_properties": {
      "type": "object",
      "properties": {
        "instance_url": {
          "description": "The location of the Salesforce resource",
          "$ref": "#/definitions/instance_url"
        },
        "is_sandbox_environment": {
          "type": "boolean"
        }
      }
    },
    "service_now_connector_profile_credentials": {
      "type": "object",
      "required": [
        "username",
        "password"
      ],
      "properties": {
        "username": {
          "description": "The name of the user.",
          "$ref": "#/definitions/username"
        },
        "password": {
          "description": "The password that corresponds to the username.",
          "$ref": "#/definitions/password"
        }
      }
    },
    "service_now_connector_profile_properties": {
      "type": "object",
      "required": [
        "instance_url"
      ],
      "properties": {
        "instance_url": {
          "description": "The location of the ServiceNow resource",
          "$ref": "#/definitions/instance_url"
        }
      }
    },
    "singular_connector_profile_credentials": {
      "type": "object",
      "required": [
        "api_key"
      ],
      "properties": {
        "api_key": {
          "description": "A unique alphanumeric identi?er used to authenticate a user, developer, or calling program to your API.",
          "$ref": "#/definitions/api_key"
        }
      }
    },
    "slack_connector_profile_credentials": {
      "type": "object",
      "required": [
        "client_id",
        "client_secret"
      ],
      "properties": {
        "client_id": {
          "description": "The identi?er for the desired client.",
          "$ref": "#/definitions/client_id"
        },
        "client_secret": {
          "description": "The client secret used by the oauth client to authenticate to the authorization server.",
          "$ref": "#/definitions/client_secret"
        },
        "access_token": {
          "description": "The credentials used to access protected resources.",
          "$ref": "#/definitions/access_token"
        },
        "connector_oauth_request": {
          "description": "The oauth needed to request security tokens from the connector endpoint.",
          "$ref": "#/definitions/connector_oauth_request"
        }
      }
    },
    "slack_connector_profile_properties": {
      "type": "object",
      "required": [
        "instance_url"
      ],
      "properties": {
        "instance_url": {
          "description": "The location of the Slack resource",
          "$ref": "#/definitions/instance_url"
        }
      }
    },
    "snowflake_connector_profile_credentials": {
      "type": "object",
      "required": [
        "username",
        "password"
      ],
      "properties": {
        "username": {
          "description": "The name of the user.",
          "$ref": "#/definitions/username"
        },
        "password": {
          "description": "The password that corresponds to the username.",
          "$ref": "#/definitions/password"
        }
      }
    },
    "snowflake_connector_profile_properties": {
      "type": "object",
      "required": [
        "warehouse",
        "stage",
        "bucket_name"
      ],
      "properties": {
        "warehouse": {
          "description": "The name of the Snow?ake warehouse.",
          "$ref": "#/definitions/warehouse"
        },
        "stage": {
          "description": "The name of the Amazon S3 stage that was created while setting up an Amazon S3 stage in the\nSnow?ake account. This is written in the following format: < Database>< Schema><Stage Name>.",
          "$ref": "#/definitions/stage"
        },
        "bucket_name": {
          "description": "The name of the Amazon S3 bucket associated with Snow?ake.",
          "$ref": "#/definitions/bucket_name"
        },
        "bucket_prefix": {
          "description": "The bucket prefix that refers to the Amazon S3 bucket associated with Snow?ake.",
          "$ref": "#/definitions/bucket_prefix"
        },
        "private_link_service_name": {
          "description": "The Snow?ake Private Link service name to be used for private data transfers.",
          "$ref": "#/definitions/private_link_service_name"
        },
        "account_name": {
          "description": "The name of the account.",
          "$ref": "#/definitions/account_name"
        },
        "region": {
          "description": "The region of the Snow?ake account.",
          "$ref": "#/definitions/region"
        }
      }
    },
    "trendmicro_connector_profile_credentials": {
      "type": "object",
      "required": [
        "api_secret_key"
      ],
      "properties": {
        "api_secret_key": {
          "description": "The Secret Access Key portion of the credentials.",
          "$ref": "#/definitions/api_secret_key"
        }
      }
    },
    "veeva_connector_profile_credentials": {
      "type": "object",
      "required": [
        "username",
        "password"
      ],
      "properties": {
        "username": {
          "description": "The name of the user.",
          "$ref": "#/definitions/username"
        },
        "password": {
          "description": "The password that corresponds to the username.",
          "$ref": "#/definitions/password"
        }
      }
    },
    "veeva_connector_profile_properties": {
      "type": "object",
      "required": [
        "instance_url"
      ],
      "properties": {
        "instance_url": {
          "description": "The location of the Veeva resource",
          "$ref": "#/definitions/instance_url"
        }
      }
    },
    "zendesk_connector_profile_credentials": {
      "type": "object",
      "required": [
        "client_id",
        "client_secret"
      ],
      "properties": {
        "client_id": {
          "description": "The identi?er for the desired client.",
          "$ref": "#/definitions/client_id"
        },
        "client_secret": {
          "description": "The client secret used by the oauth client to authenticate to the authorization server.",
          "$ref": "#/definitions/client_secret"
        },
        "access_token": {
          "description": "The credentials used to access protected resources.",
          "$ref": "#/definitions/access_token"
        },
        "connector_oauth_request": {
          "description": "The oauth needed to request security tokens from the connector endpoint.",
          "$ref": "#/definitions/connector_oauth_request"
        }
      }
    },
    "zendesk_connector_profile_properties": {
      "type": "object",
      "required": [
        "instance_url"
      ],
      "properties": {
        "instance_url": {
          "description": "The location of the Zendesk resource",
          "$ref": "#/definitions/instance_url"
        }
      }
    },
    "custom_connector_profile_credentials": {
      "type": "object",
      "required": [
        "authentication_type"
      ],
      "properties": {
        "authentication_type": {
          "$ref": "#/definitions/authentication_type"
        },
        "basic": {
          "$ref": "#/definitions/basic_auth_credentials"
        },
        "oauth2": {
          "$ref": "#/definitions/o_auth2_credentials"
        },
        "api_key": {
          "$ref": "#/definitions/api_key_credentials"
        },
        "custom": {
          "$ref": "#/definitions/custom_auth_credentials"
        }
      },
      "additionalProperties": false
    },
    "custom_connector_profile_properties": {
      "type": "object",
      "properties": {
        "profile_properties": {
          "$ref": "#/definitions/profile_properties"
        },
        "o_auth2_properties": {
          "$ref": "#/definitions/o_auth2_properties"
        }
      },
      "additionalProperties": false
    },
    "api_key_credentials": {
      "type": "object",
      "required": [
        "api_key"
      ],
      "properties": {
        "api_key": {
          "$ref": "#/definitions/api_key"
        },
        "api_secret_key": {
          "$ref": "#/definitions/api_secret_key"
        }
      },
      "additionalProperties": false
    },
    "custom_auth_credentials": {
      "type": "object",
      "required": [
        "custom_authentication_type"
      ],
      "properties": {
        "custom_authentication_type": {
          "$ref": "#/definitions/custom_authentication_type"
        },
        "credentials_map": {
          "$ref": "#/definitions/credentials_map"
        }
      },
      "additionalProperties": false
    },
    "credentials_map": {
      "description": "A map for properties for custom authentication.",
      "type": "object",
      "patternProperties": {
        "^[\\w]{1,128}$": {
          "description": "A string containing the value for the property",
          "type": "string",
          "minLength": 1,
          "maxLength": 2048,
          "pattern": "\\S+"
        }
      },
      "required": [],
      "additionalProperties": false
    },
    "o_auth2_credentials": {
      "type": "object",
      "properties": {
        "client_id": {
          "$ref": "#/definitions/client_id"
        },
        "client_secret": {
          "$ref": "#/definitions/client_secret"
        },
        "access_token": {
          "$ref": "#/definitions/access_token"
        },
        "refresh_token": {
          "$ref": "#/definitions/refresh_token"
        },
        "o_auth_request": {
          "$ref": "#/definitions/connector_oauth_request"
        }
      },
      "additionalProperties": false
    },
    "basic_auth_credentials": {
      "type": "object",
      "required": [
        "username",
        "password"
      ],
      "properties": {
        "username": {
          "$ref": "#/definitions/username"
        },
        "password": {
          "$ref": "#/definitions/password"
        }
      },
      "additionalProperties": false
    },
    "authentication_type": {
      "type": "string",
      "enum": [
        "OAUTH2",
        "APIKEY",
        "BASIC",
        "CUSTOM"
      ]
    },
    "o_auth2_properties": {
      "type": "object",
      "properties": {
        "token_url": {
          "type": "string",
          "minLength": 0,
          "maxLength": 256,
          "pattern": "^(https?)://[-a-zA-Z0-9+&amp;@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&amp;@#/%=~_|]"
        },
        "o_auth2_grant_type": {
          "$ref": "#/definitions/o_auth2_grant_type"
        },
        "token_url_custom_properties": {
          "$ref": "#/definitions/token_url_custom_properties"
        }
      },
      "additionalProperties": false
    },
    "profile_properties": {
      "description": "A map for properties for custom connector.",
      "type": "object",
      "patternProperties": {
        "^[\\w]{1,256}$": {
          "description": "A string containing the value for the property",
          "type": "string",
          "minLength": 1,
          "maxLength": 2048,
          "pattern": "\\S+"
        }
      },
      "required": [],
      "additionalProperties": false
    },
    "o_auth2_grant_type": {
      "type": "string",
      "enum": [
        "CLIENT_CREDENTIALS",
        "AUTHORIZATION_CODE"
      ]
    },
    "token_url_custom_properties": {
      "description": "A map for properties for custom connector Token Url.",
      "type": "object",
      "patternProperties": {
        "^[\\w]{1,128}$": {
          "description": "A string containing the value for the property",
          "type": "string",
          "minLength": 1,
          "maxLength": 2048,
          "pattern": "\\S+"
        }
      },
      "required": [],
      "additionalProperties": false
    },
    "custom_authentication_type": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 256
    },
    "client_id": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 512
    },
    "client_secret": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 512
    },
    "instance_url": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 256
    },
    "access_token": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 512
    },
    "api_key": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 256
    },
    "api_secret_key": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 256
    },
    "api_token": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 256
    },
    "application_key": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 512
    },
    "auth_code": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 512
    },
    "bucket_name": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 63,
      "minLength": 3
    },
    "bucket_prefix": {
      "type": "string",
      "maxLength": 128
    },
    "key": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 512
    },
    "database_url": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 512
    },
    "role_arn": {
      "type": "string",
      "pattern": "arn:aws:iam:.*:[0-9]+:.*",
      "maxLength": 512
    },
    "warehouse": {
      "type": "string",
      "pattern": "[\\s\\w/!@#+=.-]*",
      "maxLength": 512
    },
    "stage": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 16
    },
    "private_link_service_name": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 512
    },
    "account_name": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 512
    },
    "refresh_token": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 512
    },
    "region": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 64
    },
    "secret_key": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 256
    },
    "access_key_id": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 256
    },
    "username": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 512
    },
    "password": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 512
    },
    "connector_oauth_request": {
      "type": "object",
      "properties": {
        "auth_code": {
          "description": "The code provided by the connector when it has been authenticated via the connected app.",
          "type": "string"
        },
        "redirect_uri": {
          "description": "The URL to which the authentication server redirects the browser after authorization has been\ngranted.",
          "type": "string"
        }
      }
    },
    "client_credentials_arn": {
      "type": "string",
      "pattern": "arn:aws:secretsmanager:.*:[0-9]+:.*",
      "maxLength": 2048
    },
    "application_host_url": {
      "type": "string",
      "maxLength": 256,
      "pattern": "^(https?)://[-a-zA-Z0-9+&amp;@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&amp;@#/%=~_|]"
    },
    "application_service_path": {
      "type": "string",
      "pattern": "\\S+",
      "maxLength": 512
    },
    "client_number": {
      "type": "string",
      "pattern": "^\\d{3}$",
      "minLength": 3,
      "maxLength": 3
    },
    "logon_language": {
      "type": "string",
      "pattern": "^[a-zA-Z0-9_]*$",
      "maxLength": 2
    },
    "port_number": {
      "type": "integer",
      "minimum": 1,
      "maximum": 65535
    },
    "o_auth_properties": {
      "type": "object",
      "properties": {
        "auth_code_url": {
          "type": "string",
          "maxLength": 256,
          "pattern": "^(https?)://[-a-zA-Z0-9+&amp;@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&amp;@#/%=~_|]"
        },
        "token_url": {
          "type": "string",
          "maxLength": 256,
          "pattern": "^(https?)://[-a-zA-Z0-9+&amp;@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&amp;@#/%=~_|]"
        },
        "o_auth_scopes": {
          "type": "array",
          "uniqueItems": true,
          "items": {
            "type": "string",
            "maxLength": 128,
            "pattern": "[/\\w]*"
          }
        }
      }
    }
  },
  "required": [
    "connector_profile_name",
    "connection_mode",
    "connector_type"
  ],
  "createOnlyProperties": [
    "/properties/connector_profile_name",
    "/properties/kms_arn",
    "/properties/connector_type"
  ],
  "readOnlyProperties": [
    "/properties/connector_profile_arn",
    "/properties/credentials_arn"
  ],
  "writeOnlyProperties": [
    "/properties/connector_profile_config"
  ],
  "primaryIdentifier": [
    "/properties/connector_profile_name"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "appflow:CreateConnectorProfile",
        "kms:ListKeys",
        "kms:DescribeKey",
        "kms:ListAliases",
        "kms:CreateGrant",
        "kms:ListGrants",
        "iam:PassRole",
        "secretsmanager:CreateSecret",
        "secretsmanager:GetSecretValue",
        "secretsmanager:PutResourcePolicy"
      ]
    },
    "delete": {
      "permissions": [
        "appflow:DeleteConnectorProfile"
      ]
    },
    "list": {
      "permissions": [
        "appflow:DescribeConnectorProfiles"
      ]
    },
    "read": {
      "permissions": [
        "appflow:DescribeConnectorProfiles"
      ]
    },
    "update": {
      "permissions": [
        "appflow:UpdateConnectorProfile",
        "kms:ListKeys",
        "kms:DescribeKey",
        "kms:ListAliases",
        "kms:CreateGrant",
        "kms:ListGrants",
        "iam:PassRole",
        "secretsmanager:CreateSecret",
        "secretsmanager:GetSecretValue",
        "secretsmanager:PutResourcePolicy"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}