{
  "typeName": "AWS::SSMIncidents::ReplicationSet",
  "description": "Resource type definition for AWS::SSMIncidents::ReplicationSet",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-ssm-incidents.git",
  "definitions": {
    "arn": {
      "description": "The ARN of the ReplicationSet.",
      "type": "string",
      "pattern": "^arn:aws(-(cn|us-gov|iso(-b)?))?:[a-z-]+:(([a-z]+-)+[0-9])?:([0-9]{12})?:[^.]+$",
      "maxLength": 1000
    },
    "region_name": {
      "description": "The AWS region name.",
      "type": "string",
      "maxLength": 20
    },
    "replication_region": {
      "description": "The ReplicationSet regional configuration.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "region_name": {
          "$ref": "#/definitions/region_name"
        },
        "region_configuration": {
          "$ref": "#/definitions/region_configuration"
        }
      }
    },
    "region_configuration": {
      "description": "The ReplicationSet regional configuration.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "sse_kms_key_id": {
          "$ref": "#/definitions/arn"
        }
      },
      "required": [
        "sse_kms_key_id"
      ]
    },
    "deletion_protected": {
      "description": "Configures the ReplicationSet deletion protection.",
      "type": "boolean"
    },
    "region_list": {
      "type": "array",
      "minItems": 1,
      "maxItems": 3,
      "items": {
        "$ref": "#/definitions/replication_region"
      },
      "insertionOrder": false,
      "uniqueItems": true
    }
  },
  "properties": {
    "arn": {
      "description": "The ARN of the ReplicationSet.",
      "$ref": "#/definitions/arn",
      "additionalProperties": false
    },
    "regions": {
      "description": "The ReplicationSet configuration.",
      "$ref": "#/definitions/region_list"
    },
    "deletion_protected": {
      "$ref": "#/definitions/deletion_protected",
      "default": false
    }
  },
  "additionalProperties": false,
  "primaryIdentifier": [
    "/properties/arn"
  ],
  "required": [
    "regions"
  ],
  "taggable": false,
  "readOnlyProperties": [
    "/properties/arn"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "ssm-incidents:CreateReplicationSet",
        "ssm-incidents:ListReplicationSets",
        "ssm-incidents:UpdateDeletionProtection",
        "iam:CreateServiceLinkedRole"
      ]
    },
    "read": {
      "permissions": [
        "ssm-incidents:ListReplicationSets",
        "ssm-incidents:GetReplicationSet"
      ]
    },
    "update": {
      "permissions": [
        "ssm-incidents:UpdateReplicationSet",
        "ssm-incidents:UpdateDeletionProtection",
        "ssm-incidents:GetReplicationSet"
      ]
    },
    "delete": {
      "permissions": [
        "ssm-incidents:DeleteReplicationSet",
        "ssm-incidents:GetReplicationSet"
      ]
    },
    "list": {
      "permissions": [
        "ssm-incidents:ListReplicationSets"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}