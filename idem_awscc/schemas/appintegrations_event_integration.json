{
  "typeName": "AWS::AppIntegrations::EventIntegration",
  "description": "Resource Type definition for AWS::AppIntegrations::EventIntegration",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-rpdk.git",
  "definitions": {
    "event_filter": {
      "type": "object",
      "properties": {
        "source": {
          "description": "The source of the events.",
          "type": "string",
          "pattern": "^aws\\.partner\\/.*$",
          "minLength": 1,
          "maxLength": 256
        }
      },
      "additionalProperties": false,
      "required": [
        "source"
      ]
    },
    "tag": {
      "type": "object",
      "properties": {
        "key": {
          "description": "A key to identify the tag.",
          "type": "string",
          "pattern": "^(?!aws:)[a-zA-Z+-=._:/]+$",
          "minLength": 1,
          "maxLength": 128
        },
        "value": {
          "description": "Corresponding tag value for the key.",
          "type": "string",
          "minLength": 0,
          "maxLength": 256
        }
      },
      "additionalProperties": false,
      "required": [
        "key",
        "value"
      ]
    },
    "metadata": {
      "type": "object",
      "properties": {
        "key": {
          "description": "A key to identify the metadata.",
          "type": "string",
          "pattern": ".*\\S.*",
          "minLength": 1,
          "maxLength": 255
        },
        "value": {
          "description": "Corresponding metadata value for the key.",
          "type": "string",
          "pattern": ".*\\S.*",
          "minLength": 1,
          "maxLength": 255
        }
      },
      "additionalProperties": false,
      "required": [
        "key",
        "value"
      ]
    },
    "event_integration_association": {
      "type": "object",
      "properties": {
        "client_association_metadata": {
          "description": "The metadata associated with the client.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/metadata"
          }
        },
        "client_id": {
          "description": "The identifier for the client that is associated with the event integration.",
          "type": "string",
          "minLength": 1,
          "maxLength": 255
        },
        "event_bridge_rule_name": {
          "description": "The name of the Eventbridge rule.",
          "type": "string",
          "pattern": "^[a-zA-Z0-9/\\._\\-]+$",
          "minLength": 1,
          "maxLength": 2048
        },
        "event_integration_association_arn": {
          "description": "The Amazon Resource Name (ARN) for the event integration association.",
          "type": "string",
          "pattern": "^arn:aws[-a-z]*:[A-Za-z0-9][A-Za-z0-9_/.-]{0,62}:[A-Za-z0-9_/.-]{0,63}:[A-Za-z0-9_/.-]{0,63}:[A-Za-z0-9][A-Za-z0-9:_/+=,@.-]{0,1023}$",
          "minLength": 1,
          "maxLength": 2048
        },
        "event_integration_association_id": {
          "description": "The identifier for the event integration association.",
          "type": "string",
          "pattern": "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}"
        }
      },
      "additionalProperties": false,
      "required": []
    }
  },
  "properties": {
    "description": {
      "description": "The event integration description.",
      "type": "string",
      "minLength": 1,
      "maxLength": 1000
    },
    "event_integration_arn": {
      "description": "The Amazon Resource Name (ARN) of the event integration.",
      "type": "string",
      "pattern": "^arn:aws[-a-z]*:[A-Za-z0-9][A-Za-z0-9_/.-]{0,62}:[A-Za-z0-9_/.-]{0,63}:[A-Za-z0-9_/.-]{0,63}:[A-Za-z0-9][A-Za-z0-9:_/+=,@.-]{0,1023}$",
      "minLength": 1,
      "maxLength": 2048
    },
    "name": {
      "description": "The name of the event integration.",
      "type": "string",
      "pattern": "^[a-zA-Z0-9/\\._\\-]+$",
      "minLength": 1,
      "maxLength": 255
    },
    "event_bridge_bus": {
      "description": "The Amazon Eventbridge bus for the event integration.",
      "type": "string",
      "pattern": "^[a-zA-Z0-9/\\._\\-]+$",
      "minLength": 1,
      "maxLength": 255
    },
    "event_filter": {
      "description": "The EventFilter (source) associated with the event integration.",
      "$ref": "#/definitions/event_filter"
    },
    "tags": {
      "description": "The tags (keys and values) associated with the event integration.",
      "type": "array",
      "items": {
        "$ref": "#/definitions/tag"
      },
      "minItems": 0,
      "maxItems": 200
    },
    "associations": {
      "description": "The associations with the event integration.",
      "type": "array",
      "items": {
        "$ref": "#/definitions/event_integration_association"
      },
      "minItems": 0
    }
  },
  "additionalProperties": false,
  "required": [
    "name",
    "event_bridge_bus",
    "event_filter"
  ],
  "readOnlyProperties": [
    "/properties/event_integration_arn",
    "/properties/associations"
  ],
  "createOnlyProperties": [
    "/properties/name",
    "/properties/event_bridge_bus",
    "/properties/event_filter"
  ],
  "primaryIdentifier": [
    "/properties/name"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "app-integrations:CreateEventIntegration"
      ]
    },
    "read": {
      "permissions": [
        "app-integrations:GetEventIntegration",
        "app-integrations:ListEventIntegrationAssociations",
        "app-integrations:ListTagsForResource"
      ]
    },
    "list": {
      "permissions": [
        "app-integrations:ListEventIntegrations"
      ]
    },
    "update": {
      "permissions": [
        "app-integrations:GetEventIntegration",
        "app-integrations:UpdateEventIntegration",
        "app-integrations:TagResource",
        "app-integrations:UntagResource"
      ]
    },
    "delete": {
      "permissions": [
        "app-integrations:DeleteEventIntegration"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}