{
  "typeName": "AWS::IoTWireless::Destination",
  "description": "Destination's resource schema demonstrating some basic constructs and validation rules.",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-rpdk.git",
  "definitions": {
    "tag": {
      "type": "object",
      "properties": {
        "key": {
          "type": "string",
          "minLength": 1,
          "maxLength": 127
        },
        "value": {
          "type": "string",
          "minLength": 1,
          "maxLength": 255
        }
      },
      "additionalProperties": false
    }
  },
  "properties": {
    "name": {
      "description": "Unique name of destination",
      "type": "string",
      "pattern": "[a-zA-Z0-9:_-]+",
      "maxLength": 128
    },
    "expression": {
      "description": "Destination expression",
      "type": "string"
    },
    "expression_type": {
      "description": "Must be RuleName",
      "type": "string",
      "enum": [
        "RuleName",
        "MqttTopic"
      ]
    },
    "description": {
      "description": "Destination description",
      "type": "string",
      "maxLength": 2048
    },
    "tags": {
      "description": "A list of key-value pairs that contain metadata for the destination.",
      "type": "array",
      "uniqueItems": true,
      "maxItems": 200,
      "insertionOrder": false,
      "items": {
        "$ref": "#/definitions/tag"
      }
    },
    "role_arn": {
      "description": "AWS role ARN that grants access",
      "type": "string",
      "minLength": 20,
      "maxLength": 2048
    },
    "arn": {
      "description": "Destination arn. Returned after successful create.",
      "type": "string"
    }
  },
  "additionalProperties": false,
  "required": [
    "name",
    "expression",
    "expression_type",
    "role_arn"
  ],
  "readOnlyProperties": [
    "/properties/arn"
  ],
  "createOnlyProperties": [
    "/properties/name"
  ],
  "primaryIdentifier": [
    "/properties/name"
  ],
  "taggable": true,
  "handlers": {
    "create": {
      "permissions": [
        "iotwireless:CreateDestination",
        "iotwireless:TagResource",
        "iotwireless:ListTagsForResource"
      ]
    },
    "read": {
      "permissions": [
        "iotwireless:GetDestination",
        "iotwireless:ListTagsForResource"
      ]
    },
    "update": {
      "permissions": [
        "iotwireless:UpdateDestination",
        "iotwireless:UntagResource",
        "iotwireless:ListTagsForResource"
      ]
    },
    "delete": {
      "permissions": [
        "iotwireless:DeleteDestination"
      ]
    },
    "list": {
      "permissions": [
        "iotwireless:ListDestinations",
        "iotwireless:ListTagsForResource"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}