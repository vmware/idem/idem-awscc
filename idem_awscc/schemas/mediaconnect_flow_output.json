{
  "typeName": "AWS::MediaConnect::FlowOutput",
  "description": "Resource schema for AWS::MediaConnect::FlowOutput",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-mediaconnect.git",
  "definitions": {
    "encryption": {
      "type": "object",
      "description": "Information about the encryption of the flow.",
      "properties": {
        "algorithm": {
          "type": "string",
          "enum": [
            "aes128",
            "aes192",
            "aes256"
          ],
          "description": "The type of algorithm that is used for the encryption (such as aes128, aes192, or aes256)."
        },
        "key_type": {
          "type": "string",
          "enum": [
            "static-key",
            "srt-password"
          ],
          "description": "The type of key that is used for the encryption. If no keyType is provided, the service will use the default setting (static-key).",
          "default": "static-key"
        },
        "role_arn": {
          "type": "string",
          "description": "The ARN of the role that you created during setup (when you set up AWS Elemental MediaConnect as a trusted entity)."
        },
        "secret_arn": {
          "type": "string",
          "description": " The ARN of the secret that you created in AWS Secrets Manager to store the encryption key. This parameter is required for static key encryption and is not valid for SPEKE encryption."
        }
      },
      "additionalProperties": false,
      "required": [
        "role_arn",
        "secret_arn"
      ]
    },
    "vpc_interface_attachment": {
      "type": "object",
      "description": "The settings for attaching a VPC interface to an output.",
      "properties": {
        "vpc_interface_name": {
          "type": "string",
          "description": "The name of the VPC interface to use for this output."
        }
      },
      "additionalProperties": false
    }
  },
  "properties": {
    "flow_arn": {
      "description": "The Amazon Resource Name (ARN), a unique identifier for any AWS resource, of the flow.",
      "type": "string"
    },
    "output_arn": {
      "description": "The ARN of the output.",
      "type": "string"
    },
    "cidr_allow_list": {
      "type": "array",
      "description": "The range of IP addresses that should be allowed to initiate output requests to this flow. These IP addresses should be in the form of a Classless Inter-Domain Routing (CIDR) block; for example, 10.0.0.0/16.",
      "items": {
        "type": "string"
      }
    },
    "encryption": {
      "$ref": "#/definitions/encryption",
      "description": "The type of key used for the encryption. If no keyType is provided, the service will use the default setting (static-key)."
    },
    "description": {
      "type": "string",
      "description": "A description of the output."
    },
    "destination": {
      "type": "string",
      "description": "The address where you want to send the output."
    },
    "max_latency": {
      "type": "integer",
      "description": "The maximum latency in milliseconds. This parameter applies only to RIST-based and Zixi-based streams."
    },
    "min_latency": {
      "type": "integer",
      "description": "The minimum latency in milliseconds."
    },
    "name": {
      "type": "string",
      "description": "The name of the output. This value must be unique within the current flow."
    },
    "port": {
      "type": "integer",
      "description": "The port to use when content is distributed to this output."
    },
    "protocol": {
      "type": "string",
      "enum": [
        "zixi-push",
        "rtp-fec",
        "rtp",
        "zixi-pull",
        "rist",
        "srt-listener"
      ],
      "description": "The protocol that is used by the source or output."
    },
    "remote_id": {
      "type": "string",
      "description": "The remote ID for the Zixi-pull stream."
    },
    "smoothing_latency": {
      "type": "integer",
      "description": "The smoothing latency in milliseconds for RIST, RTP, and RTP-FEC streams."
    },
    "stream_id": {
      "type": "string",
      "description": "The stream ID that you want to use for this transport. This parameter applies only to Zixi-based streams."
    },
    "vpc_interface_attachment": {
      "$ref": "#/definitions/vpc_interface_attachment",
      "description": "The name of the VPC interface attachment to use for this output."
    }
  },
  "additionalProperties": false,
  "required": [
    "flow_arn",
    "protocol"
  ],
  "createOnlyProperties": [
    "/properties/name"
  ],
  "readOnlyProperties": [
    "/properties/output_arn"
  ],
  "primaryIdentifier": [
    "/properties/output_arn"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "iam:PassRole",
        "mediaconnect:AddFlowOutputs"
      ]
    },
    "read": {
      "permissions": [
        "mediaconnect:DescribeFlow"
      ]
    },
    "update": {
      "permissions": [
        "mediaconnect:DescribeFlow",
        "mediaconnect:UpdateFlowOutput"
      ]
    },
    "delete": {
      "permissions": [
        "mediaconnect:DescribeFlow",
        "mediaconnect:RemoveFlowOutput"
      ]
    },
    "list": {
      "permissions": [
        "mediaconnect:DescribeFlow"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}