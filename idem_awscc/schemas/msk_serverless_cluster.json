{
  "typeName": "AWS::MSK::ServerlessCluster",
  "description": "Resource Type definition for AWS::MSK::ServerlessCluster",
  "additionalProperties": false,
  "properties": {
    "arn": {
      "type": "string"
    },
    "cluster_name": {
      "type": "string",
      "minLength": 1,
      "maxLength": 64
    },
    "vpc_configs": {
      "type": "array",
      "uniqueItems": true,
      "insertionOrder": false,
      "items": {
        "$ref": "#/definitions/vpc_config"
      }
    },
    "client_authentication": {
      "$ref": "#/definitions/client_authentication"
    },
    "tags": {
      "type": "object",
      "description": "A key-value pair to associate with a resource.",
      "patternProperties": {
        "^([\\p{L}\\p{Z}\\p{N}_.:/=+\\-@]*)$": {
          "type": "string"
        }
      },
      "additionalProperties": false
    }
  },
  "definitions": {
    "vpc_config": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "security_groups": {
          "type": "array",
          "uniqueItems": true,
          "insertionOrder": false,
          "items": {
            "type": "string"
          }
        },
        "subnet_ids": {
          "type": "array",
          "uniqueItems": true,
          "insertionOrder": false,
          "items": {
            "type": "string"
          }
        }
      },
      "required": [
        "subnet_ids"
      ]
    },
    "client_authentication": {
      "type": "object",
      "properties": {
        "sasl": {
          "$ref": "#/definitions/sasl"
        }
      },
      "additionalProperties": false,
      "required": [
        "sasl"
      ]
    },
    "sasl": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "iam": {
          "$ref": "#/definitions/iam"
        }
      },
      "required": [
        "iam"
      ]
    },
    "iam": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "enabled": {
          "type": "boolean"
        }
      },
      "required": [
        "enabled"
      ]
    }
  },
  "required": [
    "cluster_name",
    "vpc_configs",
    "client_authentication"
  ],
  "createOnlyProperties": [
    "/properties/cluster_name",
    "/properties/vpc_configs",
    "/properties/client_authentication",
    "/properties/tags"
  ],
  "primaryIdentifier": [
    "/properties/arn"
  ],
  "readOnlyProperties": [
    "/properties/arn"
  ],
  "tagging": {
    "taggable": true,
    "tagOnCreate": true,
    "tagUpdatable": false,
    "cloudFormationSystemTags": true,
    "tagProperty": "/properties/Tags"
  },
  "handlers": {
    "create": {
      "permissions": [
        "kafka:CreateClusterV2",
        "kafka:TagResource",
        "ec2:CreateVpcEndpoint",
        "ec2:CreateTags",
        "ec2:DescribeVpcAttribute",
        "ec2:DescribeSubnets",
        "ec2:DescribeVpcEndpoints",
        "ec2:DescribeVpcs",
        "ec2:DescribeSecurityGroups"
      ],
      "timeoutInMinutes": 120
    },
    "read": {
      "permissions": [
        "kafka:DescribeClusterV2"
      ]
    },
    "delete": {
      "permissions": [
        "kafka:DeleteCluster",
        "ec2:DeleteVpcEndpoints"
      ],
      "timeoutInMinutes": 75
    },
    "list": {
      "permissions": [
        "kafka:ListClustersV2"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": false
}