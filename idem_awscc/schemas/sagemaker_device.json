{
  "typeName": "AWS::SageMaker::Device",
  "description": "Resource schema for AWS::SageMaker::Device",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-sagemaker-edge.git",
  "definitions": {
    "device": {
      "description": "Edge device you want to create",
      "type": "object",
      "properties": {
        "description": {
          "description": "Description of the device",
          "type": "string",
          "pattern": "[\\S\\s]+",
          "minLength": 1,
          "maxLength": 40
        },
        "device_name": {
          "description": "The name of the device",
          "type": "string",
          "pattern": "^[a-zA-Z0-9](-*[a-zA-Z0-9])*$",
          "minLength": 1,
          "maxLength": 63
        },
        "iot_thing_name": {
          "description": "AWS Internet of Things (IoT) object name.",
          "type": "string",
          "pattern": "[a-zA-Z0-9:_-]+",
          "maxLength": 128
        }
      },
      "required": [
        "device_name"
      ],
      "additionalProperties": false
    },
    "tag": {
      "type": "object",
      "properties": {
        "key": {
          "description": "The key name of the tag. You can specify a value that is 1 to 127 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -. ",
          "type": "string",
          "pattern": "^((?!aws:)[\\p{L}\\p{Z}\\p{N}_.:/=+\\-@]*)$",
          "minLength": 1,
          "maxLength": 128
        },
        "value": {
          "description": "The key value of the tag. You can specify a value that is 1 to 127 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -. ",
          "type": "string",
          "pattern": "^([\\p{L}\\p{Z}\\p{N}_.:/=+\\-@]*)$",
          "minLength": 0,
          "maxLength": 256
        }
      },
      "required": [
        "key",
        "value"
      ],
      "additionalProperties": false
    }
  },
  "properties": {
    "device_fleet_name": {
      "description": "The name of the edge device fleet",
      "type": "string",
      "pattern": "^[a-zA-Z0-9](-*_*[a-zA-Z0-9])*$",
      "minLength": 1,
      "maxLength": 63
    },
    "device": {
      "description": "The Edge Device you want to register against a device fleet",
      "$ref": "#/definitions/device"
    },
    "tags": {
      "description": "Associate tags with the resource",
      "type": "array",
      "items": {
        "$ref": "#/definitions/tag"
      }
    }
  },
  "required": [
    "device_fleet_name"
  ],
  "additionalProperties": false,
  "primaryIdentifier": [
    "/properties/device/device_name"
  ],
  "createOnlyProperties": [
    "/properties/device/device_name"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "sagemaker:RegisterDevices"
      ]
    },
    "read": {
      "permissions": [
        "sagemaker:DescribeDevice"
      ]
    },
    "update": {
      "permissions": [
        "sagemaker:UpdateDevices"
      ]
    },
    "delete": {
      "permissions": [
        "sagemaker:DeregisterDevices"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}