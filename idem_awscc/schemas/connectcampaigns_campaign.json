{
  "typeName": "AWS::ConnectCampaigns::Campaign",
  "description": "Definition of AWS::ConnectCampaigns::Campaign Resource Type",
  "definitions": {
    "dialer_config": {
      "description": "The possible types of dialer config parameters",
      "oneOf": [
        {
          "type": "object",
          "title": "ProgressiveDialerConfig",
          "properties": {
            "progressive_dialer_config": {
              "$ref": "#/definitions/progressive_dialer_config"
            }
          },
          "required": [
            "progressive_dialer_config"
          ],
          "additionalProperties": false
        },
        {
          "type": "object",
          "title": "PredictiveDialerConfig",
          "properties": {
            "predictive_dialer_config": {
              "$ref": "#/definitions/predictive_dialer_config"
            }
          },
          "required": [
            "predictive_dialer_config"
          ],
          "additionalProperties": false
        }
      ]
    },
    "outbound_call_config": {
      "type": "object",
      "description": "The configuration used for outbound calls.",
      "properties": {
        "connect_contact_flow_arn": {
          "type": "string",
          "maxLength": 500,
          "description": "The identifier of the contact flow for the outbound call.",
          "pattern": "^arn:aws[-a-z0-9]*:connect:[-a-z0-9]*:[0-9]{12}:instance/[-a-zA-Z0-9]*/contact-flow/[-a-zA-Z0-9]*$"
        },
        "connect_source_phone_number": {
          "type": "string",
          "maxLength": 100,
          "description": "The phone number associated with the Amazon Connect instance, in E.164 format. If you do not specify a source phone number, you must specify a queue."
        },
        "connect_queue_arn": {
          "type": "string",
          "maxLength": 500,
          "description": "The queue for the call. If you specify a queue, the phone displayed for caller ID is the phone number specified in the queue. If you do not specify a queue, the queue defined in the contact flow is used. If you do not specify a queue, you must specify a source phone number.",
          "pattern": "^arn:aws[-a-z0-9]*:connect:[-a-z0-9]*:[0-9]{12}:instance/[-a-zA-Z0-9]*/queue/[-a-zA-Z0-9]*$"
        }
      },
      "required": [
        "connect_contact_flow_arn",
        "connect_queue_arn"
      ],
      "additionalProperties": false
    },
    "predictive_dialer_config": {
      "type": "object",
      "description": "Predictive Dialer config",
      "properties": {
        "bandwidth_allocation": {
          "type": "number",
          "maximum": 1,
          "minimum": 0,
          "description": "The bandwidth allocation of a queue resource."
        }
      },
      "required": [
        "bandwidth_allocation"
      ],
      "additionalProperties": false
    },
    "progressive_dialer_config": {
      "type": "object",
      "description": "Progressive Dialer config",
      "properties": {
        "bandwidth_allocation": {
          "type": "number",
          "maximum": 1,
          "minimum": 0,
          "description": "The bandwidth allocation of a queue resource."
        }
      },
      "required": [
        "bandwidth_allocation"
      ],
      "additionalProperties": false
    },
    "tag": {
      "description": "A key-value pair to associate with a resource.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "key": {
          "type": "string",
          "description": "The key name of the tag. You can specify a value that is 1 to 128 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -. ",
          "minLength": 1,
          "maxLength": 128,
          "pattern": "^(?!aws:)[a-zA-Z+-=._:/]+$"
        },
        "value": {
          "type": "string",
          "description": "The value for the tag. You can specify a value that's 1 to 256 characters in length.",
          "minLength": 1,
          "maxLength": 256
        }
      },
      "required": [
        "key",
        "value"
      ]
    }
  },
  "properties": {
    "connect_instance_arn": {
      "type": "string",
      "maxLength": 256,
      "minLength": 0,
      "description": "Amazon Connect Instance Arn",
      "pattern": "^arn:aws[-a-z0-9]*:connect:[-a-z0-9]*:[0-9]{12}:instance/[-a-zA-Z0-9]*$"
    },
    "dialer_config": {
      "$ref": "#/definitions/dialer_config"
    },
    "arn": {
      "type": "string",
      "maxLength": 256,
      "minLength": 0,
      "description": "Amazon Connect Campaign Arn",
      "pattern": "^arn:aws[-a-z0-9]*:connect-campaigns:[-a-z0-9]*:[0-9]{12}:campaign/[-a-zA-Z0-9]*$"
    },
    "name": {
      "type": "string",
      "maxLength": 127,
      "minLength": 1,
      "description": "Amazon Connect Campaign Name"
    },
    "outbound_call_config": {
      "$ref": "#/definitions/outbound_call_config"
    },
    "tags": {
      "type": "array",
      "maxItems": 50,
      "uniqueItems": true,
      "insertionOrder": false,
      "description": "One or more tags.",
      "items": {
        "$ref": "#/definitions/tag"
      }
    }
  },
  "tagging": {
    "taggable": true,
    "tagOnCreate": true,
    "tagUpdatable": true,
    "cloudFormationSystemTags": false,
    "tagProperty": "/properties/Tags"
  },
  "required": [
    "connect_instance_arn",
    "dialer_config",
    "name",
    "outbound_call_config"
  ],
  "readOnlyProperties": [
    "/properties/arn"
  ],
  "createOnlyProperties": [
    "/properties/connect_instance_arn"
  ],
  "primaryIdentifier": [
    "/properties/arn"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "connect-campaigns:CreateCampaign",
        "connect-campaigns:DescribeCampaign",
        "connect-campaigns:TagResource",
        "connect:DescribeContactFlow",
        "connect:DescribeInstance",
        "connect:DescribeQueue"
      ]
    },
    "read": {
      "permissions": [
        "connect-campaigns:DescribeCampaign"
      ]
    },
    "delete": {
      "permissions": [
        "connect-campaigns:DeleteCampaign"
      ]
    },
    "list": {
      "permissions": [
        "connect-campaigns:ListCampaigns"
      ]
    },
    "update": {
      "permissions": [
        "connect-campaigns:UpdateCampaignDialerConfig",
        "connect-campaigns:UpdateCampaignName",
        "connect-campaigns:UpdateCampaignOutboundCallConfig",
        "connect-campaigns:TagResource",
        "connect-campaigns:UntagResource",
        "connect-campaigns:DescribeCampaign"
      ]
    }
  },
  "additionalProperties": false,
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}