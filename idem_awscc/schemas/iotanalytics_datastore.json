{
  "typeName": "AWS::IoTAnalytics::Datastore",
  "description": "Resource Type definition for AWS::IoTAnalytics::Datastore",
  "additionalProperties": false,
  "taggable": true,
  "properties": {
    "datastore_storage": {
      "$ref": "#/definitions/datastore_storage"
    },
    "datastore_name": {
      "type": "string",
      "pattern": "[a-zA-Z0-9_]+",
      "minLength": 1,
      "maxLength": 128
    },
    "datastore_partitions": {
      "$ref": "#/definitions/datastore_partitions"
    },
    "id": {
      "type": "string"
    },
    "file_format_configuration": {
      "$ref": "#/definitions/file_format_configuration"
    },
    "retention_period": {
      "$ref": "#/definitions/retention_period"
    },
    "tags": {
      "type": "array",
      "uniqueItems": false,
      "insertionOrder": false,
      "minItems": 1,
      "maxItems": 50,
      "items": {
        "$ref": "#/definitions/tag"
      }
    }
  },
  "definitions": {
    "datastore_storage": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "service_manageds3": {
          "$ref": "#/definitions/service_manageds3"
        },
        "customer_manageds3": {
          "$ref": "#/definitions/customer_manageds3"
        },
        "iot_site_wise_multi_layer_storage": {
          "$ref": "#/definitions/iot_site_wise_multi_layer_storage"
        }
      }
    },
    "schema_definition": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "columns": {
          "type": "array",
          "uniqueItems": false,
          "insertionOrder": false,
          "minItems": 1,
          "maxItems": 100,
          "items": {
            "$ref": "#/definitions/column"
          }
        }
      }
    },
    "json_configuration": {
      "type": "object",
      "additionalProperties": false
    },
    "parquet_configuration": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "schema_definition": {
          "$ref": "#/definitions/schema_definition"
        }
      }
    },
    "file_format_configuration": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "json_configuration": {
          "$ref": "#/definitions/json_configuration"
        },
        "parquet_configuration": {
          "$ref": "#/definitions/parquet_configuration"
        }
      }
    },
    "column": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "type": {
          "type": "string"
        },
        "name": {
          "type": "string"
        }
      },
      "required": [
        "type",
        "name"
      ]
    },
    "customer_manageds3": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "bucket": {
          "type": "string",
          "pattern": "[a-zA-Z0-9.\\-_]*",
          "minLength": 3,
          "maxLength": 255
        },
        "role_arn": {
          "type": "string",
          "minLength": 20,
          "maxLength": 2048
        },
        "key_prefix": {
          "type": "string",
          "pattern": "[a-zA-Z0-9!_.*'()/{}:-]*/",
          "minLength": 1,
          "maxLength": 255
        }
      },
      "required": [
        "bucket",
        "role_arn"
      ]
    },
    "iot_site_wise_multi_layer_storage": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "customer_manageds3_storage": {
          "$ref": "#/definitions/customer_manageds3_storage"
        }
      }
    },
    "customer_manageds3_storage": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "bucket": {
          "type": "string",
          "pattern": "[a-zA-Z0-9.\\-_]*",
          "minLength": 3,
          "maxLength": 255
        },
        "key_prefix": {
          "type": "string",
          "pattern": "[a-zA-Z0-9!_.*'()/{}:-]*/",
          "minLength": 1,
          "maxLength": 255
        }
      },
      "required": [
        "bucket"
      ]
    },
    "service_manageds3": {
      "type": "object",
      "additionalProperties": false
    },
    "tag": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "key": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "value": {
          "type": "string",
          "minLength": 1,
          "maxLength": 256
        }
      },
      "required": [
        "value",
        "key"
      ]
    },
    "retention_period": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "number_of_days": {
          "type": "integer",
          "minimum": 1,
          "maximum": 2147483647
        },
        "unlimited": {
          "type": "boolean"
        }
      }
    },
    "datastore_partitions": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "partitions": {
          "type": "array",
          "uniqueItems": false,
          "insertionOrder": false,
          "minItems": 0,
          "maxItems": 25,
          "items": {
            "$ref": "#/definitions/datastore_partition"
          }
        }
      }
    },
    "datastore_partition": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "partition": {
          "$ref": "#/definitions/partition"
        },
        "timestamp_partition": {
          "$ref": "#/definitions/timestamp_partition"
        }
      }
    },
    "partition": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "attribute_name": {
          "type": "string",
          "pattern": "[a-zA-Z0-9_]+"
        }
      },
      "required": [
        "attribute_name"
      ]
    },
    "timestamp_partition": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "attribute_name": {
          "type": "string",
          "pattern": "[a-zA-Z0-9_]+"
        },
        "timestamp_format": {
          "type": "string",
          "pattern": "[a-zA-Z0-9\\s\\[\\]_,.'/:-]*"
        }
      },
      "required": [
        "attribute_name"
      ]
    }
  },
  "primaryIdentifier": [
    "/properties/datastore_name"
  ],
  "createOnlyProperties": [
    "/properties/datastore_name"
  ],
  "readOnlyProperties": [
    "/properties/id"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "iotanalytics:CreateDatastore"
      ]
    },
    "read": {
      "permissions": [
        "iotanalytics:DescribeDatastore",
        "iotanalytics:ListTagsForResource"
      ]
    },
    "update": {
      "permissions": [
        "iotanalytics:UpdateDatastore",
        "iotanalytics:TagResource",
        "iotanalytics:UntagResource"
      ]
    },
    "delete": {
      "permissions": [
        "iotanalytics:DeleteDatastore"
      ]
    },
    "list": {
      "permissions": [
        "iotanalytics:ListDatastores"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}