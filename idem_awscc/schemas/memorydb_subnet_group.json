{
  "typeName": "AWS::MemoryDB::SubnetGroup",
  "description": "The AWS::MemoryDB::SubnetGroup resource creates an Amazon MemoryDB Subnet Group.",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-memorydb",
  "taggable": true,
  "definitions": {
    "tag": {
      "description": "A key-value pair to associate with a resource.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "key": {
          "description": "The key for the tag. May not be null.",
          "pattern": "^(?!aws:)(?!memorydb:)[a-zA-Z0-9 _\\.\\/=+:\\-@]{1,128}$",
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "value": {
          "description": "The tag's value. May be null.",
          "type": "string",
          "pattern": "^(?!aws:)(?!memorydb:)[a-zA-Z0-9 _\\.\\/=+:\\-@]{1,256}$",
          "minLength": 1,
          "maxLength": 256
        }
      },
      "required": [
        "key",
        "value"
      ]
    }
  },
  "properties": {
    "subnet_group_name": {
      "description": "The name of the subnet group. This value must be unique as it also serves as the subnet group identifier.",
      "pattern": "[a-z][a-z0-9\\-]*",
      "type": "string"
    },
    "description": {
      "description": "An optional description of the subnet group.",
      "type": "string"
    },
    "subnet_ids": {
      "description": "A list of VPC subnet IDs for the subnet group.",
      "type": "array",
      "uniqueItems": true,
      "insertionOrder": false,
      "items": {
        "type": "string"
      }
    },
    "tags": {
      "description": "An array of key-value pairs to apply to this subnet group.",
      "type": "array",
      "maxItems": 50,
      "uniqueItems": true,
      "insertionOrder": false,
      "items": {
        "$ref": "#/definitions/tag"
      }
    },
    "arn": {
      "description": "The Amazon Resource Name (ARN) of the subnet group.",
      "type": "string"
    }
  },
  "additionalProperties": false,
  "required": [
    "subnet_group_name",
    "subnet_ids"
  ],
  "primaryIdentifier": [
    "/properties/subnet_group_name"
  ],
  "createOnlyProperties": [
    "/properties/subnet_group_name"
  ],
  "readOnlyProperties": [
    "/properties/arn"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "memorydb:CreateSubnetGroup",
        "memorydb:DescribeSubnetGroups",
        "memorydb:ListTags"
      ]
    },
    "read": {
      "permissions": [
        "memorydb:DescribeSubnetGroups",
        "memorydb:ListTags"
      ]
    },
    "update": {
      "permissions": [
        "memorydb:UpdateSubnetGroup",
        "memorydb:DescribeSubnetGroups",
        "memorydb:ListTags",
        "memorydb:TagResource",
        "memorydb:UntagResource"
      ]
    },
    "delete": {
      "permissions": [
        "memorydb:DeleteSubnetGroup",
        "memorydb:DescribeSubnetGroups"
      ]
    },
    "list": {
      "permissions": [
        "memorydb:DescribeSubnetGroups"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}