{
  "typeName": "AWS::LakeFormation::DataCellsFilter",
  "description": "A resource schema representing a Lake Formation Data Cells Filter.",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-rpdk.git",
  "definitions": {
    "catalog_id_string": {
      "description": "A string representing the Catalog Id.",
      "type": "string",
      "minLength": 12,
      "maxLength": 12
    },
    "name_string": {
      "description": "A string representing a resource's name.",
      "type": "string",
      "minLength": 1,
      "maxLength": 255
    },
    "row_filter": {
      "description": "An object representing the Data Cells Filter's Row Filter. Either a Filter Expression or a Wildcard is required.",
      "type": "object",
      "properties": {
        "filter_expression": {
          "description": "A PartiQL predicate.",
          "type": "string"
        },
        "all_rows_wildcard": {
          "description": "An empty object representing a row wildcard.",
          "type": "object",
          "additionalProperties": false
        }
      },
      "additionalProperties": false
    },
    "column_names": {
      "description": "A list of column names.",
      "type": "array",
      "insertionOrder": false,
      "items": {
        "$ref": "#/definitions/name_string"
      }
    },
    "column_wildcard": {
      "description": "An object representing the Data Cells Filter's Columns. Either Column Names or a Wildcard is required.",
      "type": "object",
      "properties": {
        "excluded_column_names": {
          "description": "A list of column names to be excluded from the Data Cells Filter.",
          "$ref": "#/definitions/column_names"
        }
      },
      "additionalProperties": false
    }
  },
  "properties": {
    "table_catalog_id": {
      "description": "The Catalog Id of the Table on which to create a Data Cells Filter.",
      "$ref": "#/definitions/catalog_id_string"
    },
    "database_name": {
      "description": "The name of the Database that the Table resides in.",
      "$ref": "#/definitions/name_string"
    },
    "table_name": {
      "description": "The name of the Table to create a Data Cells Filter for.",
      "$ref": "#/definitions/name_string"
    },
    "name": {
      "description": "The desired name of the Data Cells Filter.",
      "$ref": "#/definitions/name_string"
    },
    "row_filter": {
      "description": "An object representing the Data Cells Filter's Row Filter. Either a Filter Expression or a Wildcard is required",
      "$ref": "#/definitions/row_filter"
    },
    "column_names": {
      "description": "A list of columns to be included in this Data Cells Filter.",
      "$ref": "#/definitions/column_names"
    },
    "column_wildcard": {
      "description": "An object representing the Data Cells Filter's Columns. Either Column Names or a Wildcard is required",
      "$ref": "#/definitions/column_wildcard"
    }
  },
  "additionalProperties": false,
  "required": [
    "table_catalog_id",
    "database_name",
    "table_name",
    "name"
  ],
  "createOnlyProperties": [
    "/properties/table_catalog_id",
    "/properties/database_name",
    "/properties/table_name",
    "/properties/name",
    "/properties/row_filter",
    "/properties/column_names",
    "/properties/column_wildcard"
  ],
  "replacementStrategy": "delete_then_create",
  "tagging": {
    "taggable": false
  },
  "primaryIdentifier": [
    "/properties/table_catalog_id",
    "/properties/database_name",
    "/properties/table_name",
    "/properties/name"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "lakeformation:CreateDataCellsFilter",
        "glue:GetTable"
      ]
    },
    "delete": {
      "permissions": [
        "lakeformation:DeleteDataCellsFilter"
      ]
    },
    "read": {
      "permissions": [
        "lakeformation:ListDataCellsFilter"
      ]
    },
    "list": {
      "permissions": [
        "lakeformation:ListDataCellsFilter"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": false
}