{
  "typeName": "AWS::WAFv2::RuleGroup",
  "description": "Contains the Rules that identify the requests that you want to allow, block, or count. In a RuleGroup, you also specify a default action (ALLOW or BLOCK), and the action for each Rule that you add to a RuleGroup, for example, block requests from specified IP addresses or block requests from specified referrers. You also associate the RuleGroup with a CloudFront distribution to identify the requests that you want AWS WAF to filter. If you add more than one Rule to a RuleGroup, a request needs to match only one of the specifications to be allowed, blocked, or counted.",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-wafv2.git",
  "definitions": {
    "and_statement": {
      "type": "object",
      "properties": {
        "statements": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/statement"
          }
        }
      },
      "required": [
        "statements"
      ],
      "additionalProperties": false
    },
    "byte_match_statement": {
      "description": "Byte Match statement.",
      "type": "object",
      "properties": {
        "search_string": {
          "$ref": "#/definitions/search_string"
        },
        "search_string_base64": {
          "$ref": "#/definitions/search_string_base64"
        },
        "field_to_match": {
          "$ref": "#/definitions/field_to_match"
        },
        "text_transformations": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/text_transformation"
          }
        },
        "positional_constraint": {
          "$ref": "#/definitions/positional_constraint"
        }
      },
      "required": [
        "field_to_match",
        "positional_constraint",
        "text_transformations"
      ],
      "additionalProperties": false
    },
    "entity_description": {
      "description": "Description of the entity.",
      "type": "string",
      "pattern": "^[a-zA-Z0-9=:#@/\\-,.][a-zA-Z0-9+=:#@/\\-,.\\s]+[a-zA-Z0-9+=:#@/\\-,.]{1,256}$"
    },
    "entity_name": {
      "description": "Name of the RuleGroup.",
      "type": "string",
      "pattern": "^[0-9A-Za-z_-]{1,128}$"
    },
    "field_to_match": {
      "description": "Field of the request to match.",
      "type": "object",
      "properties": {
        "single_header": {
          "type": "object",
          "properties": {
            "name": {
              "type": "string"
            }
          },
          "required": [
            "name"
          ],
          "additionalProperties": false
        },
        "single_query_argument": {
          "description": "One query argument in a web request, identified by name, for example UserName or SalesRegion. The name can be up to 30 characters long and isn't case sensitive.",
          "type": "object",
          "properties": {
            "name": {
              "type": "string"
            }
          },
          "required": [
            "name"
          ],
          "additionalProperties": false
        },
        "all_query_arguments": {
          "description": "All query arguments of a web request.",
          "type": "object"
        },
        "uri_path": {
          "description": "The path component of the URI of a web request. This is the part of a web request that identifies a resource, for example, /images/daily-ad.jpg.",
          "type": "object"
        },
        "query_string": {
          "description": "The query string of a web request. This is the part of a URL that appears after a ? character, if any.",
          "type": "object"
        },
        "body": {
          "$ref": "#/definitions/body"
        },
        "method": {
          "description": "The HTTP method of a web request. The method indicates the type of operation that the request is asking the origin to perform.",
          "type": "object"
        },
        "json_body": {
          "$ref": "#/definitions/json_body"
        },
        "headers": {
          "$ref": "#/definitions/headers"
        },
        "cookies": {
          "$ref": "#/definitions/cookies"
        }
      },
      "additionalProperties": false
    },
    "json_body": {
      "description": "Inspect the request body as JSON. The request body immediately follows the request headers.",
      "type": "object",
      "properties": {
        "match_pattern": {
          "$ref": "#/definitions/json_match_pattern"
        },
        "match_scope": {
          "$ref": "#/definitions/json_match_scope"
        },
        "invalid_fallback_behavior": {
          "$ref": "#/definitions/body_parsing_fallback_behavior"
        },
        "oversize_handling": {
          "$ref": "#/definitions/oversize_handling"
        }
      },
      "required": [
        "match_pattern",
        "match_scope"
      ],
      "additionalProperties": false
    },
    "body_parsing_fallback_behavior": {
      "description": "The inspection behavior to fall back to if the JSON in the request body is invalid.",
      "type": "string",
      "enum": [
        "MATCH",
        "NO_MATCH",
        "EVALUATE_AS_STRING"
      ]
    },
    "json_match_scope": {
      "description": "The parts of the JSON to match against using the MatchPattern.",
      "type": "string",
      "enum": [
        "ALL",
        "KEY",
        "VALUE"
      ]
    },
    "json_match_pattern": {
      "description": "The pattern to look for in the JSON body.",
      "type": "object",
      "properties": {
        "all": {
          "description": "Inspect all parts of the web request's JSON body.",
          "type": "object"
        },
        "included_paths": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/json_pointer_path"
          }
        }
      },
      "additionalProperties": false
    },
    "json_pointer_path": {
      "description": "JSON pointer path in the web request's JSON body",
      "type": "string",
      "pattern": "^[\\/]+([^~]*(~[01])*)*{1,512}$"
    },
    "geo_match_statement": {
      "type": "object",
      "properties": {
        "country_codes": {
          "type": "array",
          "items": {
            "type": "string",
            "minLength": 1,
            "maxLength": 2
          }
        },
        "forwarded_ip_config": {
          "$ref": "#/definitions/forwarded_ip_configuration"
        }
      },
      "additionalProperties": false
    },
    "entity_id": {
      "description": "Id of the RuleGroup",
      "type": "string",
      "pattern": "^[0-9a-f]{8}-(?:[0-9a-f]{4}-){3}[0-9a-f]{12}$"
    },
    "ip_set_reference_statement": {
      "type": "object",
      "properties": {
        "arn": {
          "$ref": "#/definitions/resource_arn"
        },
        "ip_set_forwarded_ip_config": {
          "$ref": "#/definitions/ip_set_forwarded_ip_configuration"
        }
      },
      "required": [
        "arn"
      ],
      "additionalProperties": false
    },
    "not_statement": {
      "type": "object",
      "properties": {
        "statement": {
          "$ref": "#/definitions/statement"
        }
      },
      "required": [
        "statement"
      ],
      "additionalProperties": false
    },
    "or_statement": {
      "type": "object",
      "properties": {
        "statements": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/statement"
          }
        }
      },
      "required": [
        "statements"
      ],
      "additionalProperties": false
    },
    "positional_constraint": {
      "description": "Position of the evaluation in the FieldToMatch of request.",
      "type": "string",
      "enum": [
        "EXACTLY",
        "STARTS_WITH",
        "ENDS_WITH",
        "CONTAINS",
        "CONTAINS_WORD"
      ]
    },
    "rate_based_statement": {
      "type": "object",
      "properties": {
        "limit": {
          "$ref": "#/definitions/rate_limit"
        },
        "aggregate_key_type": {
          "type": "string",
          "enum": [
            "IP",
            "FORWARDED_IP"
          ]
        },
        "scope_down_statement": {
          "$ref": "#/definitions/statement"
        },
        "forwarded_ip_config": {
          "$ref": "#/definitions/forwarded_ip_configuration"
        }
      },
      "required": [
        "limit",
        "aggregate_key_type"
      ],
      "additionalProperties": false
    },
    "rate_limit": {
      "type": "integer",
      "minimum": 100,
      "maximum": 2000000000
    },
    "regex_pattern_set_reference_statement": {
      "type": "object",
      "properties": {
        "arn": {
          "$ref": "#/definitions/resource_arn"
        },
        "field_to_match": {
          "$ref": "#/definitions/field_to_match"
        },
        "text_transformations": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/text_transformation"
          }
        }
      },
      "required": [
        "arn",
        "field_to_match",
        "text_transformations"
      ],
      "additionalProperties": false
    },
    "resource_arn": {
      "description": "ARN of the WAF entity.",
      "type": "string",
      "minLength": 20,
      "maxLength": 2048
    },
    "forwarded_ip_configuration": {
      "type": "object",
      "properties": {
        "header_name": {
          "type": "string",
          "pattern": "^[a-zA-Z0-9-]+{1,255}$"
        },
        "fallback_behavior": {
          "type": "string",
          "enum": [
            "MATCH",
            "NO_MATCH"
          ]
        }
      },
      "required": [
        "header_name",
        "fallback_behavior"
      ],
      "additionalProperties": false
    },
    "ip_set_forwarded_ip_configuration": {
      "type": "object",
      "properties": {
        "header_name": {
          "type": "string",
          "pattern": "^[a-zA-Z0-9-]+{1,255}$"
        },
        "fallback_behavior": {
          "type": "string",
          "enum": [
            "MATCH",
            "NO_MATCH"
          ]
        },
        "position": {
          "type": "string",
          "enum": [
            "FIRST",
            "LAST",
            "ANY"
          ]
        }
      },
      "required": [
        "header_name",
        "fallback_behavior",
        "position"
      ],
      "additionalProperties": false
    },
    "rule": {
      "description": "Rule of RuleGroup that contains condition and action.",
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/entity_name"
        },
        "priority": {
          "$ref": "#/definitions/rule_priority"
        },
        "statement": {
          "$ref": "#/definitions/statement"
        },
        "action": {
          "$ref": "#/definitions/rule_action"
        },
        "rule_labels": {
          "description": "Collection of Rule Labels.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/label"
          }
        },
        "visibility_config": {
          "$ref": "#/definitions/visibility_config"
        },
        "captcha_config": {
          "$ref": "#/definitions/captcha_config"
        }
      },
      "required": [
        "name",
        "priority",
        "statement",
        "visibility_config"
      ],
      "additionalProperties": false
    },
    "rule_action": {
      "description": "Action taken when Rule matches its condition.",
      "type": "object",
      "properties": {
        "allow": {
          "description": "Allow traffic towards application.",
          "type": "object",
          "properties": {
            "custom_request_handling": {
              "$ref": "#/definitions/custom_request_handling"
            }
          },
          "additionalProperties": false
        },
        "block": {
          "description": "Block traffic towards application.",
          "type": "object",
          "properties": {
            "custom_response": {
              "$ref": "#/definitions/custom_response"
            }
          },
          "additionalProperties": false
        },
        "count": {
          "description": "Count traffic towards application.",
          "type": "object",
          "properties": {
            "custom_request_handling": {
              "$ref": "#/definitions/custom_request_handling"
            }
          },
          "additionalProperties": false
        },
        "captcha": {
          "description": "Checks valid token exists with request.",
          "type": "object",
          "properties": {
            "custom_request_handling": {
              "$ref": "#/definitions/custom_request_handling"
            }
          },
          "additionalProperties": false
        }
      },
      "additionalProperties": false
    },
    "custom_http_header_name": {
      "description": "HTTP header name.",
      "type": "string",
      "minLength": 1,
      "maxLength": 64
    },
    "custom_http_header_value": {
      "description": "HTTP header value.",
      "type": "string",
      "minLength": 1,
      "maxLength": 255
    },
    "custom_http_header": {
      "description": "HTTP header.",
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/custom_http_header_name"
        },
        "value": {
          "$ref": "#/definitions/custom_http_header_value"
        }
      },
      "required": [
        "name",
        "value"
      ],
      "additionalProperties": false
    },
    "custom_request_handling": {
      "description": "Custom request handling.",
      "type": "object",
      "properties": {
        "insert_headers": {
          "description": "Collection of HTTP headers.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/custom_http_header"
          },
          "minItems": 1
        }
      },
      "required": [
        "insert_headers"
      ],
      "additionalProperties": false
    },
    "response_status_code": {
      "description": "Custom response code.",
      "type": "integer",
      "minimum": 200,
      "maximum": 599
    },
    "response_content_type": {
      "description": "Valid values are TEXT_PLAIN, TEXT_HTML, and APPLICATION_JSON.",
      "type": "string",
      "enum": [
        "TEXT_PLAIN",
        "TEXT_HTML",
        "APPLICATION_JSON"
      ]
    },
    "response_content": {
      "description": "Response content.",
      "type": "string",
      "minLength": 1,
      "maxLength": 10240
    },
    "custom_response_body": {
      "description": "Custom response body.",
      "type": "object",
      "properties": {
        "content_type": {
          "$ref": "#/definitions/response_content_type"
        },
        "content": {
          "$ref": "#/definitions/response_content"
        }
      },
      "required": [
        "content_type",
        "content"
      ],
      "additionalProperties": false
    },
    "custom_response": {
      "description": "Custom response.",
      "type": "object",
      "properties": {
        "response_code": {
          "$ref": "#/definitions/response_status_code"
        },
        "custom_response_body_key": {
          "description": "Custom response body key.",
          "type": "string",
          "pattern": "^[\\w\\-]+$"
        },
        "response_headers": {
          "description": "Collection of HTTP headers.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/custom_http_header"
          },
          "minItems": 1
        }
      },
      "required": [
        "response_code"
      ],
      "additionalProperties": false
    },
    "custom_response_bodies": {
      "description": "Custom response key and body map.",
      "type": "object",
      "patternProperties": {
        "^[\\w\\-]+$": {
          "$ref": "#/definitions/custom_response_body"
        }
      },
      "additionalProperties": false,
      "minProperties": 1
    },
    "rule_group": {
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/entity_name"
        },
        "id": {
          "$ref": "#/definitions/entity_id"
        },
        "arn": {
          "$ref": "#/definitions/resource_arn"
        },
        "description": {
          "$ref": "#/definitions/entity_description"
        },
        "rules": {
          "description": "Collection of Rules.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/rule"
          }
        },
        "visibility_config": {
          "$ref": "#/definitions/visibility_config"
        },
        "capacity": {
          "type": "integer",
          "minimum": 0
        }
      },
      "additionalProperties": false
    },
    "rule_priority": {
      "description": "Priority of the Rule, Rules get evaluated from lower to higher priority.",
      "type": "integer",
      "minimum": 0
    },
    "scope": {
      "description": "Use CLOUDFRONT for CloudFront RuleGroup, use REGIONAL for Application Load Balancer and API Gateway.",
      "type": "string",
      "enum": [
        "CLOUDFRONT",
        "REGIONAL"
      ]
    },
    "search_string": {
      "description": "String that is searched to find a match.",
      "type": "string"
    },
    "search_string_base64": {
      "description": "Base64 encoded string that is searched to find a match.",
      "type": "string"
    },
    "size_constraint_statement": {
      "description": "Size Constraint statement.",
      "type": "object",
      "properties": {
        "field_to_match": {
          "$ref": "#/definitions/field_to_match"
        },
        "comparison_operator": {
          "type": "string",
          "enum": [
            "EQ",
            "NE",
            "LE",
            "LT",
            "GE",
            "GT"
          ]
        },
        "size": {
          "type": "number",
          "minimum": 0,
          "maximum": 21474836480
        },
        "text_transformations": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/text_transformation"
          }
        }
      },
      "required": [
        "field_to_match",
        "comparison_operator",
        "size",
        "text_transformations"
      ],
      "additionalProperties": false
    },
    "sqli_match_statement": {
      "description": "Sqli Match Statement.",
      "type": "object",
      "properties": {
        "field_to_match": {
          "$ref": "#/definitions/field_to_match"
        },
        "text_transformations": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/text_transformation"
          }
        },
        "sensitivity_level": {
          "$ref": "#/definitions/sensitivity_level"
        }
      },
      "required": [
        "field_to_match",
        "text_transformations"
      ],
      "additionalProperties": false
    },
    "statement": {
      "description": "First level statement that contains conditions, such as ByteMatch, SizeConstraint, etc",
      "type": "object",
      "properties": {
        "byte_match_statement": {
          "$ref": "#/definitions/byte_match_statement"
        },
        "sqli_match_statement": {
          "$ref": "#/definitions/sqli_match_statement"
        },
        "xss_match_statement": {
          "$ref": "#/definitions/xss_match_statement"
        },
        "size_constraint_statement": {
          "$ref": "#/definitions/size_constraint_statement"
        },
        "geo_match_statement": {
          "$ref": "#/definitions/geo_match_statement"
        },
        "ip_set_reference_statement": {
          "$ref": "#/definitions/ip_set_reference_statement"
        },
        "regex_pattern_set_reference_statement": {
          "$ref": "#/definitions/regex_pattern_set_reference_statement"
        },
        "rate_based_statement": {
          "$ref": "#/definitions/rate_based_statement"
        },
        "and_statement": {
          "$ref": "#/definitions/and_statement"
        },
        "or_statement": {
          "$ref": "#/definitions/or_statement"
        },
        "not_statement": {
          "$ref": "#/definitions/not_statement"
        },
        "label_match_statement": {
          "$ref": "#/definitions/label_match_statement"
        },
        "regex_match_statement": {
          "$ref": "#/definitions/regex_match_statement"
        }
      },
      "additionalProperties": false
    },
    "tag": {
      "type": "object",
      "properties": {
        "key": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "value": {
          "type": "string",
          "minLength": 0,
          "maxLength": 256
        }
      },
      "additionalProperties": false
    },
    "text_transformation": {
      "description": "Text Transformation on the Search String before match.",
      "type": "object",
      "properties": {
        "priority": {
          "$ref": "#/definitions/text_transformation_priority"
        },
        "type": {
          "$ref": "#/definitions/text_transformation_type"
        }
      },
      "required": [
        "priority",
        "type"
      ],
      "additionalProperties": false
    },
    "text_transformation_priority": {
      "description": "Priority of Rule being evaluated.",
      "type": "integer",
      "minimum": 0
    },
    "text_transformation_type": {
      "description": "Type of text transformation.",
      "type": "string",
      "enum": [
        "NONE",
        "COMPRESS_WHITE_SPACE",
        "HTML_ENTITY_DECODE",
        "LOWERCASE",
        "CMD_LINE",
        "URL_DECODE",
        "BASE64_DECODE",
        "HEX_DECODE",
        "MD5",
        "REPLACE_COMMENTS",
        "ESCAPE_SEQ_DECODE",
        "SQL_HEX_DECODE",
        "CSS_DECODE",
        "JS_DECODE",
        "NORMALIZE_PATH",
        "NORMALIZE_PATH_WIN",
        "REMOVE_NULLS",
        "REPLACE_NULLS",
        "BASE64_DECODE_EXT",
        "URL_DECODE_UNI",
        "UTF8_TO_UNICODE"
      ]
    },
    "visibility_config": {
      "description": "Visibility Metric of the RuleGroup.",
      "type": "object",
      "properties": {
        "sampled_requests_enabled": {
          "type": "boolean"
        },
        "cloud_watch_metrics_enabled": {
          "type": "boolean"
        },
        "metric_name": {
          "type": "string",
          "maxLength": 128,
          "minLength": 1
        }
      },
      "required": [
        "sampled_requests_enabled",
        "cloud_watch_metrics_enabled",
        "metric_name"
      ],
      "additionalProperties": false
    },
    "xss_match_statement": {
      "description": "Xss Match Statement.",
      "type": "object",
      "properties": {
        "field_to_match": {
          "$ref": "#/definitions/field_to_match"
        },
        "text_transformations": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/text_transformation"
          }
        }
      },
      "required": [
        "field_to_match",
        "text_transformations"
      ],
      "additionalProperties": false
    },
    "label_name": {
      "description": "Name of the Label.",
      "type": "string",
      "pattern": "^[0-9A-Za-z_:-]{1,1024}$"
    },
    "label_summary": {
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/label_name"
        }
      },
      "additionalProperties": false
    },
    "label": {
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/label_name"
        }
      },
      "required": [
        "name"
      ],
      "additionalProperties": false
    },
    "label_match_key": {
      "type": "string",
      "pattern": "^[0-9A-Za-z_:-]{1,1024}$"
    },
    "label_match_scope": {
      "type": "string",
      "enum": [
        "LABEL",
        "NAMESPACE"
      ]
    },
    "label_match_statement": {
      "type": "object",
      "properties": {
        "scope": {
          "$ref": "#/definitions/label_match_scope"
        },
        "key": {
          "$ref": "#/definitions/label_match_key"
        }
      },
      "required": [
        "scope",
        "key"
      ],
      "additionalProperties": false
    },
    "regex_match_statement": {
      "type": "object",
      "properties": {
        "regex_string": {
          "type": "string",
          "maxLength": 512,
          "minLength": 1
        },
        "field_to_match": {
          "$ref": "#/definitions/field_to_match"
        },
        "text_transformations": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/text_transformation"
          }
        }
      },
      "required": [
        "regex_string",
        "field_to_match",
        "text_transformations"
      ],
      "additionalProperties": false
    },
    "captcha_config": {
      "type": "object",
      "properties": {
        "immunity_time_property": {
          "$ref": "#/definitions/immunity_time_property"
        }
      },
      "additionalProperties": false
    },
    "immunity_time_property": {
      "type": "object",
      "properties": {
        "immunity_time": {
          "type": "integer",
          "minimum": 60,
          "maximum": 259200
        }
      },
      "required": [
        "immunity_time"
      ],
      "additionalProperties": false
    },
    "body": {
      "description": "The body of a web request. This immediately follows the request headers.",
      "type": "object",
      "properties": {
        "oversize_handling": {
          "$ref": "#/definitions/oversize_handling"
        }
      },
      "additionalProperties": false
    },
    "headers": {
      "description": "Includes headers of a web request.",
      "type": "object",
      "properties": {
        "match_pattern": {
          "$ref": "#/definitions/header_match_pattern"
        },
        "match_scope": {
          "$ref": "#/definitions/map_match_scope"
        },
        "oversize_handling": {
          "$ref": "#/definitions/oversize_handling"
        }
      },
      "required": [
        "match_pattern",
        "match_scope",
        "oversize_handling"
      ],
      "additionalProperties": false
    },
    "cookies": {
      "description": "Includes headers of a web request.",
      "type": "object",
      "properties": {
        "match_pattern": {
          "$ref": "#/definitions/cookie_match_pattern"
        },
        "match_scope": {
          "$ref": "#/definitions/map_match_scope"
        },
        "oversize_handling": {
          "$ref": "#/definitions/oversize_handling"
        }
      },
      "required": [
        "match_pattern",
        "match_scope",
        "oversize_handling"
      ],
      "additionalProperties": false
    },
    "header_match_pattern": {
      "description": "The pattern to look for in the request headers.",
      "type": "object",
      "properties": {
        "all": {
          "description": "Inspect all parts of the web request headers.",
          "type": "object"
        },
        "included_headers": {
          "type": "array",
          "items": {
            "type": "string",
            "pattern": ".*\\S.*",
            "minLength": 1,
            "maxLength": 64
          },
          "minItems": 1,
          "maxItems": 199
        },
        "excluded_headers": {
          "type": "array",
          "items": {
            "type": "string",
            "pattern": ".*\\S.*",
            "minLength": 1,
            "maxLength": 64
          },
          "minItems": 1,
          "maxItems": 199
        }
      },
      "additionalProperties": false
    },
    "cookie_match_pattern": {
      "description": "The pattern to look for in the request cookies.",
      "type": "object",
      "properties": {
        "all": {
          "description": "Inspect all parts of the web request cookies.",
          "type": "object"
        },
        "included_cookies": {
          "type": "array",
          "items": {
            "type": "string",
            "pattern": ".*\\S.*",
            "minLength": 1,
            "maxLength": 60
          },
          "minItems": 1,
          "maxItems": 199
        },
        "excluded_cookies": {
          "type": "array",
          "items": {
            "type": "string",
            "pattern": ".*\\S.*",
            "minLength": 1,
            "maxLength": 60
          },
          "minItems": 1,
          "maxItems": 199
        }
      },
      "additionalProperties": false
    },
    "map_match_scope": {
      "description": "The parts of the request to match against using the MatchPattern.",
      "type": "string",
      "enum": [
        "ALL",
        "KEY",
        "VALUE"
      ]
    },
    "oversize_handling": {
      "description": "Handling of requests containing oversize fields",
      "type": "string",
      "enum": [
        "CONTINUE",
        "MATCH",
        "NO_MATCH"
      ]
    },
    "sensitivity_level": {
      "description": "Sensitivity Level current only used for sqli match statements.",
      "type": "string",
      "enum": [
        "LOW",
        "HIGH"
      ]
    }
  },
  "properties": {
    "arn": {
      "$ref": "#/definitions/resource_arn"
    },
    "capacity": {
      "type": "integer",
      "minimum": 0
    },
    "description": {
      "$ref": "#/definitions/entity_description"
    },
    "name": {
      "$ref": "#/definitions/entity_name"
    },
    "id": {
      "$ref": "#/definitions/entity_id"
    },
    "scope": {
      "$ref": "#/definitions/scope"
    },
    "rules": {
      "description": "Collection of Rules.",
      "type": "array",
      "items": {
        "$ref": "#/definitions/rule"
      }
    },
    "visibility_config": {
      "$ref": "#/definitions/visibility_config"
    },
    "tags": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/tag"
      },
      "minItems": 1
    },
    "label_namespace": {
      "$ref": "#/definitions/label_name"
    },
    "custom_response_bodies": {
      "$ref": "#/definitions/custom_response_bodies"
    },
    "available_labels": {
      "description": "Collection of Available Labels.",
      "type": "array",
      "items": {
        "$ref": "#/definitions/label_summary"
      }
    },
    "consumed_labels": {
      "description": "Collection of Consumed Labels.",
      "type": "array",
      "items": {
        "$ref": "#/definitions/label_summary"
      }
    }
  },
  "required": [
    "capacity",
    "scope",
    "visibility_config"
  ],
  "primaryIdentifier": [
    "/properties/name",
    "/properties/id",
    "/properties/scope"
  ],
  "createOnlyProperties": [
    "/properties/name",
    "/properties/scope"
  ],
  "readOnlyProperties": [
    "/properties/arn",
    "/properties/id",
    "/properties/label_namespace",
    "/properties/available_labels",
    "/properties/consumed_labels"
  ],
  "additionalProperties": false,
  "handlers": {
    "create": {
      "permissions": [
        "wafv2:CreateRuleGroup",
        "wafv2:GetRuleGroup",
        "wafv2:ListTagsForResource"
      ]
    },
    "delete": {
      "permissions": [
        "wafv2:DeleteRuleGroup",
        "wafv2:GetRuleGroup"
      ]
    },
    "read": {
      "permissions": [
        "wafv2:GetRuleGroup",
        "wafv2:ListTagsForResource"
      ]
    },
    "update": {
      "permissions": [
        "wafv2:UpdateRuleGroup",
        "wafv2:GetRuleGroup",
        "wafv2:ListTagsForResource"
      ]
    },
    "list": {
      "permissions": [
        "wafv2:listRuleGroups"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}