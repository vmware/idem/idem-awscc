{
  "typeName": "AWS::Timestream::ScheduledQuery",
  "description": "The AWS::Timestream::ScheduledQuery resource creates a Timestream Scheduled Query.",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-timestream.git",
  "definitions": {
    "arn": {
      "description": "Amazon Resource Name of the scheduled query that is generated upon creation.",
      "type": "string",
      "minLength": 1,
      "maxLength": 2048
    },
    "scheduled_query_name": {
      "description": "The name of the scheduled query. Scheduled query names must be unique within each Region.",
      "type": "string",
      "pattern": "[a-zA-Z0-9_.-]+",
      "minLength": 1,
      "maxLength": 64
    },
    "query_string": {
      "description": "The query string to run. Parameter names can be specified in the query string @ character followed by an identifier. The named Parameter @scheduled_runtime is reserved and can be used in the query to get the time at which the query is scheduled to run. The timestamp calculated according to the ScheduleConfiguration parameter, will be the value of @scheduled_runtime paramater for each query run. For example, consider an instance of a scheduled query executing on 2021-12-01 00:00:00. For this instance, the @scheduled_runtime parameter is initialized to the timestamp 2021-12-01 00:00:00 when invoking the query.",
      "type": "string",
      "minLength": 1,
      "maxLength": 262144
    },
    "schedule_configuration": {
      "description": "Configuration for when the scheduled query is executed.",
      "type": "object",
      "properties": {
        "schedule_expression": {
          "$ref": "#/definitions/schedule_expression"
        }
      },
      "required": [
        "schedule_expression"
      ],
      "additionalProperties": false
    },
    "notification_configuration": {
      "description": "Notification configuration for the scheduled query. A notification is sent by Timestream when a query run finishes, when the state is updated or when you delete it.",
      "type": "object",
      "properties": {
        "sns_configuration": {
          "$ref": "#/definitions/sns_configuration"
        }
      },
      "required": [
        "sns_configuration"
      ],
      "additionalProperties": false
    },
    "client_token": {
      "description": "Using a ClientToken makes the call to CreateScheduledQuery idempotent, in other words, making the same request repeatedly will produce the same result. Making multiple identical CreateScheduledQuery requests has the same effect as making a single request. If CreateScheduledQuery is called without a ClientToken, the Query SDK generates a ClientToken on your behalf. After 8 hours, any request with the same ClientToken is treated as a new request.",
      "type": "string",
      "minLength": 32,
      "maxLength": 128
    },
    "scheduled_query_execution_role_arn": {
      "description": "The ARN for the IAM role that Timestream will assume when running the scheduled query.",
      "type": "string",
      "minLength": 1,
      "maxLength": 2048
    },
    "target_configuration": {
      "description": "Configuration of target store where scheduled query results are written to.",
      "type": "object",
      "properties": {
        "timestream_configuration": {
          "$ref": "#/definitions/timestream_configuration"
        }
      },
      "required": [
        "timestream_configuration"
      ],
      "additionalProperties": false
    },
    "error_report_configuration": {
      "description": "Configuration for error reporting. Error reports will be generated when a problem is encountered when writing the query results.",
      "type": "object",
      "properties": {
        "s3_configuration": {
          "$ref": "#/definitions/s3_configuration"
        }
      },
      "required": [
        "s3_configuration"
      ],
      "additionalProperties": false
    },
    "kms_key_id": {
      "description": "The Amazon KMS key used to encrypt the scheduled query resource, at-rest. If the Amazon KMS key is not specified, the scheduled query resource will be encrypted with a Timestream owned Amazon KMS key. To specify a KMS key, use the key ID, key ARN, alias name, or alias ARN. When using an alias name, prefix the name with alias/. If ErrorReportConfiguration uses SSE_KMS as encryption type, the same KmsKeyId is used to encrypt the error report at rest.",
      "type": "string",
      "minLength": 1,
      "maxLength": 2048
    },
    "tags": {
      "description": "A list of key-value pairs to label the scheduled query.",
      "type": "array",
      "insertionOrder": false,
      "maxItems": 200,
      "items": {
        "$ref": "#/definitions/tag"
      }
    },
    "schedule_expression": {
      "description": "An expression that denotes when to trigger the scheduled query run. This can be a cron expression or a rate expression.",
      "type": "string",
      "minLength": 1,
      "maxLength": 256
    },
    "sns_configuration": {
      "description": "SNS configuration for notification upon scheduled query execution.",
      "type": "object",
      "properties": {
        "topic_arn": {
          "$ref": "#/definitions/topic_arn"
        }
      },
      "required": [
        "topic_arn"
      ],
      "additionalProperties": false
    },
    "topic_arn": {
      "description": "SNS topic ARN that the scheduled query status notifications will be sent to.",
      "type": "string",
      "minLength": 1,
      "maxLength": 2048
    },
    "timestream_configuration": {
      "description": "Configuration needed to write data into the Timestream database and table.",
      "type": "object",
      "properties": {
        "database_name": {
          "$ref": "#/definitions/database_name"
        },
        "table_name": {
          "$ref": "#/definitions/table_name"
        },
        "time_column": {
          "$ref": "#/definitions/time_column"
        },
        "dimension_mappings": {
          "$ref": "#/definitions/dimension_mappings"
        },
        "multi_measure_mappings": {
          "$ref": "#/definitions/multi_measure_mappings"
        },
        "mixed_measure_mappings": {
          "$ref": "#/definitions/mixed_measure_mappings"
        },
        "measure_name_column": {
          "$ref": "#/definitions/measure_name_column"
        }
      },
      "required": [
        "database_name",
        "table_name",
        "time_column",
        "dimension_mappings"
      ],
      "additionalProperties": false
    },
    "database_name": {
      "description": "Name of Timestream database to which the query result will be written.",
      "type": "string"
    },
    "table_name": {
      "description": "Name of Timestream table that the query result will be written to. The table should be within the same database that is provided in Timestream configuration.",
      "type": "string"
    },
    "time_column": {
      "description": "Column from query result that should be used as the time column in destination table. Column type for this should be TIMESTAMP.",
      "type": "string"
    },
    "dimension_mappings": {
      "description": "This is to allow mapping column(s) from the query result to the dimension in the destination table.",
      "type": "array",
      "insertionOrder": false,
      "items": {
        "$ref": "#/definitions/dimension_mapping"
      }
    },
    "dimension_mapping": {
      "description": "This type is used to map column(s) from the query result to a dimension in the destination table.",
      "type": "object",
      "properties": {
        "name": {
          "$ref": "#/definitions/dimension_mapping_name"
        },
        "dimension_value_type": {
          "$ref": "#/definitions/dimension_value_type"
        }
      },
      "required": [
        "name",
        "dimension_value_type"
      ],
      "additionalProperties": false
    },
    "dimension_mapping_name": {
      "description": "Column name from query result.",
      "type": "string"
    },
    "dimension_value_type": {
      "description": "Type for the dimension.",
      "type": "string",
      "enum": [
        "VARCHAR"
      ]
    },
    "multi_measure_mappings": {
      "description": "Only one of MixedMeasureMappings or MultiMeasureMappings is to be provided. MultiMeasureMappings can be used to ingest data as multi measures in the derived table.",
      "type": "object",
      "properties": {
        "target_multi_measure_name": {
          "$ref": "#/definitions/target_multi_measure_name"
        },
        "multi_measure_attribute_mappings": {
          "$ref": "#/definitions/multi_measure_attribute_mapping_list"
        }
      },
      "required": [
        "multi_measure_attribute_mappings"
      ],
      "additionalProperties": false
    },
    "target_multi_measure_name": {
      "description": "Name of the target multi-measure in the derived table. Required if MeasureNameColumn is not provided. If MeasureNameColumn is provided then the value from that column will be used as the multi-measure name.",
      "type": "string"
    },
    "multi_measure_attribute_mapping_list": {
      "description": "Required. Attribute mappings to be used for mapping query results to ingest data for multi-measure attributes.",
      "type": "array",
      "insertionOrder": false,
      "minItems": 1,
      "items": {
        "$ref": "#/definitions/multi_measure_attribute_mapping"
      }
    },
    "multi_measure_attribute_mapping": {
      "description": "An attribute mapping to be used for mapping query results to ingest data for multi-measure attributes.",
      "type": "object",
      "properties": {
        "source_column": {
          "$ref": "#/definitions/multi_measure_attribute_mapping_source_column"
        },
        "measure_value_type": {
          "$ref": "#/definitions/multi_measure_attribute_mapping_measure_value_type"
        },
        "target_multi_measure_attribute_name": {
          "$ref": "#/definitions/target_multi_measure_attribute_name"
        }
      },
      "required": [
        "source_column",
        "measure_value_type"
      ],
      "additionalProperties": false
    },
    "multi_measure_attribute_mapping_source_column": {
      "description": "Source measure value column in the query result where the attribute value is to be read.",
      "type": "string"
    },
    "multi_measure_attribute_mapping_measure_value_type": {
      "description": "Value type of the measure value column to be read from the query result.",
      "type": "string",
      "enum": [
        "BIGINT",
        "BOOLEAN",
        "DOUBLE",
        "VARCHAR"
      ]
    },
    "target_multi_measure_attribute_name": {
      "description": "Custom name to be used for attribute name in derived table. If not provided, source column name would be used.",
      "type": "string"
    },
    "mixed_measure_mappings": {
      "description": "Specifies how to map measures to multi-measure records.",
      "type": "array",
      "insertionOrder": false,
      "minItems": 1,
      "items": {
        "$ref": "#/definitions/mixed_measure_mapping"
      }
    },
    "mixed_measure_mapping": {
      "description": "MixedMeasureMappings are mappings that can be used to ingest data into a mixture of narrow and multi measures in the derived table.",
      "type": "object",
      "properties": {
        "measure_name": {
          "$ref": "#/definitions/mixed_measure_mapping_measure_name"
        },
        "source_column": {
          "$ref": "#/definitions/mixed_measure_mapping_source_column"
        },
        "target_measure_name": {
          "$ref": "#/definitions/mixed_measure_mapping_target_measure_name"
        },
        "measure_value_type": {
          "$ref": "#/definitions/mixed_measure_mapping_measure_value_type"
        },
        "multi_measure_attribute_mappings": {
          "$ref": "#/definitions/multi_measure_attribute_mapping_list"
        }
      },
      "required": [
        "measure_value_type"
      ],
      "additionalProperties": false
    },
    "mixed_measure_mapping_measure_name": {
      "description": "Refers to the value of the measure name in a result row. This field is required if MeasureNameColumn is provided.",
      "type": "string"
    },
    "mixed_measure_mapping_source_column": {
      "description": "This field refers to the source column from which the measure value is to be read for result materialization.",
      "type": "string"
    },
    "mixed_measure_mapping_target_measure_name": {
      "description": "Target measure name to be used. If not provided, the target measure name by default would be MeasureName if provided, or SourceColumn otherwise.",
      "type": "string"
    },
    "mixed_measure_mapping_measure_value_type": {
      "description": "Type of the value that is to be read from SourceColumn. If the mapping is for MULTI, use MeasureValueType.MULTI.",
      "type": "string",
      "enum": [
        "BIGINT",
        "BOOLEAN",
        "DOUBLE",
        "VARCHAR",
        "MULTI"
      ]
    },
    "measure_name_column": {
      "description": "Name of the measure name column from the query result.",
      "type": "string"
    },
    "s3_configuration": {
      "description": "Details on S3 location for error reports that result from running a query.",
      "type": "object",
      "properties": {
        "bucket_name": {
          "$ref": "#/definitions/bucket_name"
        },
        "object_key_prefix": {
          "$ref": "#/definitions/object_key_prefix"
        },
        "encryption_option": {
          "$ref": "#/definitions/encryption_option"
        }
      },
      "required": [
        "bucket_name"
      ],
      "additionalProperties": false
    },
    "bucket_name": {
      "description": "Name of the S3 bucket under which error reports will be created.",
      "type": "string",
      "minLength": 3,
      "maxLength": 63,
      "pattern": "[a-z0-9][\\.\\-a-z0-9]{1,61}[a-z0-9]"
    },
    "object_key_prefix": {
      "description": "Prefix for error report keys.",
      "type": "string",
      "minLength": 1,
      "maxLength": 896,
      "pattern": "[a-zA-Z0-9|!\\-_*'\\(\\)]([a-zA-Z0-9]|[!\\-_*'\\(\\)\\/.])+"
    },
    "encryption_option": {
      "description": "Encryption at rest options for the error reports. If no encryption option is specified, Timestream will choose SSE_S3 as default.",
      "type": "string",
      "enum": [
        "SSE_S3",
        "SSE_KMS"
      ]
    },
    "tag": {
      "description": "A key-value pair to label the scheduled query.",
      "type": "object",
      "properties": {
        "key": {
          "$ref": "#/definitions/key"
        },
        "value": {
          "$ref": "#/definitions/value"
        }
      },
      "required": [
        "key",
        "value"
      ],
      "additionalProperties": false
    },
    "key": {
      "type": "string",
      "description": "The key name of the tag. You can specify a value that is 1 to 128 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -.",
      "minLength": 1,
      "maxLength": 128
    },
    "value": {
      "type": "string",
      "description": "The value for the tag. You can specify a value that is 0 to 256 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -.",
      "minLength": 0,
      "maxLength": 256
    }
  },
  "properties": {
    "arn": {
      "$ref": "#/definitions/arn"
    },
    "scheduled_query_name": {
      "$ref": "#/definitions/scheduled_query_name"
    },
    "query_string": {
      "$ref": "#/definitions/query_string"
    },
    "schedule_configuration": {
      "$ref": "#/definitions/schedule_configuration"
    },
    "notification_configuration": {
      "$ref": "#/definitions/notification_configuration"
    },
    "client_token": {
      "$ref": "#/definitions/client_token"
    },
    "scheduled_query_execution_role_arn": {
      "$ref": "#/definitions/scheduled_query_execution_role_arn"
    },
    "target_configuration": {
      "$ref": "#/definitions/target_configuration"
    },
    "error_report_configuration": {
      "$ref": "#/definitions/error_report_configuration"
    },
    "kms_key_id": {
      "$ref": "#/definitions/kms_key_id"
    },
    "sq_name": {
      "description": "The name of the scheduled query. Scheduled query names must be unique within each Region.",
      "type": "string"
    },
    "sq_query_string": {
      "description": "The query string to run. Parameter names can be specified in the query string @ character followed by an identifier. The named Parameter @scheduled_runtime is reserved and can be used in the query to get the time at which the query is scheduled to run. The timestamp calculated according to the ScheduleConfiguration parameter, will be the value of @scheduled_runtime paramater for each query run. For example, consider an instance of a scheduled query executing on 2021-12-01 00:00:00. For this instance, the @scheduled_runtime parameter is initialized to the timestamp 2021-12-01 00:00:00 when invoking the query.",
      "type": "string"
    },
    "sq_schedule_configuration": {
      "description": "Configuration for when the scheduled query is executed.",
      "type": "string"
    },
    "sq_notification_configuration": {
      "description": "Notification configuration for the scheduled query. A notification is sent by Timestream when a query run finishes, when the state is updated or when you delete it.",
      "type": "string"
    },
    "sq_scheduled_query_execution_role_arn": {
      "description": "The ARN for the IAM role that Timestream will assume when running the scheduled query.",
      "type": "string"
    },
    "sq_target_configuration": {
      "description": "Configuration of target store where scheduled query results are written to.",
      "type": "string"
    },
    "sq_error_report_configuration": {
      "description": "Configuration for error reporting. Error reports will be generated when a problem is encountered when writing the query results.",
      "type": "string"
    },
    "sq_kms_key_id": {
      "description": "The Amazon KMS key used to encrypt the scheduled query resource, at-rest. If the Amazon KMS key is not specified, the scheduled query resource will be encrypted with a Timestream owned Amazon KMS key. To specify a KMS key, use the key ID, key ARN, alias name, or alias ARN. When using an alias name, prefix the name with alias/. If ErrorReportConfiguration uses SSE_KMS as encryption type, the same KmsKeyId is used to encrypt the error report at rest.",
      "type": "string"
    },
    "tags": {
      "$ref": "#/definitions/tags"
    }
  },
  "required": [
    "query_string",
    "schedule_configuration",
    "notification_configuration",
    "scheduled_query_execution_role_arn",
    "error_report_configuration"
  ],
  "additionalProperties": false,
  "primaryIdentifier": [
    "/properties/arn"
  ],
  "createOnlyProperties": [
    "/properties/scheduled_query_name",
    "/properties/query_string",
    "/properties/schedule_configuration",
    "/properties/notification_configuration",
    "/properties/client_token",
    "/properties/scheduled_query_execution_role_arn",
    "/properties/target_configuration",
    "/properties/error_report_configuration",
    "/properties/kms_key_id"
  ],
  "readOnlyProperties": [
    "/properties/arn",
    "/properties/sq_name",
    "/properties/sq_query_string",
    "/properties/sq_schedule_configuration",
    "/properties/sq_notification_configuration",
    "/properties/sq_scheduled_query_execution_role_arn",
    "/properties/sq_target_configuration",
    "/properties/sq_error_report_configuration",
    "/properties/sq_kms_key_id"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "timestream:CreateScheduledQuery",
        "timestream:DescribeEndpoints"
      ]
    },
    "read": {
      "permissions": [
        "timestream:DescribeScheduledQuery",
        "timestream:ListTagsForResource",
        "timestream:DescribeEndpoints"
      ]
    },
    "update": {
      "permissions": [
        "timestream:UpdateScheduledQuery",
        "timestream:TagResource",
        "timestream:UntagResource",
        "timestream:DescribeEndpoints"
      ]
    },
    "delete": {
      "permissions": [
        "timestream:DeleteScheduledQuery",
        "timestream:DescribeEndpoints"
      ]
    },
    "list": {
      "permissions": [
        "timestream:ListScheduledQueries",
        "timestream:DescribeEndpoints"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}