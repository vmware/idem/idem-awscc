{
  "typeName": "AWS::LakeFormation::TagAssociation",
  "description": "A resource schema representing a Lake Formation Tag Association. While tag associations are not explicit Lake Formation resources, this CloudFormation resource can be used to associate tags with Lake Formation entities.",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-rpdk.git",
  "definitions": {
    "catalog_id_string": {
      "type": "string",
      "minLength": 12,
      "maxLength": 12
    },
    "name_string": {
      "type": "string",
      "minLength": 1,
      "maxLength": 255
    },
    "lf_tag_pair": {
      "type": "object",
      "properties": {
        "catalog_id": {
          "$ref": "#/definitions/catalog_id_string"
        },
        "tag_key": {
          "$ref": "#/definitions/lf_tag_key"
        },
        "tag_values": {
          "$ref": "#/definitions/tag_value_list"
        }
      },
      "required": [
        "catalog_id",
        "tag_key",
        "tag_values"
      ],
      "additionalProperties": false
    },
    "lf_tags_list": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/lf_tag_pair"
      },
      "insertionOrder": false
    },
    "data_lake_principal_string": {
      "type": "string",
      "minLength": 1,
      "maxLength": 255
    },
    "data_lake_principal": {
      "type": "object",
      "properties": {
        "data_lake_principal_identifier": {
          "$ref": "#/definitions/data_lake_principal_string"
        }
      },
      "additionalProperties": false
    },
    "resource_type": {
      "type": "string",
      "enum": [
        "DATABASE",
        "TABLE"
      ]
    },
    "catalog_resource": {
      "type": "object",
      "additionalProperties": false
    },
    "database_resource": {
      "type": "object",
      "properties": {
        "catalog_id": {
          "$ref": "#/definitions/catalog_id_string"
        },
        "name": {
          "$ref": "#/definitions/name_string"
        }
      },
      "required": [
        "catalog_id",
        "name"
      ],
      "additionalProperties": false
    },
    "table_wildcard": {
      "type": "object",
      "additionalProperties": false
    },
    "table_resource": {
      "type": "object",
      "properties": {
        "catalog_id": {
          "$ref": "#/definitions/catalog_id_string"
        },
        "database_name": {
          "$ref": "#/definitions/name_string"
        },
        "name": {
          "$ref": "#/definitions/name_string"
        },
        "table_wildcard": {
          "$ref": "#/definitions/table_wildcard"
        }
      },
      "required": [
        "catalog_id",
        "database_name"
      ],
      "additionalProperties": false
    },
    "column_names": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/name_string"
      },
      "insertionOrder": false
    },
    "table_with_columns_resource": {
      "type": "object",
      "properties": {
        "catalog_id": {
          "$ref": "#/definitions/catalog_id_string"
        },
        "database_name": {
          "$ref": "#/definitions/name_string"
        },
        "name": {
          "$ref": "#/definitions/name_string"
        },
        "column_names": {
          "$ref": "#/definitions/column_names"
        }
      },
      "required": [
        "catalog_id",
        "database_name",
        "name",
        "column_names"
      ],
      "additionalProperties": false
    },
    "resource": {
      "type": "object",
      "properties": {
        "catalog": {
          "$ref": "#/definitions/catalog_resource"
        },
        "database": {
          "$ref": "#/definitions/database_resource"
        },
        "table": {
          "$ref": "#/definitions/table_resource"
        },
        "table_with_columns": {
          "$ref": "#/definitions/table_with_columns_resource"
        }
      },
      "additionalProperties": false
    },
    "lf_tag_key": {
      "type": "string",
      "minLength": 1,
      "maxLength": 128
    },
    "lf_tag_value": {
      "type": "string",
      "minLength": 0,
      "maxLength": 256
    },
    "tag_value_list": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/lf_tag_value"
      },
      "insertionOrder": false,
      "minItems": 1,
      "maxItems": 50
    }
  },
  "properties": {
    "resource": {
      "description": "Resource to tag with the Lake Formation Tags",
      "$ref": "#/definitions/resource"
    },
    "lf_tags": {
      "description": "List of Lake Formation Tags to associate with the Lake Formation Resource",
      "$ref": "#/definitions/lf_tags_list"
    },
    "resource_identifier": {
      "description": "Unique string identifying the resource. Used as primary identifier, which ideally should be a string",
      "type": "string"
    },
    "tags_identifier": {
      "description": "Unique string identifying the resource's tags. Used as primary identifier, which ideally should be a string",
      "type": "string"
    }
  },
  "additionalProperties": false,
  "required": [
    "resource",
    "lf_tags"
  ],
  "createOnlyProperties": [
    "/properties/resource",
    "/properties/lf_tags"
  ],
  "readOnlyProperties": [
    "/properties/resource_identifier",
    "/properties/tags_identifier"
  ],
  "replacementStrategy": "delete_then_create",
  "tagging": {
    "taggable": false
  },
  "primaryIdentifier": [
    "/properties/resource_identifier",
    "/properties/tags_identifier"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "lakeformation:AddLFTagsToResource",
        "glue:GetDatabase",
        "glue:GetTable"
      ]
    },
    "read": {
      "permissions": [
        "lakeformation:GetResourceLFTags",
        "glue:GetDatabase",
        "glue:GetTable"
      ]
    },
    "delete": {
      "permissions": [
        "lakeformation:RemoveLFTagsFromResource",
        "glue:GetDatabase",
        "glue:GetTable"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": false
}