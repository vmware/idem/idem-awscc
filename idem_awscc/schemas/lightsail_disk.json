{
  "typeName": "AWS::Lightsail::Disk",
  "description": "Resource Type definition for AWS::Lightsail::Disk",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-lightsail.git",
  "definitions": {
    "tag": {
      "description": "A key-value pair to associate with a resource.",
      "type": "object",
      "properties": {
        "key": {
          "type": "string",
          "description": "The key name of the tag. You can specify a value that is 1 to 128 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -.",
          "minLength": 1,
          "maxLength": 128
        },
        "value": {
          "type": "string",
          "description": "The value for the tag. You can specify a value that is 0 to 256 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -.",
          "minLength": 0,
          "maxLength": 256
        }
      },
      "required": [
        "key"
      ],
      "additionalProperties": false
    },
    "auto_snapshot_add_on": {
      "description": "An object that represents additional parameters when enabling or modifying the automatic snapshot add-on",
      "type": "object",
      "properties": {
        "snapshot_time_of_day": {
          "type": "string",
          "description": "The daily time when an automatic snapshot will be created.",
          "pattern": "^[0-9]{2}:00$"
        }
      },
      "additionalProperties": false
    },
    "add_on": {
      "description": "A addon associate with a resource.",
      "type": "object",
      "properties": {
        "add_on_type": {
          "type": "string",
          "description": "The add-on type",
          "minLength": 1,
          "maxLength": 128
        },
        "status": {
          "type": "string",
          "description": "Status of the Addon",
          "enum": [
            "Enabling",
            "Disabling",
            "Enabled",
            "Terminating",
            "Terminated",
            "Disabled",
            "Failed"
          ]
        },
        "auto_snapshot_add_on_request": {
          "$ref": "#/definitions/auto_snapshot_add_on"
        }
      },
      "required": [
        "add_on_type"
      ],
      "additionalProperties": false
    },
    "location": {
      "description": "Location of a resource.",
      "type": "object",
      "properties": {
        "availability_zone": {
          "type": "string",
          "description": "The Availability Zone in which to create your disk. Use the following format: us-east-2a (case sensitive). Be sure to add the include Availability Zones parameter to your request."
        },
        "region_name": {
          "type": "string",
          "description": "The Region Name in which to create your disk."
        }
      },
      "additionalProperties": false
    }
  },
  "properties": {
    "disk_name": {
      "description": "The names to use for your new Lightsail disk.",
      "type": "string",
      "pattern": "^[a-zA-Z0-9][\\w\\-.]*[a-zA-Z0-9]$",
      "minLength": 1,
      "maxLength": 254
    },
    "disk_arn": {
      "type": "string"
    },
    "support_code": {
      "description": "Support code to help identify any issues",
      "type": "string"
    },
    "availability_zone": {
      "description": "The Availability Zone in which to create your instance. Use the following format: us-east-2a (case sensitive). Be sure to add the include Availability Zones parameter to your request.",
      "type": "string",
      "minLength": 1,
      "maxLength": 255
    },
    "location": {
      "$ref": "#/definitions/location"
    },
    "resource_type": {
      "description": "Resource type of Lightsail instance.",
      "type": "string"
    },
    "tags": {
      "description": "An array of key-value pairs to apply to this resource.",
      "type": "array",
      "uniqueItems": true,
      "insertionOrder": false,
      "items": {
        "$ref": "#/definitions/tag"
      }
    },
    "add_ons": {
      "description": "An array of objects representing the add-ons to enable for the new instance.",
      "type": "array",
      "insertionOrder": false,
      "items": {
        "$ref": "#/definitions/add_on"
      }
    },
    "state": {
      "description": "State of the Lightsail disk",
      "type": "string"
    },
    "attachment_state": {
      "description": "Attachment State of the Lightsail disk",
      "type": "string"
    },
    "size_in_gb": {
      "description": "Size of the Lightsail disk",
      "type": "integer"
    },
    "iops": {
      "description": "Iops of the Lightsail disk",
      "type": "integer"
    },
    "is_attached": {
      "description": "Check is Disk is attached state",
      "type": "boolean"
    },
    "path": {
      "description": "Path of the  attached Disk",
      "type": "string"
    },
    "attached_to": {
      "description": "Name of the attached Lightsail Instance",
      "type": "string"
    }
  },
  "additionalProperties": false,
  "required": [
    "disk_name",
    "size_in_gb"
  ],
  "readOnlyProperties": [
    "/properties/attached_to",
    "/properties/path",
    "/properties/is_attached",
    "/properties/iops",
    "/properties/attachment_state",
    "/properties/state",
    "/properties/resource_type",
    "/properties/location",
    "/properties/support_code",
    "/properties/disk_arn"
  ],
  "taggable": true,
  "primaryIdentifier": [
    "/properties/disk_name"
  ],
  "createOnlyProperties": [
    "/properties/disk_name",
    "/properties/availability_zone",
    "/properties/size_in_gb"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "lightsail:CreateDisk",
        "lightsail:EnableAddOn",
        "lightsail:DisableAddOn",
        "lightsail:GetRegions",
        "lightsail:TagResource",
        "lightsail:UntagResource"
      ]
    },
    "read": {
      "permissions": [
        "lightsail:GetDisk",
        "lightsail:GetDisks"
      ]
    },
    "delete": {
      "permissions": [
        "lightsail:GetDisk",
        "lightsail:GetDisks",
        "lightsail:DeleteDisk"
      ]
    },
    "list": {
      "permissions": [
        "lightsail:GetDisks"
      ]
    },
    "update": {
      "permissions": [
        "lightsail:GetDisk",
        "lightsail:GetDisks",
        "lightsail:EnableAddOn",
        "lightsail:DisableAddOn",
        "lightsail:TagResource",
        "lightsail:UntagResource"
      ],
      "timeoutInMinutes": 2160
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}