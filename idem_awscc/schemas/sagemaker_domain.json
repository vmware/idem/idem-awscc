{
  "typeName": "AWS::SageMaker::Domain",
  "description": "Resource Type definition for AWS::SageMaker::Domain",
  "additionalProperties": false,
  "properties": {
    "domain_arn": {
      "type": "string",
      "description": "The Amazon Resource Name (ARN) of the created domain.",
      "maxLength": 256,
      "pattern": "arn:aws[a-z\\-]*:sagemaker:[a-z0-9\\-]*:[0-9]{12}:domain/.*"
    },
    "url": {
      "type": "string",
      "description": "The URL to the created domain.",
      "maxLength": 1024
    },
    "app_network_access_type": {
      "type": "string",
      "description": "Specifies the VPC used for non-EFS traffic. The default value is PublicInternetOnly.",
      "enum": [
        "PublicInternetOnly",
        "VpcOnly"
      ]
    },
    "auth_mode": {
      "type": "string",
      "description": "The mode of authentication that members use to access the domain.",
      "enum": [
        "SSO",
        "IAM"
      ]
    },
    "default_user_settings": {
      "$ref": "#/definitions/user_settings",
      "description": "The default user settings."
    },
    "domain_name": {
      "type": "string",
      "description": "A name for the domain.",
      "maxLength": 63,
      "pattern": "^[a-zA-Z0-9](-*[a-zA-Z0-9]){0,62}"
    },
    "kms_key_id": {
      "type": "string",
      "description": "SageMaker uses AWS KMS to encrypt the EFS volume attached to the domain with an AWS managed customer master key (CMK) by default.",
      "maxLength": 2048,
      "pattern": ".*"
    },
    "subnet_ids": {
      "type": "array",
      "description": "The VPC subnets that Studio uses for communication.",
      "uniqueItems": false,
      "insertionOrder": false,
      "minItems": 1,
      "maxItems": 16,
      "items": {
        "type": "string",
        "maxLength": 32,
        "pattern": "[-0-9a-zA-Z]+"
      }
    },
    "tags": {
      "type": "array",
      "description": "A list of tags to apply to the user profile.",
      "uniqueItems": false,
      "insertionOrder": false,
      "minItems": 0,
      "maxItems": 50,
      "items": {
        "$ref": "#/definitions/tag"
      }
    },
    "vpc_id": {
      "type": "string",
      "description": "The ID of the Amazon Virtual Private Cloud (VPC) that Studio uses for communication.",
      "maxLength": 32,
      "pattern": "[-0-9a-zA-Z]+"
    },
    "domain_id": {
      "type": "string",
      "description": "The domain name.",
      "maxLength": 63,
      "pattern": "^d-(-*[a-z0-9])+"
    },
    "home_efs_file_system_id": {
      "type": "string",
      "description": "The ID of the Amazon Elastic File System (EFS) managed by this Domain.",
      "maxLength": 32
    },
    "single_sign_on_managed_application_instance_id": {
      "type": "string",
      "description": "The SSO managed application instance ID.",
      "maxLength": 256
    },
    "domain_settings": {
      "$ref": "#/definitions/domain_settings"
    },
    "app_security_group_management": {
      "type": "string",
      "description": "The entity that creates and manages the required security groups for inter-app communication in VPCOnly mode. Required when CreateDomain.AppNetworkAccessType is VPCOnly and DomainSettings.RStudioServerProDomainSettings.DomainExecutionRoleArn is provided.",
      "enum": [
        "Service",
        "Customer"
      ]
    },
    "security_group_id_for_domain_boundary": {
      "type": "string",
      "description": "The ID of the security group that authorizes traffic between the RSessionGateway apps and the RStudioServerPro app.",
      "maxLength": 32,
      "pattern": "[-0-9a-zA-Z]+"
    }
  },
  "definitions": {
    "user_settings": {
      "type": "object",
      "description": "A collection of settings that apply to users of Amazon SageMaker Studio. These settings are specified when the CreateUserProfile API is called, and as DefaultUserSettings when the CreateDomain API is called.",
      "additionalProperties": false,
      "properties": {
        "execution_role": {
          "type": "string",
          "description": "The user profile Amazon Resource Name (ARN).",
          "minLength": 20,
          "maxLength": 2048,
          "pattern": "^arn:aws[a-z\\-]*:iam::\\d{12}:role/?[a-zA-Z_0-9+=,.@\\-_/]+$"
        },
        "jupyter_server_app_settings": {
          "$ref": "#/definitions/jupyter_server_app_settings",
          "description": "The Jupyter server's app settings."
        },
        "kernel_gateway_app_settings": {
          "$ref": "#/definitions/kernel_gateway_app_settings",
          "description": "The kernel gateway app settings."
        },
        "r_studio_server_pro_app_settings": {
          "$ref": "#/definitions/r_studio_server_pro_app_settings"
        },
        "r_session_app_settings": {
          "$ref": "#/definitions/r_session_app_settings"
        },
        "security_groups": {
          "type": "array",
          "description": "The security groups for the Amazon Virtual Private Cloud (VPC) that Studio uses for communication.",
          "uniqueItems": false,
          "insertionOrder": false,
          "minItems": 0,
          "maxItems": 5,
          "items": {
            "type": "string",
            "maxLength": 32,
            "pattern": "[-0-9a-zA-Z]+"
          }
        },
        "sharing_settings": {
          "$ref": "#/definitions/sharing_settings",
          "description": "The sharing settings."
        }
      }
    },
    "jupyter_server_app_settings": {
      "type": "object",
      "description": "The JupyterServer app settings.",
      "additionalProperties": false,
      "properties": {
        "default_resource_spec": {
          "$ref": "#/definitions/resource_spec"
        }
      }
    },
    "resource_spec": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "instance_type": {
          "type": "string",
          "description": "The instance type that the image version runs on.",
          "enum": [
            "system",
            "ml.t3.micro",
            "ml.t3.small",
            "ml.t3.medium",
            "ml.t3.large",
            "ml.t3.xlarge",
            "ml.t3.2xlarge",
            "ml.m5.large",
            "ml.m5.xlarge",
            "ml.m5.2xlarge",
            "ml.m5.4xlarge",
            "ml.m5.8xlarge",
            "ml.m5.12xlarge",
            "ml.m5.16xlarge",
            "ml.m5.24xlarge",
            "ml.c5.large",
            "ml.c5.xlarge",
            "ml.c5.2xlarge",
            "ml.c5.4xlarge",
            "ml.c5.9xlarge",
            "ml.c5.12xlarge",
            "ml.c5.18xlarge",
            "ml.c5.24xlarge",
            "ml.p3.2xlarge",
            "ml.p3.8xlarge",
            "ml.p3.16xlarge",
            "ml.g4dn.xlarge",
            "ml.g4dn.2xlarge",
            "ml.g4dn.4xlarge",
            "ml.g4dn.8xlarge",
            "ml.g4dn.12xlarge",
            "ml.g4dn.16xlarge"
          ]
        },
        "sage_maker_image_arn": {
          "type": "string",
          "description": "The Amazon Resource Name (ARN) of the SageMaker image that the image version belongs to.",
          "maxLength": 256,
          "pattern": "^arn:aws(-[\\w]+)*:sagemaker:.+:[0-9]{12}:image/[a-z0-9]([-.]?[a-z0-9])*$"
        },
        "sage_maker_image_version_arn": {
          "type": "string",
          "description": "The Amazon Resource Name (ARN) of the image version created on the instance.",
          "maxLength": 256,
          "pattern": "^arn:aws(-[\\w]+)*:sagemaker:.+:[0-9]{12}:image-version/[a-z0-9]([-.]?[a-z0-9])*/[0-9]+$"
        },
        "lifecycle_config_arn": {
          "type": "string",
          "description": "The Amazon Resource Name (ARN) of the Lifecycle Configuration to attach to the Resource.",
          "maxLength": 256,
          "pattern": "^arn:aws(-[\\w]+)*:sagemaker:.+:[0-9]{12}:image-version/[a-z0-9]([-.]?[a-z0-9])*/[0-9]+$"
        }
      }
    },
    "kernel_gateway_app_settings": {
      "type": "object",
      "description": "The kernel gateway app settings.",
      "additionalProperties": false,
      "properties": {
        "custom_images": {
          "type": "array",
          "description": "A list of custom SageMaker images that are configured to run as a KernelGateway app.",
          "uniqueItems": false,
          "insertionOrder": false,
          "minItems": 0,
          "maxItems": 30,
          "items": {
            "$ref": "#/definitions/custom_image"
          }
        },
        "default_resource_spec": {
          "$ref": "#/definitions/resource_spec",
          "description": "The default instance type and the Amazon Resource Name (ARN) of the default SageMaker image used by the KernelGateway app."
        }
      }
    },
    "custom_image": {
      "type": "object",
      "description": "A custom SageMaker image.",
      "additionalProperties": false,
      "properties": {
        "app_image_config_name": {
          "type": "string",
          "description": "The Name of the AppImageConfig.",
          "maxLength": 63,
          "pattern": "^[a-zA-Z0-9](-*[a-zA-Z0-9]){0,62}"
        },
        "image_name": {
          "type": "string",
          "description": "The name of the CustomImage. Must be unique to your account.",
          "maxLength": 63,
          "pattern": "^[a-zA-Z0-9]([-.]?[a-zA-Z0-9]){0,62}$"
        },
        "image_version_number": {
          "type": "integer",
          "description": "The version number of the CustomImage.",
          "minimum": 0
        }
      },
      "required": [
        "app_image_config_name",
        "image_name"
      ]
    },
    "sharing_settings": {
      "type": "object",
      "description": "Specifies options when sharing an Amazon SageMaker Studio notebook. These settings are specified as part of DefaultUserSettings when the CreateDomain API is called, and as part of UserSettings when the CreateUserProfile API is called.",
      "additionalProperties": false,
      "properties": {
        "notebook_output_option": {
          "type": "string",
          "description": "Whether to include the notebook cell output when sharing the notebook. The default is Disabled.",
          "enum": [
            "Allowed",
            "Disabled"
          ]
        },
        "s3_kms_key_id": {
          "type": "string",
          "description": "When NotebookOutputOption is Allowed, the AWS Key Management Service (KMS) encryption key ID used to encrypt the notebook cell output in the Amazon S3 bucket.",
          "maxLength": 2048,
          "pattern": ".*"
        },
        "s3_output_path": {
          "type": "string",
          "description": "When NotebookOutputOption is Allowed, the Amazon S3 bucket used to store the shared notebook snapshots.",
          "maxLength": 1024,
          "pattern": "^(https|s3)://([^/]+)/?(.*)$"
        }
      }
    },
    "tag": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "value": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        },
        "key": {
          "type": "string",
          "minLength": 1,
          "maxLength": 128
        }
      },
      "required": [
        "key",
        "value"
      ]
    },
    "domain_settings": {
      "type": "object",
      "description": "A collection of Domain settings.",
      "additionalProperties": false,
      "properties": {
        "security_group_ids": {
          "type": "array",
          "description": "The security groups for the Amazon Virtual Private Cloud that the Domain uses for communication between Domain-level apps and user apps.",
          "uniqueItems": false,
          "insertionOrder": false,
          "minItems": 1,
          "maxItems": 3,
          "items": {
            "type": "string",
            "maxLength": 32,
            "pattern": "[-0-9a-zA-Z]+"
          }
        },
        "r_studio_server_pro_domain_settings": {
          "$ref": "#/definitions/r_studio_server_pro_domain_settings"
        }
      }
    },
    "r_studio_server_pro_domain_settings": {
      "type": "object",
      "description": "A collection of settings that update the current configuration for the RStudioServerPro Domain-level app.",
      "additionalProperties": false,
      "properties": {
        "domain_execution_role_arn": {
          "type": "string",
          "description": "The ARN of the execution role for the RStudioServerPro Domain-level app.",
          "minLength": 20,
          "maxLength": 2048,
          "pattern": "^arn:aws[a-z\\-]*:iam::\\d{12}:role/?[a-zA-Z_0-9+=,.@\\-_/]+$"
        },
        "r_studio_connect_url": {
          "type": "string",
          "description": "A URL pointing to an RStudio Connect server.",
          "pattern": "^(https:|http:|www\\.)\\S*"
        },
        "r_studio_package_manager_url": {
          "type": "string",
          "description": "A URL pointing to an RStudio Package Manager server.",
          "pattern": "^(https:|http:|www\\.)\\S*"
        },
        "default_resource_spec": {
          "$ref": "#/definitions/resource_spec"
        }
      },
      "required": [
        "domain_execution_role_arn"
      ]
    },
    "r_session_app_settings": {
      "type": "object",
      "description": "A collection of settings that apply to an RSessionGateway app.",
      "additionalProperties": false,
      "properties": {
        "custom_images": {
          "type": "array",
          "description": "A list of custom SageMaker images that are configured to run as a KernelGateway app.",
          "insertionOrder": false,
          "uniqueItems": false,
          "minItems": 0,
          "maxItems": 30,
          "items": {
            "$ref": "#/definitions/custom_image"
          }
        },
        "default_resource_spec": {
          "$ref": "#/definitions/resource_spec"
        }
      }
    },
    "r_studio_server_pro_app_settings": {
      "type": "object",
      "description": "A collection of settings that configure user interaction with the RStudioServerPro app.",
      "additionalProperties": false,
      "properties": {
        "access_status": {
          "type": "string",
          "description": "Indicates whether the current user has access to the RStudioServerPro app.",
          "enum": [
            "ENABLED",
            "DISABLED"
          ]
        },
        "user_group": {
          "type": "string",
          "description": "The level of permissions that the user has within the RStudioServerPro app. This value defaults to User. The Admin value allows the user access to the RStudio Administrative Dashboard.",
          "enum": [
            "R_STUDIO_ADMIN",
            "R_STUDIO_USER"
          ]
        }
      }
    }
  },
  "required": [
    "auth_mode",
    "default_user_settings",
    "domain_name",
    "subnet_ids",
    "vpc_id"
  ],
  "createOnlyProperties": [
    "/properties/app_network_access_type",
    "/properties/app_security_group_management",
    "/properties/auth_mode",
    "/properties/domain_name",
    "/properties/domain_settings/security_group_ids",
    "/properties/domain_settings/r_studio_server_pro_domain_settings/domain_execution_role_arn",
    "/properties/domain_settings/r_studio_server_pro_domain_settings/default_resource_spec",
    "/properties/kms_key_id",
    "/properties/subnet_ids",
    "/properties/vpc_id",
    "/properties/tags"
  ],
  "writeOnlyProperties": [
    "/properties/tags"
  ],
  "primaryIdentifier": [
    "/properties/domain_id"
  ],
  "readOnlyProperties": [
    "/properties/domain_arn",
    "/properties/url",
    "/properties/domain_id",
    "/properties/home_efs_file_system_id",
    "/properties/security_group_id_for_domain_boundary",
    "/properties/single_sign_on_managed_application_instance_id"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "sagemaker:CreateApp",
        "sagemaker:CreateDomain",
        "sagemaker:DescribeDomain",
        "sagemaker:DescribeImage",
        "sagemaker:DescribeImageVersion",
        "iam:CreateServiceLinkedRole",
        "iam:PassRole",
        "efs:CreateFileSystem",
        "kms:CreateGrant",
        "kms:Decrypt",
        "kms:DescribeKey",
        "kms:GenerateDataKeyWithoutPlainText"
      ]
    },
    "read": {
      "permissions": [
        "sagemaker:DescribeDomain"
      ]
    },
    "update": {
      "permissions": [
        "sagemaker:UpdateDomain",
        "sagemaker:DescribeDomain",
        "sagemaker:DescribeImage",
        "sagemaker:DescribeImageVersion",
        "iam:PassRole"
      ]
    },
    "delete": {
      "permissions": [
        "sagemaker:DeleteApp",
        "sagemaker:DeleteDomain",
        "sagemaker:DescribeDomain"
      ]
    },
    "list": {
      "permissions": [
        "sagemaker:ListDomains"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}