{
  "typeName": "AWS::OpsWorksCM::Server",
  "description": "Resource Type definition for AWS::OpsWorksCM::Server",
  "additionalProperties": false,
  "properties": {
    "key_pair": {
      "type": "string",
      "pattern": ".*",
      "maxLength": 10000
    },
    "engine_version": {
      "type": "string",
      "maxLength": 10000
    },
    "service_role_arn": {
      "type": "string",
      "pattern": "arn:aws:iam::[0-9]{12}:role/.*",
      "maxLength": 10000
    },
    "disable_automated_backup": {
      "type": "boolean"
    },
    "backup_id": {
      "type": "string",
      "pattern": "[a-zA-Z][a-zA-Z0-9\\-\\.\\:]*",
      "maxLength": 79
    },
    "engine_model": {
      "type": "string",
      "maxLength": 10000
    },
    "preferred_maintenance_window": {
      "type": "string",
      "pattern": "^((Mon|Tue|Wed|Thu|Fri|Sat|Sun):)?([0-1][0-9]|2[0-3]):[0-5][0-9]$",
      "maxLength": 10000
    },
    "associate_public_ip_address": {
      "type": "boolean"
    },
    "instance_profile_arn": {
      "type": "string",
      "pattern": "arn:aws:iam::[0-9]{12}:instance-profile/.*",
      "maxLength": 10000
    },
    "custom_certificate": {
      "type": "string",
      "pattern": "(?s)\\s*-----BEGIN CERTIFICATE-----.+-----END CERTIFICATE-----\\s*",
      "maxLength": 2097152
    },
    "preferred_backup_window": {
      "type": "string",
      "pattern": "^((Mon|Tue|Wed|Thu|Fri|Sat|Sun):)?([0-1][0-9]|2[0-3]):[0-5][0-9]$",
      "maxLength": 10000
    },
    "security_group_ids": {
      "type": "array",
      "uniqueItems": false,
      "items": {
        "type": "string",
        "maxLength": 10000
      }
    },
    "subnet_ids": {
      "type": "array",
      "uniqueItems": false,
      "items": {
        "type": "string",
        "maxLength": 10000
      }
    },
    "custom_domain": {
      "type": "string",
      "pattern": "^(((?!-)[A-Za-z0-9-]{0,62}[A-Za-z0-9])\\.)+((?!-)[A-Za-z0-9-]{1,62}[A-Za-z0-9])$",
      "maxLength": 253
    },
    "endpoint": {
      "type": "string",
      "maxLength": 10000
    },
    "custom_private_key": {
      "type": "string",
      "pattern": "(?ms)\\s*^-----BEGIN (?-s:.*)PRIVATE KEY-----$.*?^-----END (?-s:.*)PRIVATE KEY-----$\\s*",
      "maxLength": 4096
    },
    "server_name": {
      "type": "string",
      "minLength": 1,
      "maxLength": 40,
      "pattern": "[a-zA-Z][a-zA-Z0-9\\-]*"
    },
    "engine_attributes": {
      "type": "array",
      "uniqueItems": false,
      "items": {
        "$ref": "#/definitions/engine_attribute"
      }
    },
    "backup_retention_count": {
      "type": "integer",
      "minLength": 1
    },
    "id": {
      "type": "string",
      "maxLength": 10000
    },
    "arn": {
      "type": "string",
      "maxLength": 10000
    },
    "instance_type": {
      "type": "string",
      "maxLength": 10000
    },
    "tags": {
      "type": "array",
      "uniqueItems": false,
      "items": {
        "$ref": "#/definitions/tag"
      }
    },
    "engine": {
      "type": "string",
      "maxLength": 10000
    }
  },
  "definitions": {
    "engine_attribute": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "value": {
          "type": "string",
          "pattern": "(?s).*",
          "maxLength": 10000
        },
        "name": {
          "type": "string",
          "pattern": "(?s).*",
          "maxLength": 10000
        }
      }
    },
    "tag": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "value": {
          "type": "string",
          "pattern": "^([\\p{L}\\p{Z}\\p{N}_.:/=+\\-@]*)$",
          "minLength": 0,
          "maxLength": 256
        },
        "key": {
          "type": "string",
          "pattern": "^([\\p{L}\\p{Z}\\p{N}_.:/=+\\-@]*)$",
          "minLength": 1,
          "maxLength": 128
        }
      },
      "required": [
        "value",
        "key"
      ]
    }
  },
  "required": [
    "service_role_arn",
    "instance_profile_arn",
    "instance_type"
  ],
  "createOnlyProperties": [
    "/properties/key_pair",
    "/properties/custom_private_key",
    "/properties/service_role_arn",
    "/properties/instance_type",
    "/properties/custom_certificate",
    "/properties/custom_domain",
    "/properties/instance_profile_arn",
    "/properties/security_group_ids",
    "/properties/server_name",
    "/properties/subnet_ids",
    "/properties/backup_id",
    "/properties/engine_model",
    "/properties/associate_public_ip_address",
    "/properties/engine_version",
    "/properties/engine"
  ],
  "primaryIdentifier": [
    "/properties/server_name"
  ],
  "readOnlyProperties": [
    "/properties/id",
    "/properties/endpoint",
    "/properties/arn"
  ],
  "writeOnlyProperties": [
    "/properties/custom_private_key",
    "/properties/engine_attributes"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "opsworks-cm:CreateServer",
        "opsworks-cm:DescribeServers",
        "iam:PassRole"
      ]
    },
    "delete": {
      "permissions": [
        "opsworks-cm:DeleteServer",
        "opsworks-cm:DescribeServers"
      ]
    },
    "update": {
      "permissions": [
        "opsworks-cm:UpdateServer",
        "opsworks-cm:TagResource",
        "opsworks-cm:UntagResource",
        "opsworks-cm:DescribeServers"
      ]
    },
    "list": {
      "permissions": [
        "opsworks-cm:DescribeServers",
        "opsworks-cm:ListTagsForResource"
      ]
    },
    "read": {
      "permissions": [
        "opsworks-cm:DescribeServers"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}