{
  "typeName": "AWS::SageMaker::ModelQualityJobDefinition",
  "description": "Resource Type definition for AWS::SageMaker::ModelQualityJobDefinition",
  "additionalProperties": false,
  "properties": {
    "job_definition_arn": {
      "description": "The Amazon Resource Name (ARN) of job definition.",
      "type": "string",
      "minLength": 1,
      "maxLength": 256
    },
    "job_definition_name": {
      "$ref": "#/definitions/job_definition_name"
    },
    "model_quality_baseline_config": {
      "$ref": "#/definitions/model_quality_baseline_config"
    },
    "model_quality_app_specification": {
      "$ref": "#/definitions/model_quality_app_specification"
    },
    "model_quality_job_input": {
      "$ref": "#/definitions/model_quality_job_input"
    },
    "model_quality_job_output_config": {
      "$ref": "#/definitions/monitoring_output_config"
    },
    "job_resources": {
      "$ref": "#/definitions/monitoring_resources"
    },
    "network_config": {
      "$ref": "#/definitions/network_config"
    },
    "endpoint_name": {
      "$ref": "#/definitions/endpoint_name"
    },
    "role_arn": {
      "description": "The Amazon Resource Name (ARN) of an IAM role that Amazon SageMaker can assume to perform tasks on your behalf.",
      "type": "string",
      "pattern": "^arn:aws[a-z\\-]*:iam::\\d{12}:role/?[a-zA-Z_0-9+=,.@\\-_/]+$",
      "minLength": 20,
      "maxLength": 2048
    },
    "stopping_condition": {
      "$ref": "#/definitions/stopping_condition"
    },
    "tags": {
      "type": "array",
      "maxItems": 50,
      "description": "An array of key-value pairs to apply to this resource.",
      "items": {
        "$ref": "#/definitions/tag"
      }
    },
    "creation_time": {
      "description": "The time at which the job definition was created.",
      "type": "string"
    }
  },
  "definitions": {
    "model_quality_baseline_config": {
      "type": "object",
      "additionalProperties": false,
      "description": "Baseline configuration used to validate that the data conforms to the specified constraints and statistics.",
      "properties": {
        "baselining_job_name": {
          "$ref": "#/definitions/processing_job_name"
        },
        "constraints_resource": {
          "$ref": "#/definitions/constraints_resource"
        }
      }
    },
    "constraints_resource": {
      "type": "object",
      "additionalProperties": false,
      "description": "The baseline constraints resource for a monitoring job.",
      "properties": {
        "s3_uri": {
          "description": "The Amazon S3 URI for baseline constraint file in Amazon S3 that the current monitoring job should validated against.",
          "$ref": "#/definitions/s3_uri"
        }
      }
    },
    "s3_uri": {
      "type": "string",
      "description": "The Amazon S3 URI.",
      "pattern": "^(https|s3)://([^/]+)/?(.*)$",
      "maxLength": 1024
    },
    "model_quality_app_specification": {
      "type": "object",
      "additionalProperties": false,
      "description": "Container image configuration object for the monitoring job.",
      "properties": {
        "container_arguments": {
          "type": "array",
          "description": "An array of arguments for the container used to run the monitoring job.",
          "maxItems": 50,
          "items": {
            "type": "string",
            "minLength": 1,
            "maxLength": 256
          }
        },
        "container_entrypoint": {
          "type": "array",
          "description": "Specifies the entrypoint for a container used to run the monitoring job.",
          "maxItems": 100,
          "items": {
            "type": "string",
            "minLength": 1,
            "maxLength": 256
          }
        },
        "image_uri": {
          "type": "string",
          "description": "The container image to be run by the monitoring job.",
          "pattern": ".*",
          "maxLength": 255
        },
        "post_analytics_processor_source_uri": {
          "description": "An Amazon S3 URI to a script that is called after analysis has been performed. Applicable only for the built-in (first party) containers.",
          "$ref": "#/definitions/s3_uri"
        },
        "record_preprocessor_source_uri": {
          "description": "An Amazon S3 URI to a script that is called per row prior to running analysis. It can base64 decode the payload and convert it into a flatted json so that the built-in container can use the converted data. Applicable only for the built-in (first party) containers",
          "$ref": "#/definitions/s3_uri"
        },
        "environment": {
          "type": "object",
          "additionalProperties": false,
          "description": "Sets the environment variables in the Docker container",
          "patternProperties": {
            "[a-zA-Z_][a-zA-Z0-9_]*": {
              "type": "string",
              "minLength": 1,
              "maxLength": 256
            },
            "[\\S\\s]*": {
              "type": "string",
              "maxLength": 256
            }
          }
        },
        "problem_type": {
          "$ref": "#/definitions/problem_type"
        }
      },
      "required": [
        "image_uri",
        "problem_type"
      ]
    },
    "model_quality_job_input": {
      "type": "object",
      "additionalProperties": false,
      "description": "The inputs for a monitoring job.",
      "properties": {
        "endpoint_input": {
          "$ref": "#/definitions/endpoint_input"
        },
        "batch_transform_input": {
          "$ref": "#/definitions/batch_transform_input"
        },
        "ground_truths3_input": {
          "$ref": "#/definitions/monitoring_ground_truths3_input"
        }
      },
      "required": [
        "ground_truths3_input"
      ]
    },
    "endpoint_input": {
      "type": "object",
      "additionalProperties": false,
      "description": "The endpoint for a monitoring job.",
      "properties": {
        "endpoint_name": {
          "$ref": "#/definitions/endpoint_name"
        },
        "local_path": {
          "type": "string",
          "description": "Path to the filesystem where the endpoint data is available to the container.",
          "pattern": ".*",
          "maxLength": 256
        },
        "s3_data_distribution_type": {
          "type": "string",
          "description": "Whether input data distributed in Amazon S3 is fully replicated or sharded by an S3 key. Defauts to FullyReplicated",
          "enum": [
            "FullyReplicated",
            "ShardedByS3Key"
          ]
        },
        "s3_input_mode": {
          "type": "string",
          "description": "Whether the Pipe or File is used as the input mode for transfering data for the monitoring job. Pipe mode is recommended for large datasets. File mode is useful for small files that fit in memory. Defaults to File.",
          "enum": [
            "Pipe",
            "File"
          ]
        },
        "start_time_offset": {
          "description": "Monitoring start time offset, e.g. -PT1H",
          "$ref": "#/definitions/monitoring_time_offset_string"
        },
        "end_time_offset": {
          "description": "Monitoring end time offset, e.g. PT0H",
          "$ref": "#/definitions/monitoring_time_offset_string"
        },
        "inference_attribute": {
          "type": "string",
          "description": "Index or JSONpath to locate predicted label(s)",
          "maxLength": 256
        },
        "probability_attribute": {
          "type": "string",
          "description": "Index or JSONpath to locate probabilities",
          "maxLength": 256
        },
        "probability_threshold_attribute": {
          "type": "number",
          "format": "double"
        }
      },
      "required": [
        "endpoint_name",
        "local_path"
      ]
    },
    "batch_transform_input": {
      "type": "object",
      "additionalProperties": false,
      "description": "The batch transform input for a monitoring job.",
      "properties": {
        "data_captured_destinations3_uri": {
          "type": "string",
          "description": "A URI that identifies the Amazon S3 storage location where Batch Transform Job captures data.",
          "pattern": "^(https|s3)://([^/]+)/?(.*)$",
          "maxLength": 512
        },
        "dataset_format": {
          "$ref": "#/definitions/dataset_format"
        },
        "local_path": {
          "type": "string",
          "description": "Path to the filesystem where the endpoint data is available to the container.",
          "pattern": ".*",
          "maxLength": 256
        },
        "s3_data_distribution_type": {
          "type": "string",
          "description": "Whether input data distributed in Amazon S3 is fully replicated or sharded by an S3 key. Defauts to FullyReplicated",
          "enum": [
            "FullyReplicated",
            "ShardedByS3Key"
          ]
        },
        "s3_input_mode": {
          "type": "string",
          "description": "Whether the Pipe or File is used as the input mode for transfering data for the monitoring job. Pipe mode is recommended for large datasets. File mode is useful for small files that fit in memory. Defaults to File.",
          "enum": [
            "Pipe",
            "File"
          ]
        },
        "start_time_offset": {
          "description": "Monitoring start time offset, e.g. -PT1H",
          "$ref": "#/definitions/monitoring_time_offset_string"
        },
        "end_time_offset": {
          "description": "Monitoring end time offset, e.g. PT0H",
          "$ref": "#/definitions/monitoring_time_offset_string"
        },
        "inference_attribute": {
          "type": "string",
          "description": "Index or JSONpath to locate predicted label(s)",
          "maxLength": 256
        },
        "probability_attribute": {
          "type": "string",
          "description": "Index or JSONpath to locate probabilities",
          "maxLength": 256
        },
        "probability_threshold_attribute": {
          "type": "number",
          "format": "double"
        }
      },
      "required": [
        "data_captured_destinations3_uri",
        "dataset_format",
        "local_path"
      ]
    },
    "monitoring_output_config": {
      "type": "object",
      "additionalProperties": false,
      "description": "The output configuration for monitoring jobs.",
      "properties": {
        "kms_key_id": {
          "type": "string",
          "description": "The AWS Key Management Service (AWS KMS) key that Amazon SageMaker uses to encrypt the model artifacts at rest using Amazon S3 server-side encryption.",
          "pattern": ".*",
          "maxLength": 2048
        },
        "monitoring_outputs": {
          "type": "array",
          "description": "Monitoring outputs for monitoring jobs. This is where the output of the periodic monitoring jobs is uploaded.",
          "minLength": 1,
          "maxLength": 1,
          "items": {
            "$ref": "#/definitions/monitoring_output"
          }
        }
      },
      "required": [
        "monitoring_outputs"
      ]
    },
    "monitoring_output": {
      "type": "object",
      "additionalProperties": false,
      "description": "The output object for a monitoring job.",
      "properties": {
        "s3_output": {
          "$ref": "#/definitions/s3_output"
        }
      },
      "required": [
        "s3_output"
      ]
    },
    "s3_output": {
      "type": "object",
      "additionalProperties": false,
      "description": "Information about where and how to store the results of a monitoring job.",
      "properties": {
        "local_path": {
          "type": "string",
          "description": "The local path to the Amazon S3 storage location where Amazon SageMaker saves the results of a monitoring job. LocalPath is an absolute path for the output data.",
          "pattern": ".*",
          "maxLength": 256
        },
        "s3_upload_mode": {
          "type": "string",
          "description": "Whether to upload the results of the monitoring job continuously or after the job completes.",
          "enum": [
            "Continuous",
            "EndOfJob"
          ]
        },
        "s3_uri": {
          "type": "string",
          "description": "A URI that identifies the Amazon S3 storage location where Amazon SageMaker saves the results of a monitoring job.",
          "pattern": "^(https|s3)://([^/]+)/?(.*)$",
          "maxLength": 512
        }
      },
      "required": [
        "local_path",
        "s3_uri"
      ]
    },
    "monitoring_resources": {
      "type": "object",
      "additionalProperties": false,
      "description": "Identifies the resources to deploy for a monitoring job.",
      "properties": {
        "cluster_config": {
          "$ref": "#/definitions/cluster_config"
        }
      },
      "required": [
        "cluster_config"
      ]
    },
    "cluster_config": {
      "type": "object",
      "additionalProperties": false,
      "description": "Configuration for the cluster used to run model monitoring jobs.",
      "properties": {
        "instance_count": {
          "description": "The number of ML compute instances to use in the model monitoring job. For distributed processing jobs, specify a value greater than 1. The default value is 1.",
          "type": "integer",
          "minimum": 1,
          "maximum": 100
        },
        "instance_type": {
          "description": "The ML compute instance type for the processing job.",
          "type": "string"
        },
        "volume_kms_key_id": {
          "description": "The AWS Key Management Service (AWS KMS) key that Amazon SageMaker uses to encrypt data on the storage volume attached to the ML compute instance(s) that run the model monitoring job.",
          "type": "string",
          "minimum": 1,
          "maximum": 2048
        },
        "volume_size_in_gb": {
          "description": "The size of the ML storage volume, in gigabytes, that you want to provision. You must specify sufficient ML storage for your scenario.",
          "type": "integer",
          "minimum": 1,
          "maximum": 16384
        }
      },
      "required": [
        "instance_count",
        "instance_type",
        "volume_size_in_gb"
      ]
    },
    "network_config": {
      "type": "object",
      "additionalProperties": false,
      "description": "Networking options for a job, such as network traffic encryption between containers, whether to allow inbound and outbound network calls to and from containers, and the VPC subnets and security groups to use for VPC-enabled jobs.",
      "properties": {
        "enable_inter_container_traffic_encryption": {
          "description": "Whether to encrypt all communications between distributed processing jobs. Choose True to encrypt communications. Encryption provides greater security for distributed processing jobs, but the processing might take longer.",
          "type": "boolean"
        },
        "enable_network_isolation": {
          "description": "Whether to allow inbound and outbound network calls to and from the containers used for the processing job.",
          "type": "boolean"
        },
        "vpc_config": {
          "$ref": "#/definitions/vpc_config"
        }
      }
    },
    "vpc_config": {
      "type": "object",
      "additionalProperties": false,
      "description": "Specifies a VPC that your training jobs and hosted models have access to. Control access to and from your training and model containers by configuring the VPC.",
      "properties": {
        "security_group_ids": {
          "description": "The VPC security group IDs, in the form sg-xxxxxxxx. Specify the security groups for the VPC that is specified in the Subnets field.",
          "type": "array",
          "minItems": 1,
          "maxItems": 5,
          "items": {
            "type": "string",
            "maxLength": 32,
            "pattern": "[-0-9a-zA-Z]+"
          }
        },
        "subnets": {
          "description": "The ID of the subnets in the VPC to which you want to connect to your monitoring jobs.",
          "type": "array",
          "minItems": 1,
          "maxItems": 16,
          "items": {
            "type": "string",
            "maxLength": 32,
            "pattern": "[-0-9a-zA-Z]+"
          }
        }
      },
      "required": [
        "security_group_ids",
        "subnets"
      ]
    },
    "stopping_condition": {
      "type": "object",
      "additionalProperties": false,
      "description": "Specifies a time limit for how long the monitoring job is allowed to run.",
      "properties": {
        "max_runtime_in_seconds": {
          "description": "The maximum runtime allowed in seconds.",
          "type": "integer",
          "minimum": 1,
          "maximum": 86400
        }
      },
      "required": [
        "max_runtime_in_seconds"
      ]
    },
    "tag": {
      "description": "A key-value pair to associate with a resource.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "key": {
          "type": "string",
          "description": "The key name of the tag. You can specify a value that is 1 to 127 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -. ",
          "minLength": 1,
          "maxLength": 128,
          "pattern": "^([\\p{L}\\p{Z}\\p{N}_.:/=+\\-@]*)$"
        },
        "value": {
          "type": "string",
          "description": "The value for the tag. You can specify a value that is 1 to 255 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -. ",
          "maxLength": 256,
          "pattern": "^([\\p{L}\\p{Z}\\p{N}_.:/=+\\-@]*)$"
        }
      },
      "required": [
        "key",
        "value"
      ]
    },
    "endpoint_name": {
      "type": "string",
      "description": "The name of the endpoint used to run the monitoring job.",
      "pattern": "^[a-zA-Z0-9](-*[a-zA-Z0-9])*",
      "maxLength": 63
    },
    "job_definition_name": {
      "type": "string",
      "description": "The name of the job definition.",
      "pattern": "^[a-zA-Z0-9](-*[a-zA-Z0-9])*$",
      "maxLength": 63
    },
    "processing_job_name": {
      "type": "string",
      "description": "The name of a processing job",
      "pattern": "^[a-zA-Z0-9](-*[a-zA-Z0-9])*$",
      "minLength": 1,
      "maxLength": 63
    },
    "monitoring_time_offset_string": {
      "type": "string",
      "description": "The time offsets in ISO duration format",
      "pattern": "^.?P.*",
      "minLength": 1,
      "maxLength": 15
    },
    "problem_type": {
      "description": "The status of the monitoring job.",
      "type": "string",
      "enum": [
        "BinaryClassification",
        "MulticlassClassification",
        "Regression"
      ]
    },
    "monitoring_ground_truths3_input": {
      "type": "object",
      "additionalProperties": false,
      "description": "Ground truth input provided in S3 ",
      "properties": {
        "s3_uri": {
          "type": "string",
          "description": "A URI that identifies the Amazon S3 storage location where Amazon SageMaker saves the results of a monitoring job.",
          "pattern": "^(https|s3)://([^/]+)/?(.*)$",
          "maxLength": 512
        }
      },
      "required": [
        "s3_uri"
      ]
    },
    "dataset_format": {
      "description": "The dataset format of the data to monitor",
      "type": "object",
      "properties": {
        "csv": {
          "$ref": "#/definitions/csv"
        },
        "json": {
          "$ref": "#/definitions/json"
        },
        "parquet": {
          "$ref": "#/definitions/parquet"
        }
      }
    },
    "csv": {
      "description": "The CSV format",
      "type": "object",
      "properties": {
        "header": {
          "description": "A boolean flag indicating if given CSV has header",
          "type": "boolean"
        }
      }
    },
    "json": {
      "description": "The Json format",
      "type": "object",
      "properties": {
        "line": {
          "description": "A boolean flag indicating if it is JSON line format",
          "type": "boolean"
        }
      }
    },
    "parquet": {
      "description": "A flag indicating if the dataset format is Parquet",
      "type": "boolean"
    }
  },
  "required": [
    "model_quality_app_specification",
    "model_quality_job_input",
    "model_quality_job_output_config",
    "job_resources",
    "role_arn"
  ],
  "primaryIdentifier": [
    "/properties/job_definition_arn"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "sagemaker:CreateModelQualityJobDefinition",
        "sagemaker:DescribeModelQualityJobDefinition",
        "iam:PassRole"
      ]
    },
    "delete": {
      "permissions": [
        "sagemaker:DeleteModelQualityJobDefinition"
      ]
    },
    "read": {
      "permissions": [
        "sagemaker:DescribeModelQualityJobDefinition"
      ]
    },
    "list": {
      "permissions": [
        "sagemaker:ListModelQualityJobDefinitions"
      ]
    }
  },
  "readOnlyProperties": [
    "/properties/creation_time",
    "/properties/job_definition_arn"
  ],
  "createOnlyProperties": [
    "/properties/job_definition_name",
    "/properties/model_quality_app_specification",
    "/properties/model_quality_baseline_config",
    "/properties/model_quality_job_input",
    "/properties/model_quality_job_output_config",
    "/properties/job_resources",
    "/properties/network_config",
    "/properties/role_arn",
    "/properties/stopping_condition",
    "/properties/tags"
  ],
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": false
}