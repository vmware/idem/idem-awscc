{
  "typeName": "AWS::DevOpsGuru::NotificationChannel",
  "description": "This resource schema represents the NotificationChannel resource in the Amazon DevOps Guru.",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-devops-guru",
  "definitions": {
    "notification_channel_config": {
      "description": "Information about notification channels you have configured with DevOps Guru.",
      "type": "object",
      "properties": {
        "sns": {
          "$ref": "#/definitions/sns_channel_config"
        },
        "filters": {
          "$ref": "#/definitions/notification_filter_config"
        }
      },
      "additionalProperties": false
    },
    "sns_channel_config": {
      "description": "Information about a notification channel configured in DevOps Guru to send notifications when insights are created.",
      "type": "object",
      "properties": {
        "topic_arn": {
          "type": "string",
          "minLength": 36,
          "maxLength": 1024,
          "pattern": "^arn:aws[a-z0-9-]*:sns:[a-z0-9-]+:\\d{12}:[^:]+$"
        }
      },
      "additionalProperties": false
    },
    "notification_filter_config": {
      "description": "Information about filters of a notification channel configured in DevOpsGuru to filter for insights.",
      "type": "object",
      "properties": {
        "severities": {
          "$ref": "#/definitions/insight_severities_filter_list"
        },
        "message_types": {
          "$ref": "#/definitions/notification_message_types_filter_list"
        }
      },
      "additionalProperties": false
    },
    "insight_severity": {
      "description": "DevOps Guru Insight Severity Enum",
      "type": "string",
      "enum": [
        "LOW",
        "MEDIUM",
        "HIGH"
      ]
    },
    "notification_message_type": {
      "description": "DevOps Guru NotificationMessageType Enum",
      "type": "string",
      "enum": [
        "NEW_INSIGHT",
        "CLOSED_INSIGHT",
        "NEW_ASSOCIATION",
        "SEVERITY_UPGRADED",
        "NEW_RECOMMENDATION"
      ]
    },
    "insight_severities_filter_list": {
      "description": "DevOps Guru insight severities to filter for",
      "type": "array",
      "insertionOrder": false,
      "items": {
        "$ref": "#/definitions/insight_severity"
      },
      "maxItems": 3,
      "minItems": 1
    },
    "notification_message_types_filter_list": {
      "description": "DevOps Guru message types to filter for",
      "type": "array",
      "insertionOrder": false,
      "items": {
        "$ref": "#/definitions/notification_message_type"
      },
      "maxItems": 5,
      "minItems": 1
    }
  },
  "properties": {
    "config": {
      "$ref": "#/definitions/notification_channel_config"
    },
    "id": {
      "description": "The ID of a notification channel.",
      "type": "string",
      "minLength": 36,
      "maxLength": 36,
      "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$"
    }
  },
  "additionalProperties": false,
  "required": [
    "config"
  ],
  "createOnlyProperties": [
    "/properties/config"
  ],
  "readOnlyProperties": [
    "/properties/id"
  ],
  "primaryIdentifier": [
    "/properties/id"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "devops-guru:AddNotificationChannel",
        "devops-guru:ListNotificationChannels",
        "sns:Publish",
        "sns:GetTopicAttributes",
        "sns:SetTopicAttributes"
      ]
    },
    "list": {
      "permissions": [
        "devops-guru:ListNotificationChannels"
      ]
    },
    "delete": {
      "permissions": [
        "devops-guru:RemoveNotificationChannel",
        "devops-guru:ListNotificationChannels"
      ]
    },
    "read": {
      "permissions": [
        "devops-guru:ListNotificationChannels"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": false
}