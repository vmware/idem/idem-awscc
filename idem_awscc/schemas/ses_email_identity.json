{
  "typeName": "AWS::SES::EmailIdentity",
  "description": "Resource Type definition for AWS::SES::EmailIdentity",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-ses.git",
  "additionalProperties": false,
  "properties": {
    "email_identity": {
      "type": "string",
      "description": "The email address or domain to verify."
    },
    "configuration_set_attributes": {
      "$ref": "#/definitions/configuration_set_attributes"
    },
    "dkim_signing_attributes": {
      "$ref": "#/definitions/dkim_signing_attributes"
    },
    "dkim_attributes": {
      "$ref": "#/definitions/dkim_attributes"
    },
    "mail_from_attributes": {
      "$ref": "#/definitions/mail_from_attributes"
    },
    "feedback_attributes": {
      "$ref": "#/definitions/feedback_attributes"
    },
    "dkim_dns_token_name1": {
      "type": "string"
    },
    "dkim_dns_token_name2": {
      "type": "string"
    },
    "dkim_dns_token_name3": {
      "type": "string"
    },
    "dkim_dns_token_value1": {
      "type": "string"
    },
    "dkim_dns_token_value2": {
      "type": "string"
    },
    "dkim_dns_token_value3": {
      "type": "string"
    }
  },
  "definitions": {
    "dkim_signing_attributes": {
      "type": "object",
      "additionalProperties": false,
      "description": "If your request includes this object, Amazon SES configures the identity to use Bring Your Own DKIM (BYODKIM) for DKIM authentication purposes, or, configures the key length to be used for Easy DKIM.",
      "properties": {
        "domain_signing_selector": {
          "type": "string",
          "description": "[Bring Your Own DKIM] A string that's used to identify a public key in the DNS configuration for a domain."
        },
        "domain_signing_private_key": {
          "type": "string",
          "description": "[Bring Your Own DKIM] A private key that's used to generate a DKIM signature. The private key must use 1024 or 2048-bit RSA encryption, and must be encoded using base64 encoding."
        },
        "next_signing_key_length": {
          "type": "string",
          "description": "[Easy DKIM] The key length of the future DKIM key pair to be generated. This can be changed at most once per day.",
          "pattern": "RSA_1024_BIT|RSA_2048_BIT"
        }
      }
    },
    "configuration_set_attributes": {
      "type": "object",
      "additionalProperties": false,
      "description": "Used to associate a configuration set with an email identity.",
      "properties": {
        "configuration_set_name": {
          "type": "string",
          "description": "The configuration set to use by default when sending from this identity. Note that any configuration set defined in the email sending request takes precedence."
        }
      }
    },
    "dkim_attributes": {
      "type": "object",
      "additionalProperties": false,
      "description": "Used to enable or disable DKIM authentication for an email identity.",
      "properties": {
        "signing_enabled": {
          "type": "boolean",
          "description": "Sets the DKIM signing configuration for the identity. When you set this value true, then the messages that are sent from the identity are signed using DKIM. If you set this value to false, your messages are sent without DKIM signing."
        }
      }
    },
    "mail_from_attributes": {
      "type": "object",
      "additionalProperties": false,
      "description": "Used to enable or disable the custom Mail-From domain configuration for an email identity.",
      "properties": {
        "mail_from_domain": {
          "type": "string",
          "description": "The custom MAIL FROM domain that you want the verified identity to use"
        },
        "behavior_on_mx_failure": {
          "type": "string",
          "description": "The action to take if the required MX record isn't found when you send an email. When you set this value to UseDefaultValue , the mail is sent using amazonses.com as the MAIL FROM domain. When you set this value to RejectMessage , the Amazon SES API v2 returns a MailFromDomainNotVerified error, and doesn't attempt to deliver the email.",
          "pattern": "USE_DEFAULT_VALUE|REJECT_MESSAGE"
        }
      }
    },
    "feedback_attributes": {
      "type": "object",
      "additionalProperties": false,
      "description": "Used to enable or disable feedback forwarding for an identity.",
      "properties": {
        "email_forwarding_enabled": {
          "type": "boolean",
          "description": "If the value is true, you receive email notifications when bounce or complaint events occur"
        }
      }
    }
  },
  "required": [
    "email_identity"
  ],
  "readOnlyProperties": [
    "/properties/dkim_dns_token_name1",
    "/properties/dkim_dns_token_name2",
    "/properties/dkim_dns_token_name3",
    "/properties/dkim_dns_token_value1",
    "/properties/dkim_dns_token_value2",
    "/properties/dkim_dns_token_value3"
  ],
  "createOnlyProperties": [
    "/properties/email_identity"
  ],
  "primaryIdentifier": [
    "/properties/email_identity"
  ],
  "writeOnlyProperties": [
    "/properties/dkim_signing_attributes/domain_signing_selector",
    "/properties/dkim_signing_attributes/domain_signing_private_key"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "ses:CreateEmailIdentity",
        "ses:PutEmailIdentityMailFromAttributes",
        "ses:PutEmailIdentityFeedbackAttributes",
        "ses:PutEmailIdentityDkimAttributes"
      ]
    },
    "read": {
      "permissions": [
        "ses:GetEmailIdentity"
      ]
    },
    "update": {
      "permissions": [
        "ses:PutEmailIdentityMailFromAttributes",
        "ses:PutEmailIdentityFeedbackAttributes",
        "ses:PutEmailIdentityConfigurationSetAttributes",
        "ses:PutEmailIdentityDkimSigningAttributes",
        "ses:PutEmailIdentityDkimAttributes"
      ]
    },
    "delete": {
      "permissions": [
        "ses:DeleteEmailIdentity"
      ]
    },
    "list": {
      "permissions": [
        "ses:ListEmailIdentities"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}