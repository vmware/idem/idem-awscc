{
  "typeName": "AWS::IoTWireless::PartnerAccount",
  "description": "Create and manage partner account",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-rpdk.git",
  "definitions": {
    "sidewalk_account_info": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "app_server_private_key": {
          "type": "string",
          "pattern": "[a-fA-F0-9]{64}",
          "minLength": 1,
          "maxLength": 4096
        }
      },
      "required": [
        "app_server_private_key"
      ]
    },
    "sidewalk_account_info_with_fingerprint": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "amazon_id": {
          "type": "string",
          "maxLength": 2048
        },
        "fingerprint": {
          "type": "string",
          "pattern": "[a-fA-F0-9]{64}",
          "minLength": 64,
          "maxLength": 64
        },
        "arn": {
          "type": "string"
        }
      }
    },
    "sidewalk_update_account": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "app_server_private_key": {
          "type": "string",
          "pattern": "[a-fA-F0-9]{64}",
          "minLength": 1,
          "maxLength": 4096
        }
      }
    },
    "tag": {
      "type": "object",
      "properties": {
        "key": {
          "type": "string",
          "minLength": 1,
          "maxLength": 127
        },
        "value": {
          "type": "string",
          "minLength": 1,
          "maxLength": 255
        }
      },
      "additionalProperties": false
    }
  },
  "properties": {
    "sidewalk": {
      "description": "The Sidewalk account credentials.",
      "$ref": "#/definitions/sidewalk_account_info"
    },
    "partner_account_id": {
      "description": "The partner account ID to disassociate from the AWS account",
      "type": "string",
      "maxLength": 256
    },
    "partner_type": {
      "description": "The partner type",
      "type": "string",
      "enum": [
        "Sidewalk"
      ]
    },
    "sidewalk_response": {
      "description": "The Sidewalk account credentials.",
      "$ref": "#/definitions/sidewalk_account_info_with_fingerprint"
    },
    "account_linked": {
      "description": "Whether the partner account is linked to the AWS account.",
      "type": "boolean"
    },
    "sidewalk_update": {
      "description": "The Sidewalk account credentials.",
      "$ref": "#/definitions/sidewalk_update_account"
    },
    "fingerprint": {
      "description": "The fingerprint of the Sidewalk application server private key.",
      "type": "string",
      "pattern": "[a-fA-F0-9]{64}"
    },
    "arn": {
      "description": "PartnerAccount arn. Returned after successful create.",
      "type": "string"
    },
    "tags": {
      "description": "A list of key-value pairs that contain metadata for the destination.",
      "type": "array",
      "uniqueItems": true,
      "maxItems": 200,
      "insertionOrder": false,
      "items": {
        "$ref": "#/definitions/tag"
      }
    }
  },
  "additionalProperties": false,
  "readOnlyProperties": [
    "/properties/arn",
    "/properties/sidewalk_response"
  ],
  "createOnlyProperties": [
    "/properties/partner_account_id"
  ],
  "primaryIdentifier": [
    "/properties/partner_account_id"
  ],
  "taggable": true,
  "handlers": {
    "create": {
      "permissions": [
        "iotwireless:AssociateAwsAccountWithPartnerAccount",
        "iotwireless:TagResource",
        "iotwireless:ListTagsForResource"
      ]
    },
    "read": {
      "permissions": [
        "iotwireless:GetPartnerAccount",
        "iotwireless:ListTagsForResource"
      ]
    },
    "list": {
      "permissions": [
        "iotwireless:ListPartnerAccounts",
        "iotwireless:ListTagsForResource"
      ]
    },
    "update": {
      "permissions": [
        "iotwireless:UpdatePartnerAccount",
        "iotwireless:UntagResource",
        "iotwireless:ListTagsForResource"
      ]
    },
    "delete": {
      "permissions": [
        "iotwireless:DisassociateAwsAccountFromPartnerAccount"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}