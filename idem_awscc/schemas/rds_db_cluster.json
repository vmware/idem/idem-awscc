{
  "typeName": "AWS::RDS::DBCluster",
  "description": "The AWS::RDS::DBCluster resource creates an Amazon Aurora DB cluster.",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-rds",
  "properties": {
    "endpoint": {
      "$ref": "#/definitions/endpoint"
    },
    "read_endpoint": {
      "$ref": "#/definitions/read_endpoint"
    },
    "allocated_storage": {
      "description": "The amount of storage in gibibytes (GiB) to allocate to each DB instance in the Multi-AZ DB cluster.",
      "type": "integer"
    },
    "associated_roles": {
      "description": "Provides a list of the AWS Identity and Access Management (IAM) roles that are associated with the DB cluster. IAM roles that are associated with a DB cluster grant permission for the DB cluster to access other AWS services on your behalf.",
      "type": "array",
      "uniqueItems": true,
      "items": {
        "$ref": "#/definitions/db_cluster_role"
      }
    },
    "availability_zones": {
      "description": "A list of Availability Zones (AZs) where instances in the DB cluster can be created. For information on AWS Regions and Availability Zones, see Choosing the Regions and Availability Zones in the Amazon Aurora User Guide.",
      "type": "array",
      "uniqueItems": true,
      "items": {
        "type": "string"
      }
    },
    "auto_minor_version_upgrade": {
      "description": "A value that indicates whether minor engine upgrades are applied automatically to the DB cluster during the maintenance window. By default, minor engine upgrades are applied automatically.",
      "type": "boolean"
    },
    "backtrack_window": {
      "description": "The target backtrack window, in seconds. To disable backtracking, set this value to 0.",
      "default": 0,
      "minimum": 0,
      "type": "integer"
    },
    "backup_retention_period": {
      "description": "The number of days for which automated backups are retained.",
      "default": 1,
      "minimum": 1,
      "type": "integer"
    },
    "copy_tags_to_snapshot": {
      "description": "A value that indicates whether to copy all tags from the DB cluster to snapshots of the DB cluster. The default is not to copy them.",
      "type": "boolean"
    },
    "database_name": {
      "description": "The name of your database. If you don't provide a name, then Amazon RDS won't create a database in this DB cluster. For naming constraints, see Naming Constraints in the Amazon RDS User Guide.",
      "type": "string"
    },
    "db_cluster_instance_class": {
      "description": "The compute and memory capacity of each DB instance in the Multi-AZ DB cluster, for example db.m6g.xlarge.",
      "type": "string"
    },
    "db_cluster_resource_id": {
      "description": "The AWS Region-unique, immutable identifier for the DB cluster.",
      "type": "string"
    },
    "db_instance_parameter_group_name": {
      "description": "The name of the DB parameter group to apply to all instances of the DB cluster.",
      "type": "string"
    },
    "global_cluster_identifier": {
      "description": "If you are configuring an Aurora global database cluster and want your Aurora DB cluster to be a secondary member in the global database cluster, specify the global cluster ID of the global database cluster. To define the primary database cluster of the global cluster, use the AWS::RDS::GlobalCluster resource.\n\nIf you aren't configuring a global database cluster, don't specify this property.",
      "type": "string",
      "pattern": "^$|^[a-zA-Z]{1}(?:-?[a-zA-Z0-9]){0,62}$",
      "minLength": 0,
      "maxLength": 63
    },
    "db_cluster_identifier": {
      "description": "The DB cluster identifier. This parameter is stored as a lowercase string.",
      "type": "string",
      "pattern": "^[a-zA-Z]{1}(?:-?[a-zA-Z0-9]){0,62}$",
      "minLength": 1,
      "maxLength": 63
    },
    "db_cluster_parameter_group_name": {
      "description": "The name of the DB cluster parameter group to associate with this DB cluster.",
      "type": "string",
      "default": "default.aurora5.6"
    },
    "db_subnet_group_name": {
      "description": "A DB subnet group that you want to associate with this DB cluster.",
      "type": "string"
    },
    "deletion_protection": {
      "description": "A value that indicates whether the DB cluster has deletion protection enabled. The database can't be deleted when deletion protection is enabled. By default, deletion protection is disabled.",
      "type": "boolean"
    },
    "domain": {
      "description": "The Active Directory directory ID to create the DB cluster in.",
      "type": "string"
    },
    "domain_iam_role_name": {
      "description": "Specify the name of the IAM role to be used when making API calls to the Directory Service.",
      "type": "string"
    },
    "enable_cloudwatch_logs_exports": {
      "description": "The list of log types that need to be enabled for exporting to CloudWatch Logs. The values in the list depend on the DB engine being used. For more information, see Publishing Database Logs to Amazon CloudWatch Logs in the Amazon Aurora User Guide.",
      "type": "array",
      "uniqueItems": true,
      "items": {
        "type": "string"
      }
    },
    "enable_http_endpoint": {
      "description": "A value that indicates whether to enable the HTTP endpoint for an Aurora Serverless DB cluster. By default, the HTTP endpoint is disabled.",
      "type": "boolean"
    },
    "enable_iam_database_authentication": {
      "description": "A value that indicates whether to enable mapping of AWS Identity and Access Management (IAM) accounts to database accounts. By default, mapping is disabled.",
      "type": "boolean"
    },
    "engine": {
      "description": "The name of the database engine to be used for this DB cluster. Valid Values: aurora (for MySQL 5.6-compatible Aurora), aurora-mysql (for MySQL 5.7-compatible Aurora), and aurora-postgresql",
      "type": "string"
    },
    "engine_mode": {
      "description": "The DB engine mode of the DB cluster, either provisioned, serverless, parallelquery, global, or multimaster.",
      "type": "string"
    },
    "engine_version": {
      "description": "The version number of the database engine to use.",
      "type": "string"
    },
    "iops": {
      "description": "The amount of Provisioned IOPS (input/output operations per second) to be initially allocated for each DB instance in the Multi-AZ DB cluster.",
      "type": "integer"
    },
    "kms_key_id": {
      "description": "The Amazon Resource Name (ARN) of the AWS Key Management Service master key that is used to encrypt the database instances in the DB cluster, such as arn:aws:kms:us-east-1:012345678910:key/abcd1234-a123-456a-a12b-a123b4cd56ef. If you enable the StorageEncrypted property but don't specify this property, the default master key is used. If you specify this property, you must set the StorageEncrypted property to true.",
      "type": "string"
    },
    "master_username": {
      "description": "The name of the master user for the DB cluster. You must specify MasterUsername, unless you specify SnapshotIdentifier. In that case, don't specify MasterUsername.",
      "type": "string",
      "pattern": "^[a-zA-Z]{1}[a-zA-Z0-9_]*$",
      "minLength": 1
    },
    "master_user_password": {
      "description": "The master password for the DB instance.",
      "type": "string"
    },
    "monitoring_interval": {
      "description": "The interval, in seconds, between points when Enhanced Monitoring metrics are collected for the DB cluster. To turn off collecting Enhanced Monitoring metrics, specify 0. The default is 0.",
      "type": "integer",
      "default": 0
    },
    "monitoring_role_arn": {
      "description": "The Amazon Resource Name (ARN) for the IAM role that permits RDS to send Enhanced Monitoring metrics to Amazon CloudWatch Logs.",
      "type": "string"
    },
    "network_type": {
      "description": "The network type of the DB cluster.",
      "type": "string"
    },
    "performance_insights_enabled": {
      "description": "A value that indicates whether to turn on Performance Insights for the DB cluster.",
      "type": "boolean"
    },
    "performance_insights_kms_key_id": {
      "description": "The Amazon Web Services KMS key identifier for encryption of Performance Insights data.",
      "type": "string"
    },
    "performance_insights_retention_period": {
      "description": "The amount of time, in days, to retain Performance Insights data.",
      "type": "integer"
    },
    "port": {
      "description": "The port number on which the instances in the DB cluster accept connections. Default: 3306 if engine is set as aurora or 5432 if set to aurora-postgresql.",
      "type": "integer"
    },
    "preferred_backup_window": {
      "description": "The daily time range during which automated backups are created if automated backups are enabled using the BackupRetentionPeriod parameter. The default is a 30-minute window selected at random from an 8-hour block of time for each AWS Region. To see the time blocks available, see Adjusting the Preferred DB Cluster Maintenance Window in the Amazon Aurora User Guide.",
      "type": "string"
    },
    "preferred_maintenance_window": {
      "description": "The weekly time range during which system maintenance can occur, in Universal Coordinated Time (UTC). The default is a 30-minute window selected at random from an 8-hour block of time for each AWS Region, occurring on a random day of the week. To see the time blocks available, see Adjusting the Preferred DB Cluster Maintenance Window in the Amazon Aurora User Guide.",
      "type": "string"
    },
    "publicly_accessible": {
      "description": "A value that indicates whether the DB cluster is publicly accessible.",
      "type": "boolean"
    },
    "replication_source_identifier": {
      "description": "The Amazon Resource Name (ARN) of the source DB instance or DB cluster if this DB cluster is created as a Read Replica.",
      "type": "string"
    },
    "restore_type": {
      "description": "The type of restore to be performed. You can specify one of the following values:\nfull-copy - The new DB cluster is restored as a full copy of the source DB cluster.\ncopy-on-write - The new DB cluster is restored as a clone of the source DB cluster.",
      "type": "string",
      "default": "full-copy"
    },
    "serverlessv2_scaling_configuration": {
      "description": "Contains the scaling configuration of an Aurora Serverless v2 DB cluster.",
      "$ref": "#/definitions/serverlessv2_scaling_configuration"
    },
    "scaling_configuration": {
      "description": "The ScalingConfiguration property type specifies the scaling configuration of an Aurora Serverless DB cluster.",
      "$ref": "#/definitions/scaling_configuration"
    },
    "snapshot_identifier": {
      "description": "The identifier for the DB snapshot or DB cluster snapshot to restore from.\nYou can use either the name or the Amazon Resource Name (ARN) to specify a DB cluster snapshot. However, you can use only the ARN to specify a DB snapshot.\nAfter you restore a DB cluster with a SnapshotIdentifier property, you must specify the same SnapshotIdentifier property for any future updates to the DB cluster. When you specify this property for an update, the DB cluster is not restored from the snapshot again, and the data in the database is not changed. However, if you don't specify the SnapshotIdentifier property, an empty DB cluster is created, and the original DB cluster is deleted. If you specify a property that is different from the previous snapshot restore property, the DB cluster is restored from the specified SnapshotIdentifier property, and the original DB cluster is deleted.",
      "type": "string"
    },
    "source_db_cluster_identifier": {
      "description": "The identifier of the source DB cluster from which to restore.",
      "type": "string"
    },
    "source_region": {
      "description": "The AWS Region which contains the source DB cluster when replicating a DB cluster. For example, us-east-1.",
      "type": "string"
    },
    "storage_encrypted": {
      "description": "Indicates whether the DB instance is encrypted.\nIf you specify the DBClusterIdentifier, SnapshotIdentifier, or SourceDBInstanceIdentifier property, don't specify this property. The value is inherited from the cluster, snapshot, or source DB instance.",
      "type": "boolean"
    },
    "storage_type": {
      "description": "Specifies the storage type to be associated with the DB cluster.",
      "type": "string"
    },
    "tags": {
      "type": "array",
      "maxItems": 50,
      "uniqueItems": true,
      "insertionOrder": false,
      "description": "An array of key-value pairs to apply to this resource.",
      "items": {
        "$ref": "#/definitions/tag"
      }
    },
    "use_latest_restorable_time": {
      "description": "A value that indicates whether to restore the DB cluster to the latest restorable backup time. By default, the DB cluster is not restored to the latest restorable backup time.",
      "type": "boolean"
    },
    "vpc_security_group_ids": {
      "description": "A list of EC2 VPC security groups to associate with this DB cluster.",
      "uniqueItems": true,
      "items": {
        "type": "string"
      },
      "type": "array"
    }
  },
  "definitions": {
    "endpoint": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "address": {
          "description": "The connection endpoint for the DB cluster.",
          "type": "string"
        },
        "port": {
          "description": "The port number that will accept connections on this DB cluster.",
          "type": "string"
        }
      }
    },
    "read_endpoint": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "address": {
          "description": "The reader endpoint for the DB cluster.",
          "type": "string"
        }
      }
    },
    "db_cluster_role": {
      "description": "Describes an AWS Identity and Access Management (IAM) role that is associated with a DB cluster.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "feature_name": {
          "description": "The name of the feature associated with the AWS Identity and Access Management (IAM) role. For the list of supported feature names, see DBEngineVersion in the Amazon RDS API Reference.",
          "type": "string"
        },
        "role_arn": {
          "description": "The Amazon Resource Name (ARN) of the IAM role that is associated with the DB cluster.",
          "type": "string"
        }
      },
      "required": [
        "role_arn"
      ]
    },
    "serverlessv2_scaling_configuration": {
      "description": "Contains the scaling configuration of an Aurora Serverless v2 DB cluster.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "min_capacity": {
          "description": "The minimum number of Aurora capacity units (ACUs) for a DB instance in an Aurora Serverless v2 cluster. You can specify ACU values in half-step increments, such as 8, 8.5, 9, and so on. The smallest value that you can use is 0.5.",
          "type": "number",
          "minimum": 0.5,
          "maximum": 128
        },
        "max_capacity": {
          "description": "The maximum number of Aurora capacity units (ACUs) for a DB instance in an Aurora Serverless v2 cluster. You can specify ACU values in half-step increments, such as 40, 40.5, 41, and so on. The largest value that you can use is 128.",
          "type": "number",
          "minimum": 0.5,
          "maximum": 128
        }
      }
    },
    "scaling_configuration": {
      "description": "The ScalingConfiguration property type specifies the scaling configuration of an Aurora Serverless DB cluster.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "auto_pause": {
          "description": "A value that indicates whether to allow or disallow automatic pause for an Aurora DB cluster in serverless DB engine mode. A DB cluster can be paused only when it's idle (it has no connections).",
          "type": "boolean"
        },
        "max_capacity": {
          "description": "The maximum capacity for an Aurora DB cluster in serverless DB engine mode.\nFor Aurora MySQL, valid capacity values are 1, 2, 4, 8, 16, 32, 64, 128, and 256.\nFor Aurora PostgreSQL, valid capacity values are 2, 4, 8, 16, 32, 64, 192, and 384.\nThe maximum capacity must be greater than or equal to the minimum capacity.",
          "type": "integer"
        },
        "min_capacity": {
          "description": "The minimum capacity for an Aurora DB cluster in serverless DB engine mode.\nFor Aurora MySQL, valid capacity values are 1, 2, 4, 8, 16, 32, 64, 128, and 256.\nFor Aurora PostgreSQL, valid capacity values are 2, 4, 8, 16, 32, 64, 192, and 384.\nThe minimum capacity must be less than or equal to the maximum capacity.",
          "type": "integer"
        },
        "seconds_until_auto_pause": {
          "description": "The time, in seconds, before an Aurora DB cluster in serverless mode is paused.",
          "type": "integer"
        }
      }
    },
    "tag": {
      "description": "A key-value pair to associate with a resource.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "key": {
          "type": "string",
          "description": "The key name of the tag. You can specify a value that is 1 to 128 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -. ",
          "minLength": 1,
          "maxLength": 128
        },
        "value": {
          "type": "string",
          "description": "The value for the tag. You can specify a value that is 0 to 256 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -. ",
          "minLength": 0,
          "maxLength": 256
        }
      },
      "required": [
        "key"
      ]
    }
  },
  "additionalProperties": false,
  "propertyTransform": {
    "/properties/DBClusterIdentifier": "$lowercase(DBClusterIdentifier)",
    "/properties/DBClusterParameterGroupName": "$lowercase(DBClusterParameterGroupName)",
    "/properties/DBSubnetGroupName": "$lowercase(DBSubnetGroupName)",
    "/properties/SnapshotIdentifier": "$lowercase(SnapshotIdentifier)",
    "/properties/SourceDBClusterIdentifier": "$lowercase(SourceDBClusterIdentifier)"
  },
  "readOnlyProperties": [
    "/properties/db_cluster_resource_id",
    "/properties/endpoint",
    "/properties/endpoint/address",
    "/properties/endpoint/port",
    "/properties/read_endpoint/port",
    "/properties/read_endpoint/address"
  ],
  "createOnlyProperties": [
    "/properties/availability_zones",
    "/properties/db_cluster_identifier",
    "/properties/db_subnet_group_name",
    "/properties/database_name",
    "/properties/engine_mode",
    "/properties/kms_key_id",
    "/properties/publicly_accessible",
    "/properties/restore_type",
    "/properties/snapshot_identifier",
    "/properties/source_db_cluster_identifier",
    "/properties/source_region",
    "/properties/storage_encrypted",
    "/properties/use_latest_restorable_time"
  ],
  "conditionalCreateOnlyProperties": [
    "/properties/Engine",
    "/properties/GlobalClusterIdentifier",
    "/properties/MasterUsername"
  ],
  "primaryIdentifier": [
    "/properties/db_cluster_identifier"
  ],
  "writeOnlyProperties": [
    "/properties/db_instance_parameter_group_name",
    "/properties/master_user_password",
    "/properties/restore_type",
    "/properties/snapshot_identifier",
    "/properties/source_db_cluster_identifier",
    "/properties/source_region",
    "/properties/use_latest_restorable_time"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "iam:PassRole",
        "rds:AddRoleToDBCluster",
        "rds:AddTagsToResource",
        "rds:CreateDBCluster",
        "rds:CreateDBInstance",
        "rds:DescribeDBClusters",
        "rds:ModifyDBCluster",
        "rds:RestoreDBClusterFromSnapshot",
        "rds:RestoreDBClusterToPointInTime"
      ],
      "timeoutInMinutes": 2160
    },
    "read": {
      "permissions": [
        "rds:DescribeDBClusters"
      ]
    },
    "update": {
      "permissions": [
        "ec2:DescribeSecurityGroups",
        "iam:PassRole",
        "rds:AddRoleToDBCluster",
        "rds:AddTagsToResource",
        "rds:DescribeDBClusters",
        "rds:DescribeDBSubnetGroups",
        "rds:DescribeGlobalClusters",
        "rds:ModifyDBCluster",
        "rds:ModifyDBInstance",
        "rds:RemoveFromGlobalCluster",
        "rds:RemoveRoleFromDBCluster",
        "rds:RemoveTagsFromResource"
      ],
      "timeoutInMinutes": 2160
    },
    "delete": {
      "permissions": [
        "rds:DeleteDBCluster",
        "rds:DeleteDBInstance",
        "rds:DescribeDBClusters",
        "rds:DescribeGlobalClusters",
        "rds:RemoveFromGlobalCluster"
      ]
    },
    "list": {
      "permissions": [
        "rds:DescribeDBClusters"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}