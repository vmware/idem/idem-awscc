{
  "typeName": "AWS::CloudFormation::HookTypeConfig",
  "description": "Specifies the configuration data for a registered hook in CloudFormation Registry.",
  "sourceUrl": "https://github.com/aws-cloudformation/aws-cloudformation-resource-providers-cloudformation",
  "properties": {
    "type_arn": {
      "description": "The Amazon Resource Name (ARN) of the type without version number.",
      "pattern": "^arn:aws[A-Za-z0-9-]{0,64}:cloudformation:[A-Za-z0-9-]{1,64}:([0-9]{12})?:type/hook/.+$",
      "type": "string"
    },
    "type_name": {
      "description": "The name of the type being registered.\n\nWe recommend that type names adhere to the following pattern: company_or_organization::service::type.",
      "pattern": "^[A-Za-z0-9]{2,64}::[A-Za-z0-9]{2,64}::[A-Za-z0-9]{2,64}$",
      "type": "string"
    },
    "configuration_arn": {
      "description": "The Amazon Resource Name (ARN) for the configuration data, in this account and region.",
      "pattern": "^arn:aws[A-Za-z0-9-]{0,64}:cloudformation:[A-Za-z0-9-]{1,64}:([0-9]{12})?:type(-configuration)?/hook/.+$",
      "type": "string"
    },
    "configuration": {
      "description": "The configuration data for the extension, in this account and region.",
      "pattern": "[\\s\\S]+",
      "type": "string"
    },
    "configuration_alias": {
      "description": "An alias by which to refer to this extension configuration data.",
      "pattern": "^[a-zA-Z0-9]{1,256}$",
      "default": "default",
      "enum": [
        "default"
      ],
      "type": "string"
    }
  },
  "oneOf": [
    {
      "required": [
        "type_arn",
        "configuration"
      ]
    },
    {
      "required": [
        "type_name",
        "configuration"
      ]
    }
  ],
  "readOnlyProperties": [
    "/properties/configuration_arn"
  ],
  "createOnlyProperties": [
    "/properties/configuration_alias"
  ],
  "primaryIdentifier": [
    "/properties/configuration_arn"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "cloudformation:SetTypeConfiguration"
      ]
    },
    "read": {
      "permissions": [
        "cloudformation:BatchDescribeTypeConfigurations"
      ]
    },
    "update": {
      "permissions": [
        "cloudformation:SetTypeConfiguration"
      ]
    },
    "delete": {
      "permissions": [
        "cloudformation:SetTypeConfiguration"
      ]
    },
    "list": {
      "permissions": [
        "cloudformation:BatchDescribeTypeConfigurations"
      ]
    }
  },
  "additionalProperties": false,
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": true
}