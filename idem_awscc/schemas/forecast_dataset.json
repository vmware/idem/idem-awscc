{
  "typeName": "AWS::Forecast::Dataset",
  "description": "Resource Type Definition for AWS::Forecast::Dataset",
  "sourceUrl": "https://github.com/junlinzw/aws-cloudformation-resource-providers-forecast",
  "taggable": false,
  "definitions": {
    "attributes": {
      "type": "array",
      "insertionOrder": true,
      "items": {
        "type": "object",
        "additionalProperties": false,
        "properties": {
          "attribute_name": {
            "description": "Name of the dataset field",
            "type": "string",
            "pattern": "^[a-zA-Z][a-zA-Z0-9_]*"
          },
          "attribute_type": {
            "description": "Data type of the field",
            "type": "string",
            "enum": [
              "string",
              "integer",
              "float",
              "timestamp",
              "geolocation"
            ]
          }
        }
      },
      "minItems": 1,
      "maxItems": 100
    },
    "kms_key_arn": {
      "description": "KMS key used to encrypt the Dataset data",
      "type": "string",
      "maxLength": 256,
      "pattern": "arn:aws[-a-z]*:kms:.*:key/.*"
    },
    "role_arn": {
      "description": "The ARN of the IAM role that Amazon Forecast can assume to access the AWS KMS key.",
      "type": "string",
      "maxLength": 256,
      "pattern": "^[a-zA-Z0-9\\-\\_\\.\\/\\:]+$"
    },
    "key": {
      "type": "string",
      "description": "The key name of the tag. You can specify a value that is 1 to 128 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -.",
      "minLength": 1,
      "maxLength": 128
    },
    "value": {
      "type": "string",
      "description": "The value for the tag. You can specify a value that is 0 to 256 Unicode characters in length and cannot be prefixed with aws:. You can use any of the following characters: the set of Unicode letters, digits, whitespace, _, ., /, =, +, and -.",
      "minLength": 0,
      "maxLength": 256
    }
  },
  "properties": {
    "arn": {
      "type": "string",
      "maxLength": 256,
      "pattern": "^[a-zA-Z0-9\\-\\_\\.\\/\\:]+$"
    },
    "dataset_name": {
      "description": "A name for the dataset",
      "type": "string",
      "minLength": 1,
      "maxLength": 63,
      "pattern": "^[a-zA-Z][a-zA-Z0-9_]*"
    },
    "dataset_type": {
      "description": "The dataset type",
      "type": "string",
      "enum": [
        "TARGET_TIME_SERIES",
        "RELATED_TIME_SERIES",
        "ITEM_METADATA"
      ]
    },
    "data_frequency": {
      "description": "Frequency of data collection. This parameter is required for RELATED_TIME_SERIES",
      "type": "string",
      "pattern": "^Y|M|W|D|H|30min|15min|10min|5min|1min$"
    },
    "domain": {
      "description": "The domain associated with the dataset",
      "type": "string",
      "enum": [
        "RETAIL",
        "CUSTOM",
        "INVENTORY_PLANNING",
        "EC2_CAPACITY",
        "WORK_FORCE",
        "WEB_TRAFFIC",
        "METRICS"
      ]
    },
    "encryption_config": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "kms_key_arn": {
          "$ref": "#/definitions/kms_key_arn"
        },
        "role_arn": {
          "$ref": "#/definitions/role_arn"
        }
      }
    },
    "schema": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "attributes": {
          "$ref": "#/definitions/attributes"
        }
      }
    },
    "tags": {
      "type": "array",
      "insertionOrder": true,
      "items": {
        "description": "A key-value pair to associate with a resource.",
        "type": "object",
        "properties": {
          "key": {
            "$ref": "#/definitions/key"
          },
          "value": {
            "$ref": "#/definitions/value"
          }
        },
        "required": [
          "key",
          "value"
        ],
        "additionalProperties": false
      },
      "minItems": 0,
      "maxItems": 200
    }
  },
  "additionalProperties": false,
  "required": [
    "dataset_name",
    "dataset_type",
    "domain",
    "schema"
  ],
  "createOnlyProperties": [
    "/properties/dataset_name"
  ],
  "readOnlyProperties": [
    "/properties/arn"
  ],
  "primaryIdentifier": [
    "/properties/arn"
  ],
  "handlers": {
    "create": {
      "permissions": [
        "forecast:CreateDataset"
      ]
    },
    "read": {
      "permissions": [
        "forecast:DescribeDataset"
      ]
    },
    "delete": {
      "permissions": [
        "forecast:DeleteDataset"
      ]
    },
    "list": {
      "permissions": [
        "forecast:ListDatasets"
      ]
    }
  },
  "$schema": "http://json-schema.org/draft-07/schema#",
  "is_resource_mutable": false
}