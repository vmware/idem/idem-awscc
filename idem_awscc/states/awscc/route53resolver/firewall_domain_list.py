
from typing import Dict, Any\

__contracts__ = ["resource"]

RESOURCE_TYPE = "route53resolver_firewall_domain_list"


async def present(
    hub, ctx, name: str, resource_id: str = None, **attributes: Dict[str, Any]
) -> Dict[str, Any]:
    return await hub.states.awscc.cloudcontrol.api.present(
        ctx, name, RESOURCE_TYPE, resource_id, **attributes
    )


async def absent(hub, ctx, name: str, resource_id: str = None) -> Dict[str, Any]:
    return await hub.states.awscc.cloudcontrol.api.absent(
        ctx, name, RESOURCE_TYPE, resource_id
    )


async def describe(
    hub, ctx
) -> Dict[str, Dict[str, Any]]:
    return await hub.states.awscc.cloudcontrol.api.describe(
        ctx, RESOURCE_TYPE
    )
    