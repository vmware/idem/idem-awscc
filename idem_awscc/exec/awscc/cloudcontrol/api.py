import json
import os
from typing import Any
from typing import Dict

from json_schema_for_humans.generate import generate_from_filename
from json_schema_for_humans.generation_configuration import GenerationConfiguration
from jsonschema.exceptions import SchemaError
from jsonschema.validators import Draft7Validator


async def generate_resource_state(hub, ctx, aws_resource_type: str) -> Dict[str, Any]:
    result = dict(result=True, comment=(), ret=None)
    resource_type_lowercase = (
        hub.tool.awscc.cloudcontrol.api.convert_aws_resource_type_name_to_lowercase(
            aws_resource_type
        )
    )

    json_schema_ret = await hub.exec.awscc.cloudcontrol.api.generate_json_schema(
        ctx, aws_resource_type, resource_type_lowercase
    )
    if not json_schema_ret["result"]:
        result["result"] = False
        result["comment"] = json_schema_ret["comment"]
        return result

    resource_python_state_file_ret = (
        hub.exec.awscc.cloudcontrol.api.generate_resource_python_state_file(
            resource_type_lowercase
        )
    )
    if not resource_python_state_file_ret["result"]:
        result["result"] = False
        result["comment"] = resource_python_state_file_ret["comment"]
        return result

    # This generates HTML documentation from the resource JSON schema but it is not compliant with the idem standards
    # resource_documentation_ret = (
    #     hub.exec.awscc.cloudcontrol.api.generate_resource_documentation(
    #         resource_type_lowercase
    #     )
    # )
    # if not resource_documentation_ret["result"]:
    #     result["result"] = False
    #     result["comment"] = resource_documentation_ret["comment"]
    #     return result

    return result


async def generate_all_resources_states(hub, ctx):
    result = dict(result=True, comment=(), ret=None)

    all_aws_resource_types_ret = (
        await hub.tool.awscc.cloudcontrol.api.get_all_resource_types_names(ctx)
    )
    if not all_aws_resource_types_ret["result"]:
        result["result"] = False
        result["comment"] = all_aws_resource_types_ret["comment"]
        return result

    for aws_resource_type in all_aws_resource_types_ret["resource_types_list"]:
        generated_resource_ret = (
            await hub.exec.awscc.cloudcontrol.api.generate_resource_state(
                ctx, aws_resource_type
            )
        )
        result["result"] = result["result"] and generated_resource_ret["result"]
        if not generated_resource_ret["result"]:
            result["comment"] = generated_resource_ret["comment"]

    return result


async def generate_json_schema(
    hub, ctx, aws_resource_type: str, resource_type_lowercase: str
) -> Dict[str, Any]:
    result = dict(result=True, comment=(), ret=None)

    resource_description_ret = await hub.exec.boto3.client.cloudformation.describe_type(
        ctx, Type="RESOURCE", TypeName=aws_resource_type
    )

    if resource_description_ret["result"]:
        resource_attributes_schema = json.loads(
            resource_description_ret["ret"].get("Schema")
        )
        validation_schema = hub.tool.awscc.cloudcontrol.api.convert_schema_to_lowercase(
            resource_attributes_schema
        )
        validation_schema["$schema"] = "http://json-schema.org/draft-07/schema#"

        resource_provisioning_type = resource_description_ret["ret"].get(
            "ProvisioningType"
        )
        validation_schema["is_resource_mutable"] = (
            resource_provisioning_type == "FULLY_MUTABLE"
        )

        try:
            Draft7Validator.check_schema(validation_schema)
        except SchemaError as err:
            result["result"] = False
            result["comment"] += (err.message,)
            return result

        # TODO: Make sure that the directory os correct no matter from where idem exec is run
        filename = os.path.join(
            os.getcwd(), f"idem_awscc/schemas/{resource_type_lowercase}.json"
        )

        try:
            with open(filename, "w") as fp:
                json.dump(validation_schema, fp, indent=2)
        except OSError as err:
            result["result"] = False
            result["comment"] += (err,)
    else:
        result["result"] = False
        result["comment"] = resource_description_ret["comment"]

    return result


async def generate_all_resources_json_schemas(hub, ctx) -> Dict[str, Any]:
    result = dict(result=True, comment=(), ret=None)

    all_resource_types_ret = (
        await hub.tool.awscc.cloudcontrol.api.get_all_resource_types_names(ctx)
    )

    if not all_resource_types_ret["result"]:
        result["result"] = False
        result["comment"] += all_resource_types_ret["comment"]
        return result

    for resource_type in all_resource_types_ret["resource_types_list"]:
        generated_schema_ret = await hub.exec.awscc.cloudcontrol.api.generate_json_schema(
            ctx, resource_type
        )

        if not generated_schema_ret["result"]:
            result["result"] = False
            result["comment"] += generated_schema_ret["comment"]

    return result


def generate_resource_python_state_file(hub, resource_type: str):
    result = dict(result=True, comment=(), ret=None)

    template = rf"""
from typing import Dict, Any\

__contracts__ = ["resource"]

RESOURCE_TYPE = "{resource_type}"


async def present(
    hub, ctx, name: str, resource_id: str = None, **attributes: Dict[str, Any]
) -> Dict[str, Any]:
    return await hub.states.awscc.cloudcontrol.api.present(
        ctx, name, RESOURCE_TYPE, resource_id, **attributes
    )


async def absent(hub, ctx, name: str, resource_id: str = None) -> Dict[str, Any]:
    return await hub.states.awscc.cloudcontrol.api.absent(
        ctx, name, RESOURCE_TYPE, resource_id
    )


async def describe(
    hub, ctx
) -> Dict[str, Dict[str, Any]]:
    return await hub.states.awscc.cloudcontrol.api.describe(
        ctx, RESOURCE_TYPE
    )
    """

    # TODO: Make sure that the directory os correct no matter from where idem exec is run

    split = resource_type.split("_")
    directory = split[0]
    filename = "_".join(split[1:])
    file_path = os.path.join(
        os.getcwd(), f"idem_awscc/states/awscc/{directory}/{filename}.py"
    )
    os.makedirs(f"idem_awscc/states/awscc/{directory}", exist_ok=True)
    try:
        with open(file_path, "w") as fp:
            fp.write(template)
    except OSError as err:
        result["result"] = False
        result["comment"] += (err,)

    return result


async def generate_all_resource_state_files(hub, ctx):
    result = dict(result=True, comment=(), ret=None)

    all_resource_types_ret = (
        await hub.tool.awscc.cloudcontrol.api.get_all_resource_types_names(ctx)
    )

    if not all_resource_types_ret["result"]:
        result["result"] = False
        result["comment"] += all_resource_types_ret["comment"]
        return result

    for resource_type in all_resource_types_ret["resource_types_list"]:
        resource_name = (
            hub.tool.awscc.cloudcontrol.api.convert_aws_resource_type_name_to_lowercase(
                resource_type
            )
        )
        generated_schema_ret = (
            hub.exec.awscc.cloudcontrol.api.generate_resource_python_state_file(
                resource_name
            )
        )

        if not generated_schema_ret["result"]:
            result["result"] = False
            result["comment"] += generated_schema_ret["comment"]

    return result


def generate_resource_documentation(
    hub, resource_type_lowercase: str
) -> Dict[str, Any]:
    """
    Generates HTML documentation from the resource JSON schemas.
    This documentation as of now is not compliant with the idem
    standards and should not be used

    :param hub: the hub
    :param resource_type_lowercase: the type of the resource in lowercase
    :return: documentation generated from the reosurce JSON schema
    """


    result = dict(result=True, comment=(), ret=None)
    json_schema_filename = os.path.join(
        os.getcwd(), f"idem_awscc/schemas/{resource_type_lowercase}.json"
    )
    documentation_filename = os.path.join(
        os.getcwd(), f"documentation/{resource_type_lowercase}.html"
    )
    config = GenerationConfiguration(
        expand_buttons=True,
        deprecated_from_description=True,
        default_from_description=True,
        examples_as_yaml=True,
    )
    try:
        generate_from_filename(
            json_schema_filename, documentation_filename, config=config
        )
    except Exception as e:
        result["result"] = False
        result["comment"] += (
            f"Could not generate documentation for the schema {json_schema_filename}",
            e,
        )

    return result


def generate_documentation_for_all_resources(hub) -> Dict[str, Any]:
    result = dict(result=True, comment=(), ret=None)
    json_schemas_dir = os.path.join(os.getcwd(), "idem_awscc/schemas")

    for filename in os.listdir(json_schemas_dir):
        if filename.endswith(".json"):
            resource_type_name = filename.split(".")[0]
            generated_documentation_ret = (
                hub.exec.awscc.cloudcontrol.api.generate_resource_documentation(
                    resource_type_name
                )
            )

            if not generated_documentation_ret["result"]:
                result["result"] = False
                result["comment"] = generated_documentation_ret["comment"]

    return result
